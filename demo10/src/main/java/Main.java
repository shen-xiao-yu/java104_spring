public class Main {
//    手撕：
//    给出一个含有不重复整数元素的数组 arr ，每个整数 arr[i] 均大于 1。
//    用这些整数来构建二叉树，每个整数可以使用任意次数。其中：每个非叶结点的值应等于它的两个子结点的值的乘积。
//    满足条件的二叉树一共有多少个？
//
//    模板：
//    class Solution {
//            public int numFactoredBinaryTrees(int[] arr) {
//
//            }
//    }
//
//    输入: arr = [2, 4]
//    输出: 3
//    解释: 可以得到这些二叉树: [2], [4], [4, 2, 2]
//
//    作者：985柜员
//    链接：https://www.nowcoder.com/feed/main/detail/50434891b7354ab0b505359ba6bdae04?anchorPoint=comment
//    来源：牛客网
    class TreeNode {
        public int value;
        public TreeNode left;
        public TreeNode right;
        TreeNode(int value) {
            this.value = value;
        }
}
public static void main(String[] args) {
    int[] arr = new int[]{2, 4};
    for (int i = 0; i < arr.length; ++i) {
        TreeNode head = new TreeNode(arr[i]);

    }
}
}


