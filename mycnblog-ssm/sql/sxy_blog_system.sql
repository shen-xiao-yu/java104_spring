-- 创建数据库
drop database if exists sxy_blog_system;
create database sxy_blog_system DEFAULT CHARACTER SET utf8mb4;

-- 使用数据数据
use sxy_blog_system;

-- 创建表[用户表]
drop table if exists  userinfo;
create table userinfo(
    id int primary key auto_increment,
    username varchar(100) not null unique,
    password varchar(65) not null,
    photo varchar(500) default '',
    createtime timestamp default now(),
    `state` int default 1
) default charset 'utf8mb4';

-- 创建文章表
drop table if exists  articleinfo;
create table articleinfo(
    id int primary key auto_increment,
    title varchar(100) not null,
    content text not null,
    createtime timestamp default now(),
    uid int not null,
    rcount int not null default 1,
    `state` int default 1
)default charset 'utf8mb4';

-- 添加一个用户信息
INSERT INTO `sxy_blog_system`.`userinfo` (`id`, `username`, `password`, `photo`, `createtime`,  `state`) VALUES
(1, 'admin', 'admin', '', '2021-12-06 17:10:48', 1);

-- 文章添加测试数据
insert into articleinfo(title,content,uid)
    values('Java','Java正文',1);
