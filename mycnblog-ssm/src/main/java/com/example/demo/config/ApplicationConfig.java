package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// 系统配置文件类
@Configuration // 该配置文件在项目启动时配置
public class ApplicationConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterception loginInterception; // 用户登录拦截器
    // 自定义拦截器添加到系统的配置文件中

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterception)
                .addPathPatterns("/**")       // 设置拦截规则
//                .excludePathPatterns("/login.html")// 下面这些指定的url，不会检查是否登录——进行拦截
//                .excludePathPatterns("/reg.html")
                .excludePathPatterns("/**/*.html")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/user/reg")
                // 总的博客的列表，和博客详情，我们需要开放权限，即使用户未登录也可以查看
                .excludePathPatterns("/article/get_list_by_page") // 放行总博客列表
                .excludePathPatterns("/article/get_by_blog_id") // 放行博客详情页面
                .excludePathPatterns("/user/get_detail_by_uid")
                //.excludePathPatterns("/css/**")--->和下一行的作用是一样的
                //.excludePathPatterns("/**/*.css")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/editor.md/**")
                .excludePathPatterns("/img/**"); // 直接img目录下的所有都放行

    }
}
