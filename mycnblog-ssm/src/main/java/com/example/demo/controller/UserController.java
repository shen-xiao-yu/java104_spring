package com.example.demo.controller;

import com.example.demo.config.AjaxResult;
import com.example.demo.model.UserInfo;
import com.example.demo.service.UserService;
import com.example.demo.util.ConstVariable;
import com.example.demo.util.PasswordUtil;
import com.sun.corba.se.impl.ior.OldJIDLObjectKeyTemplate;
import lombok.SneakyThrows;
import org.apache.ibatis.annotations.ResultMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 用户注册
     * @param username
     * @param password1
     * @param password2
     * @return
     */
    @RequestMapping("/reg")
    public Object reg(String username, String password1, String password2) {
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password1) || !StringUtils.hasLength(password2)) {
            return AjaxResult.fail(-1, "参数输入错误");
        }
        else {
            if (!password1.equals(password2)) {
                return AjaxResult.fail(-1, "前后密码不一致");
            }
            else {
                int result = userService.reg(username, PasswordUtil.encrypt(password1));
                if (result != 1) {
                    return AjaxResult.fail(-1, "数据库添加出错");
                }
                return AjaxResult.success("注册成功", "1");
            }
        }
    }

    /**
     * 用户登录
     * @param request
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("/login")
    public Object login(HttpServletRequest request, String username, String password) {

        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password)) {
            return AjaxResult.fail(-1, "参数输入错误,登录失败");
        }
        UserInfo userInfo = userService.selectByUsername(username);
        // 随机加盐算法，未加密的密码用同样的加密算法和数据库中的加密算法进行比较
        if (userInfo == null || ! PasswordUtil.decrypt(password, userInfo.getPassword()) || userInfo.getId() <= 0) {

            return AjaxResult.fail(-1, "账号或密码错误，登录失败");
        }
        // 如果程序走到这里，说明登陆成功了
        // 每次登录成功都要对数据库中的密码进行重新生成,这样才符合随机加密
        // userService.passwordRefresh()
        HttpSession session = request.getSession(); // 获取session，如果没有就创建一个

        session.setAttribute(ConstVariable.USER_SESSION_KEY, userInfo);
        return AjaxResult.success("登录成功", 1);
    }
    /**
     *   获取当前登录的用户名
     */
    @RequestMapping("/get_user")
    public Object getUser(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return AjaxResult.fail(401, "当前用户未登录");
        }
        UserInfo userInfo = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
        if (userInfo == null) {
            return AjaxResult.fail(401, "当前用户未登录");
        }
        return userInfo; // 这里我们可以直接用AjaxResult自定义统一格式返回，也可以传统的统一格式返回
    }

    /**
     * 退出登录
     * @param request
     * @param response
     */
    @SneakyThrows
    @RequestMapping("/logout")
    public Object logout(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            // response.sendRedirect("/login.html"); // 当前用户未登录
            return AjaxResult.fail(401, "当前用户未登录！");
        }
        session.setAttribute(ConstVariable.USER_SESSION_KEY, null);
        //response.sendRedirect("/login.html");
        return AjaxResult.success("当前状态正常，可以正常退出！", "true");
    }

    /**
     * 通过个人id(uid)查询个人详细信息
     * @param authorId
     * @return
     */
    @RequestMapping("/get_detail_by_uid")
    public Object selectByUid(Integer authorId) {
        int uid = authorId;
        if (uid <= 0) {
            return AjaxResult.fail(-1, "uid《=0，查询出错！");
        }
        UserInfo userInfo = userService.selectByUid(uid);
        if (userInfo == null) {
            return AjaxResult.fail(-1, "在数据库中查询不到指定用户id的详细信息，请稍后再试！");
        }
        return AjaxResult.success("通过uid查询个人详细信息成功", userInfo);
    }
}
