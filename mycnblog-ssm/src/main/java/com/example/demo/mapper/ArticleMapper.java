package com.example.demo.mapper;

import com.example.demo.model.ArticleInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleMapper {
    // 发布新文章
    public int add(@Param("articleinfo")ArticleInfo articleinfo);
    // 查询指定博客id的博客详情--》用于博客详情页
    public ArticleInfo selectByBlogId(@Param("blogId") int blogId);

    // 查询个人的博客列表——》个人博客详情页
    public List<ArticleInfo> getMyBlogList(@Param("uid") int uid);
    // 查询总的博客列表（无分页功能）
    public List<ArticleInfo> getBlogList();

    // 删除指定博客id的博客
    public int del(@Param("id") int id);

    // 得到当前的博客总数目
    public int getPageCount();

    // 查询总的博客列表（分页功能的实现）_>注意limit和offset的位置（相对位置（参数的相对位置，不要弄反——》查询数据库会出错
    public List<ArticleInfo> getListByPage(@Param("limit") int psize, @Param("offset") int offset);
    // psize对应limit--》表示每页显示的记录条数。pindex对应offset——》表示当前查看的是第几页的数据

    // 修改指定博客id的博客title和content----------->注意形参的顺序
    public int update(@Param("title") String title, @Param("content") String content, @Param("id") int id);

}
