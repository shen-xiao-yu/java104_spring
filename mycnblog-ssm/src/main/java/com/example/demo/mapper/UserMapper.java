package com.example.demo.mapper;

import com.example.demo.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    // 用户注册
    public int reg(@Param("username") String username, @Param("password") String password);


    // 通过名称查询用户-->用于登录的验证
    public UserInfo selectByUsername(@Param("username") String username);

    // 通过个人id(uid)查询个人详细信息
    public UserInfo selectByUid(@Param("uid") int uid);
}
