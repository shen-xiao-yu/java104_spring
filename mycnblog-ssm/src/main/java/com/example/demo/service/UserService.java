package com.example.demo.service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.ArticleInfo;
import com.example.demo.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    // 用户注册
    public int reg(String username, String password) {
        return userMapper.reg(username, password);
    }
    // 通过名称查询用户-->用于登录的验证
    public UserInfo selectByUsername(String username) {
        return userMapper.selectByUsername(username);
    }
    // 通过个人id(uid)查询个人详细信息
    public UserInfo selectByUid(int uid) {
        return userMapper.selectByUid(uid);
    }
}
