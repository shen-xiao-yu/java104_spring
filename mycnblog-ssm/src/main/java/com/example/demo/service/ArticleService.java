package com.example.demo.service;

import com.example.demo.mapper.ArticleMapper;
import com.example.demo.model.ArticleInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    // 发布新的博客
    public int add(ArticleInfo articleInfo) {
        return articleMapper.add(articleInfo);
    }
    // 查看指定用户id的所有文章--》个人博客详情页——》这里的uid其实就是用户表的id
    public List<ArticleInfo> getMyBlogList(int uid) {
        return articleMapper.getMyBlogList(uid);
    }

    // 查询所有用户的博客列表(无分页功能）
    public List<ArticleInfo> getBlogList() {
        return articleMapper.getBlogList();
    }

    // 查看指定博客id的博客详情-->博客详情页
    public ArticleInfo selectByBlogId(int blogId) {
        return articleMapper.selectByBlogId(blogId);
    }
    // 删除指定Id的博客
    public int del(int id) {
        return articleMapper.del(id);
    }
    // 查询博客总数
    public int getPageCount() {
        return articleMapper.getPageCount();
    }
    //  // 查询所有用户的博客列表(分页功能的实现）
    public List<ArticleInfo> getListByPage(int pindex, int psize) {
        return articleMapper.getListByPage(pindex, psize);
    }

    // 修改指定博客id的博客
    public int update(int id, String title, String content) {
        return articleMapper.update(title, content, id);
    }
}
