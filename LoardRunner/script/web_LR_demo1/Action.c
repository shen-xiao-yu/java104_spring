Action()
{
	// 1、访问http://127.0.0.1:1080/WebTours/首页
	web_url("index",
		"URL=http://127.0.0.1:1080/WebTours/",
		"TargetFrame=",
		"Resource=0",
		"Referer=",
		LAST);
	// 在脚本的登录函数前后插入事务lr_start_transaction("login")
	lr_start_transaction("login");
	// 2、输入登录的用户名和密码jojo/bean
	web_submit_form("login",
		ITEMDATA,
		"Name=username", "Value=jojo", ENDITEM,
		"Name=password", "Value=bean", ENDITEM,
		LAST);
	// 插入事务lr_end_transaction("login", LR_AUTO)并执行脚本
	lr_end_transaction("login", LR_AUTO);

	
	return 0;
}
