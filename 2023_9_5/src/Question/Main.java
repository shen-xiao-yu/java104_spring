package Question;

import java.util.Scanner;

public class Main {
    public static int singleNumber(int[] nums) {
        int[] h = new int[100];
        for (int i = 0; i < nums.length; ++i) {
            h[nums[i] - 0]++;
            System.out.println(nums[i] - 0);
        }
        for (int i = 0; i < nums.length; ++i) {
            if (h[nums[i] - 0] == 1) {
                return nums[i];
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        int[] nums = new int[]{2, 3, 4,8};
        int a = 0;
        Scanner in = new Scanner(System.in);

        String currentDirectory = System.getProperty("user.dir");
        System.out.println("当前工作目录：" + currentDirectory);
        System.out.println(singleNumber(nums));
    }
}
