
import javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class CharacterCount {
    public static void main(String[] args) throws IOException {
        // 字符串数组：105个人名或者地名
        String[] characterNames = {"Harry Potter", "Ron Weasley", "Hermione Granger", "Albus Dumbledore", "Gibleroy Lockhart", "Professor Minerva Mcgonagall", "Professor Severus Snape", "Professor Lupin", "Dobby", "Winky", "Voldemort", "Tom Riddle", "Rubeus Hagrid", "Professor Quirrel", "Professor Sprout", "Madam Hooch", "Porfessor Trelawney", "Sirius Black", "Peter Pettigrew", "Professor Flitwick", "Cedric Diggory", "Viktor Krum", "Filch", "Fleur Delacour", "Mad-eye Moody", "Mr Crouch", "Ludo Bagman", "Cornelius Fudge", "Bill Weasley", "Charlie Weasley", "Fred Weasley", "George Weasley", "Ginny Weasley", "Percy Weasley", "Aurthor Weasley", "Molly Weasley", "Vernon Dursley", "Petunia Dursley", "Dudley Dursley", "Cho Chang", "Lavender Brown", "Parvati Patil", "Draco Malfoy", "Fang", "Norbert", "Fluffy", "Aragog", "Crookshanks", "Pigwidgeon", "Scabbers", "Hedwig", "Hannah Abbott", "Millicent Bulstrode", "Justin Finch-Fletchley", "Fawkes", "Griphook", "Ronan", "Bane", "Firenze", "Sir Cadogan", "Amos Diggory", "Igor Karkaroff", "Madam Maxime", "Padma Patil", "Oliver Wood", "Goyle", "Vincent Crabbe", "Madam Pomfrey", "Lee Jordan", "Angelina Johnson", "Seamus Finnigan", "Dean Thomas", "Neville Longbottom", "Ernie Mcmillan", "Colin Creevey", "Dennis Creevey", "James Potter", "Lily Potter", "Katie Bell", "Penelop Clearwater", "Alicia Spinnet", "Professor Binns", "Madam Pince", "Mr Ollivander", "Madam Malkin", "Nicolas Flamel", "Peeves", "Fat Friar", "Moaning Murtle", "Bloody Baron", "Nearly Headless Nick", "Rita Skeeter", "Professor Sinistra", "Professor Grubbly_plank", "Pansy Parkinson", "Bertha Jorkins", "Roger Davis", "Godric Gryffindor", "Helga Hufflepuff", "Rowena Ravenclaw", "Salazar Slytherin", "Moony", "Prongs", "Padfoot", "Wormtail", "Buckbeak"};

        //  Path类是Java NIO.2中提供的一个文件操作类，用于表示文件或目录的路径。
        // 创建一个Path对象filePath，表示名为"src"的目录下的文件"Harry_Potter.txt"
        // 在Paths.get()方法中，我们可以使用绝对路径，Paths.get("src","Harry_Potter.txt")用于获取文件的路径
//        在Java中，Path对象代表了一个文件系统中的路径，可以通过Paths.get()方法获取Path对象。
//
//        该方法接受一个或多个字符串参数，用于指定路径的组成部分。在这里，我们使用了两个参数："src"和"Harry_Potter.txt"。
//        其中，"src"表示项目的源代码目录，而"Harry_Potter.txt"是文件名。
//
//        因此，该语句实际上是将"src"和"Harry_Potter.txt"组合成一个路径，并将其封装成一个Path对象filePath
        Path filePath = Paths.get("src","Harry_Potter.txt");
        // **一种方法是使用Files.readAllLines()方法将文本文件的所有行读取到列表中，然后使用String.join()方法将它们连接成一个字符串：

        /*这行代码使用Java的Files类读取指定文件中的所有行，并将每行内容存储到一个List<String>对象中。
        具体解释如下：
        Files类是Java NIO.2中提供的一个用于文件操作的工具类。
        readAllLines()方法是Files类提供的一个静态方法，用于读取指定文件的所有行。

        方法参数解释：
        filePath：表示要读取的文件路径，它是一个Path对象。
        StandardCharsets.UTF_8：表示要使用的字符编码，这里选择了UTF-8编码。通过StandardCharsets类提供的常量，我们可以指定不同的字符编码。
        运行该方法后，它会返回一个List<String>对象，其中每个元素都代表文件中的一行内容。

        总结：该行代码利用Files类的readAllLines()方法，读取了指定文件（由filePath表示）中的所有行内容，并将每行的内容存储到一个List<String>对象中。
        这样，我们可以通过lines列表来逐行处理文件的内容。请确保在使用该代码之前，已经正确创建了filePath对象并指定了正确的文件路径。*/
        List<String> lines = Files.readAllLines(filePath, StandardCharsets.UTF_8);
        //将List<String>中的每行内容按照"\n"分隔符连接成一个新的字符串
        String text = String.join("\n", lines);
        // 创建了一个HashMap对象count，用于存储每个角色名字及其对应的出现次数。
        // 遍历characterNames数组，将每个角色名字作为键，初始值设为0，并将其存储在count中
        // Map是一个接口类，该类没有继承自Collection，该类中存储的是<K,V>结构的键值对，并且K一定是唯一的，不能重复
        Map<String, Integer> map = new HashMap<>();
        for (String characterName : characterNames) {
            map.put(characterName, 0); //MAP函数给velue赋初值0
        }
        // 创建了一个Scanner对象scanner，并使用其读取text中的每一行。
        Scanner scanner = new Scanner(text);
        /* java.util.Scanner 是 Java5 的新特征，我们可以通过 Scanner 类来获取用户的输入。
        下面是创建 Scanner 对象的基本语法：
        Scanner s = new Scanner(System.in);
        接下来我们演示一个最简单的数据输入，并通过 Scanner 类的 next() 与 nextLine() 方法获取输入的字符串，
        在读取前我们一般需要 使用 hasNext 与 hasNextLine 判断是否还有输入的数据：*/


        while (scanner.hasNextLine()) {
//            然后，遍历characterNames数组，对于每个角色名字，如果该行包含该角色名字，则更新count中该角色对应的值，使其加1。
            String line = scanner.nextLine();
            for (String characterName : characterNames) {
                if (line.contains(characterName)) {
                    //  put(K key, V value) 设置 key 对应的 value
                    map.put(characterName, map.get(characterName) + 1); // get(Object key) 返回 key 对应的 value
                }
            }
        }
// 但是如果Harry Patter这样一个完整的人名被分割在两行呢？会不会造成统计缺少，该如何解决呢
        // 正则表达式可以吗
//        String previousLine = "";
//        while (scanner.hasNextLine()) {
//            String line = scanner.nextLine();
//            String fullLine = previousLine + line; // 将上一行和当前行拼接起来
//            for (String characterName : characterNames) {
//                int index = fullLine.indexOf(characterName);
//                if (index >= previousLine.length()) {
//                    map.put(characterName, map.get(characterName) + 1);
//                }
//            }
//            previousLine = line; // 将当前行保存为上一行
//        }
        // 使用Comparator和Lambda表达式对List中的键值对进行排序
        // Map.Entry<K, V> 是Map内部实现的用来存放<key, value>键值对映射关系的内部类，该内部类中主要提供了<key, value>的获取，value的设置以及Key的比较方式
        // 首先，我们需要将map中的键值对转换为一个List对象。我们可以使用entrySet()方法获取键值对集合，然后将其转换为List。
        // 具体解释如下：HashMap对象可通entrySet()将键值对内容取出返回的是Set<Entry<K,V>>集合，
        // 然后可以同过ArrayList的构造函数（public ArrayList(Collection<? extends E> c)）将set集合转出成List集合
//        **&&&该行代码创建了一个包含了所有键值对的 Set，并将其转化为 ArrayList，ArrayList 中的每个元素都是 Map.Entry<String, Integer> 类型的对象&&**
        List<Map.Entry<String, Integer>> mapList = new ArrayList<>(map.entrySet());
        // 使用Collections.sort()方法和自定义的比较器对List中的键值对进行排序。比较器使用lambda表达式定义，根据键值对的值进行比较。
        // 键不同——》按值降序           || 值相同，按键升序

        // 首先，这是一个Lambda表达式，它接受两个参数entry1和entry2，这两个参数都是Map.Entry类型的对象，分别代表Map中的一个键值对。
        //在Lambda表达式的主体中，首先比较两个entry的值（entry1.getValue()和entry2.getValue()），这是通过调用每个entry的getValue()方法得到的。这两个值被传递给compareTo()方法进行比较。如果这两个值不相等（即compareTo()方法返回的结果不等于0），则根据compareTo()方法的结果进行排序，也就是说，值较大的entry会排在值较小的entry前面。
        //如果这两个值相等（即compareTo()方法返回的结果等于0），则会比较这两个entry的键（entry1.getKey()和entry2.getKey()）。这是通过调用每个entry的getKey()方法得到的。根据键的比较结果进行排序，也就是说，键较小的entry会排在键较大的entry前面。
        //总的来说，这段代码的作用是对一个Map的entrySet进行排序，排序的依据是entry的值和键。
        mapList.sort((entry1, entry2) -> {
            int valueComparison = entry2.getValue().compareTo(entry1.getValue());
            if (valueComparison != 0) {
                return valueComparison;
            } else {
                return entry1.getKey().compareTo(entry2.getKey());
            }
        });
        // 在Java中，compareTo()方法是Object类的一个方法，所有对象都继承了这个方法。对于Integer类，compareTo()方法已经被重写，用于比较两个Integer对象的大小。
        //所以，你不需要在自定义的Comparator类中重写compareTo()方法，可以直接使用entry2.getValue().compareTo(entry1.getValue())进行比较。
        // 如果entry1的值小于entry2的值，compareTo()方法会返回一个负数；
        // 如果entry1的值等于entry2的值，compareTo()方法会返回0；如果entry1的值大于entry2的值，compareTo()方法会返回一个正数。

        // 输出排序后的结果
        for (Map.Entry<String, Integer> entry : mapList) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println(key + " : " + value);
        }


    }






    // 一些前置知识，关于Paths.get()用于创建Path对象。该方法接受一个或多个字符串参数，并将它们组合成一个表示文件或目录路径的Path对象。
    public static void main1(String[] args) throws IOException {
        // （1）创建绝对路径的Path对象：
        // Path filePath = Paths.get("/java-gitee/java104/2023_9_5/src/Harry_Potter.txt");

        // （2）使用相对路径（要清楚当前的工作目录在哪里）
        //  Path filePath = Paths.get("./src/Harry_Potter.txt");
        String currentDirectory = System.getProperty("user.dir");
        System.out.println("当前工作目录：" + currentDirectory);
        Path filePath = Paths.get("src","Harry_Potter.txt");
        //在Paths.get("directory", "subdirectory", "file.txt")这个例子中，"directory"和"subdirectory"都是相对路径中的目录名。这些字符串参数被组合起来形成一个相对路径，最终形成的路径是"directory/subdirectory/file.txt"。
        //具体来说，使用相对路径时，路径是相对于当前工作目录的。如果当前工作目录是"/home/user"，则"directory/subdirectory/file.txt"表示的实际路径是"/home/user/directory/subdirectory/file.txt"。当你使用相对路径时，通常会根据你的代码所在位置来选择一个相对路径
        Path absolutePath = filePath.toAbsolutePath();
        System.out.println("文件路径地址：" + absolutePath.toString());

        if (Files.exists(filePath) && Files.isReadable(filePath)) {
            // 执行文件读取操作
            List<String> lines = Files.readAllLines(filePath, StandardCharsets.UTF_8);
            String text = String.join("\n", lines);
            // 其他操作

        } else {
            System.out.println("文件不存在或不可读。");
        }

    }
}
