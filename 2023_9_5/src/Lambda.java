// 使用Lambda表达式计算正方形的面积
public class Lambda {
    public static void main(String[] args) {
        // 这里的()->就是Lambda表达式，他允许你通过表达式来代替功能接口（你看是不是实现了正方形面积的计算）——》效果就想之前用子类Square重写了calculateArea抽象方法一样
        Shape shape = () -> {
            double len = 5;
            return  len * len;
        };
        double area = shape.calculateArea();
        System.out.println("正方形的面积: " + area);
    }
}