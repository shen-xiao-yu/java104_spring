import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class demo {

    // 使用具体子类 Square 实现 Shape 接口
    static class Square implements Shape {
        private double len = 5; // 自定义正方形的长度

        @Override
        public double calculateArea() {
            return len * len;
        }
    }

    static class AreaCalculator {
        // 这是一个静态方法
        public static double calculateSquareArea() {
            double sideLength = 5.0;
            return sideLength * sideLength;
        }
    }

    public static void main(String[] args) {
        String[] strings = {"Harry Potter", "Ron Weasley", "Hermione Granger", "Albus Dumbledore", "Gibleroy Lockhart", "Professor Minerva Mcgonagall", "Professor Severus Snape", "Professor Lupin", "Dobby", "Winky", "Voldemort", "Tom Riddle", "Rubeus Hagrid", "Professor Quirrel", "Professor Sprout", "Madam Hooch", "Porfessor Trelawney", "Sirius Black", "Peter Pettigrew", "Professor Flitwick", "Cedric Diggory", "Viktor Krum", "Filch", "Fleur Delacour", "Mad-eye Moody", "Mr Crouch", "Ludo Bagman", "Cornelius Fudge", "Bill Weasley", "Charlie Weasley", "Fred Weasley", "George Weasley", "Ginny Weasley", "Percy Weasley", "Aurthor Weasley", "Molly Weasley", "Vernon Dursley", "Petunia Dursley", "Dudley Dursley", "Cho Chang", "Lavender Brown", "Parvati Patil", "Draco Malfoy", "Fang", "Norbert", "Fluffy", "Aragog", "Crookshanks", "Pigwidgeon", "Scabbers", "Hedwig", "Hannah Abbott", "Millicent Bulstrode", "Justin Finch-Fletchley", "Fawkes", "Griphook", "Ronan", "Bane", "Firenze", "Sir Cadogan", "Amos Diggory", "Igor Karkaroff", "Madam Maxime", "Padma Patil", "Oliver Wood", "Goyle", "Vincent Crabbe", "Madam Pomfrey", "Lee Jordan", "Angelina Johnson", "Seamus Finnigan", "Dean Thomas", "Neville Longbottom", "Ernie Mcmillan", "Colin Creevey", "Dennis Creevey", "James Potter", "Lily Potter", "Katie Bell", "Penelop Clearwater", "Alicia Spinnet", "Professor Binns", "Madam Pince", "Mr Ollivander", "Madam Malkin", "Nicolas Flamel", "Peeves", "Fat Friar", "Moaning Murtle", "Bloody Baron", "Nearly Headless Nick", "Rita Skeeter", "Professor Sinistra", "Professor Grubbly_plank", "Pansy Parkinson", "Bertha Jorkins", "Roger Davis", "Godric Gryffindor", "Helga Hufflepuff", "Rowena Ravenclaw", "Salazar Slytherin", "Moony", "Prongs", "Padfoot", "Wormtail", "Buckbeak"};
        System.out.println(strings.length);


    }


    public static void main1(String[] args) {
        Square square1 = new Square();
        System.out.println("1、采用Square子类实现Shape接口的方式，计算正方形的面积为：" + square1.calculateArea());

        // 这里的()->就是Lambda表达式，他允许你通过表达式来代替功能接口（你看是不是实现了正方形面积的计算）——》效果就想之前用子类Square重写了calculateArea抽象方法一样
        Shape shape1 = () -> {
            double len = 5;
            return  len * len;
        };
        double area = shape1.calculateArea();
        System.out.println("2、采用Lambda表达式的方式计算，正方形的面积: " + area);

        // 我们之前不是说过Shape是一个接口，不能实例化吗，但是有没有发现这里的new Shape();后面还有一个大括号，大括号里面还有方法的重写
        Shape shape2 = new Shape() {
            private double sideLength = 5.0;

            @Override
            public double calculateArea() {
                return sideLength * sideLength;
            }
        };
        System.out.println("3、采用匿名内部类的方式计算，正方形的面积: " + shape2.calculateArea());

        Shape square2 = AreaCalculator::calculateSquareArea;
        System.out.println("4、采用方法引进计算，正方形的面积: " + square2.calculateArea());
    }


}
