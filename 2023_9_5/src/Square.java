// 使用具体子类 Square 实现 Shape 接口
public class Square implements Shape{
    private double len = 5; // 自定义正方形的长度
    @Override
    public double calculateArea() {
        return len * len;
    }

    public static void main(String[] args) {
        Square square = new Square();
        System.out.println("正方形的面积为：" + square.calculateArea());
    }
}
