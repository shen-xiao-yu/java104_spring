package demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CharacterNameCounter {
    public static void main(String[] args) {
        String filePath = "./src/demo/Harry_Potter.txt"; // Replace with the actual file path
        String currentDirectory = System.getProperty("user.dir");
        System.out.println("当前工作目录：" + currentDirectory);
        Map<String, Integer> nameCounts = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                if (!line.isEmpty()) {
                    String[] names = line.split("\\s+");
                    for (String name : names) {
                        if (nameCounts.containsKey(name)) {
                            nameCounts.put(name, nameCounts.get(name) + 1);
                        } else {
                            nameCounts.put(name, 1);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Map.Entry<String, Integer> entry : nameCounts.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}