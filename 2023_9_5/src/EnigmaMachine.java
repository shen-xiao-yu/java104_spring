
import java.util.ArrayList;
import java.util.List;

public class EnigmaMachine {

    static class Rotor{
        private final String wiring;
        private final int notch;
        private int offset = 0;
        public Rotor(String wiring, int notch){
            this.wiring = wiring;
            this.notch = notch;
        }
        public boolean rotate(){
            offset = (offset + 1) % 26;
            return offset == notch;
        }
        public int encode(int n){
            int i=(n + offset) % 26;
            char c = wiring.charAt(i);
            int j=(c-'A' -offset +26)%26;
            return j;
        }
        public int reverseEncode(int n){
            int i= (n + offset) % 26;
            char c=(char)('A' +i);
            int j= wiring.indexOf(c);
            j= (j - offset + 26) % 26;
            return j;
        }
    }
    static class Reflector{
        private final String wiring;

        public Reflector(String wiring){
            this.wiring = wiring;
        }
        public int reflect(int n) {
            char c=(char)('A' + n);
            int i= wiring.indexOf(c);
            return (i-'A' +26)%26;
        }
    }
    private final List<Rotor> rotors;
    private final Reflector reflector;

    public EnigmaMachine(List<Rotor> rotors, Reflector reflector){
        this.rotors = rotors;
        this.reflector = reflector;
    }
    public String encrypt(String plainText) {
        StringBuilder cipherText = new StringBuilder();
        for (char c :plainText.toUpperCase().toCharArray()){
            if(Character.isLetter(c)){
                rotateRotors();
                int n=c-'A';
                for (Rotor rotor : rotors){
                    n = rotor.encode(n);
                }
                n = reflector.reflect(n);
                for (int i= rotors.size() - 1; i>= 0; i--){
                    n =rotors.get(i).reverseEncode(n);
                }
                char encryptedChar =(char)(n +'A');
                cipherText.append(encryptedChar);
            }
        }
        return cipherText.toString();
    }
    private void rotateRotors(){
        boolean shouldRotateNextRotor = true;
        for(int i=rotors.size() - 1;i>= 0 && shouldRotateNextRotor;i--){
            Rotor rotor = rotors.get(i);
            shouldRotateNextRotor = rotor.rotate();
        }
    }
    public static void main(String[] args){
        Rotor rotorl = new Rotor("EKMFLGDQVZNTOWYHXUSPAIBRCJ",16);
        Rotor rotorll = new Rotor("AJDKSIRUXBLHWTMCQGZNPYFVOE",4);
        Rotor rotorlll = new Rotor("BDFHJLCPRTXVZNYEIWGAKMUSQO",21);
        Reflector reflectorB = new Reflector("YRUHQSLDPXNGOKMIEBFZCWVJAT");
        List<Rotor> rotors = new ArrayList<>();
        rotors.add(rotorl);
        rotors.add(rotorll);
        rotors.add(rotorlll);
        EnigmaMachine machine = new EnigmaMachine(rotors, reflectorB);
        String plainText="HELLOWORLD";
        System.out.println("明文:"+ plainText);
        String cipherText =machine.encrypt(plainText);
        System.out.println("加密后:"+cipherText);
    }
}