// 匿名内部类的实现
public class Anonymous {

    public static void main(String[] args) {
        // 我们之前不是说过Shape是一个接口，不能实例化吗，但是有没有发现这里的new Shape();后面还有一个大括号，大括号里面还有方法的重写
        Shape shape = new Shape() {
            private double sideLength = 5.0;

            @Override
            public double calculateArea() {
                return sideLength * sideLength;
            }
        };

        double area = shape.calculateArea();
        System.out.println("正方形的面积: " + area);
    }
}
/**
 * 我们创建了一个匿名内部类来实现 Shape 接口（或抽象类）并重写 calculateArea() 方法。（大括号里面的）
 * 然后我们使用该匿名内部类创建了一个对象，并通过调用 calculateArea() 方法来计算面积。
 *
 *
 * 匿名内部类应该是一个接口的实现类或抽象类的实现类，而不是直接实例化一个类
 * ————这里面我们明明实现了该抽象类Shape，但是好像明没有写出实现这个抽象类的子类的名字，不像之前我们用 Square子类实现Shape接口的时候（重写其中的calculateArea()方法），指定了这个子类名字是Square
    这里实现该接口的类没有名字，所以叫匿名内部类
 */