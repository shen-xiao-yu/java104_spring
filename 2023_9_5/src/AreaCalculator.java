//使用方法引用实现 Shape 接口
public class AreaCalculator {
    // 这是一个静态方法
    public static double calculateSquareArea() {
        double sideLength = 5.0;
        return sideLength * sideLength;
    }

    public static void main(String[] args) {
        // 方法引用 —————— 静态方法引用：类名::静态方法名 （我们上面的calculateSquareArea就是一个静态方法，不依赖与对象，直接用类名）
        // 通过方法引用的方式，将 AreaCalculator 类中的静态方法 calculateSquareArea 与 Shape 接口关联起
        /**
         * 具体来说，代码 Shape square = AreaCalculator::calculateSquareArea;
         * 表示将 calculateSquareArea 方法作为 Shape 接口中的 calculateArea 方法的实现。
         * 这里使用了静态方法引用的语法：类名::静态方法名。
         */
        Shape square = AreaCalculator::calculateSquareArea;
        double area = square.calculateArea();
        System.out.println("正方形的面积: " + area);
    }
}
/**
 * 在 AreaCalculator 类中，我们实现了 calculateSquareArea() 方法来计算正方形的面积。
 *
 * 在 main() 方法中，我们使用方法引用 AreaCalculator::calculateSquareArea 来实例化 Shape 接口，
 * 将其赋值给 square 变量。最后，通过调用 square.calculateArea() 方法来计算正方形的面积并输出结果。
 */
