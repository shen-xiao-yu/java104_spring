package com.example.demo.mapper;
import com.example.demo.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    // 登录
    public UserInfo checkLogin(@Param("username") String username);

    // 注册
    public int reg(@Param("username") String username, @Param("password") String password);



}
