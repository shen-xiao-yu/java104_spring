package com.example.demo.mapper;

import com.example.demo.model.LoveMusicInfo;
import com.example.demo.model.MusicInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LoveMusicMapper {
    // 添加指定musicId的音乐到喜欢列表
    int add(@Param("musicId")int musicId, @Param("userId") int userId);

    // 查询喜欢的音乐）如果没有传入具体的歌曲名，显示当前用户收藏的所有音乐
    List<MusicInfo> findLoveMusicByUserId(@Param("userId") int userId);

    // 查询喜欢列表中的指定音乐（按音乐名称和当前用户的userid)
    List<MusicInfo> findLoveMusicByName(@Param("title") String title, @Param("userId") int userId);


    // 检查指定musicId的音乐是否存在于喜欢列表(注意喜欢表中可能出现musicId相同的歌曲，但是是属于不同的人所喜欢的，所以对应的记录也是不同的）
    LoveMusicInfo loveMusicCheck(@Param("musicId") int musicId, @Param("userId") int userId);

    // 将指定musicId的音乐从喜欢列表移除
    int deleteLoveMusic(@Param("musicId") int musicId);
}
