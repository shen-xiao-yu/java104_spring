package com.example.demo.mapper;

import com.example.demo.model.MusicInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

@Mapper
public interface MusicMapper {
    int upload(@Param("title") String title, @Param("singer") String singer, @Param("time") String time, @Param("url") String url, @Param("userid") int userid);

    // 根据音乐名和歌手查找对应音乐
    MusicInfo musicCheck(@Param("title") String title, @Param("singer") String singer);

    // 根据musicId查找对应音乐
    MusicInfo musicFind(@Param("musicId") int musicId);

    // 根据音乐名和歌手删除指定音乐
    int deleteMusic(@Param("title") String title, @Param("singer") String singer);

    // 根据musicId删除指定音乐
    int deleteMusicById(@Param("musicId") int musicId);


    // 查找指定姓名的音乐(可能存在同名的音乐）
    List<MusicInfo> findMusicByName(@Param("title") String title, @Param("limit") int limit, @Param("offset") int offset);
    // 音乐数量统计
    int musicCount(@Param("title") String title);
}
