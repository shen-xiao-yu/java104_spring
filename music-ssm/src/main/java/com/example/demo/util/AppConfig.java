package com.example.demo.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


// 系统配置文件类
@Configuration // 该配置文件在项目启动时配置
public class AppConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor; // 用户登录拦截器
    // 自定义拦截器添加到系统的配置文件中

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")       // 设置拦截规则
                .excludePathPatterns("/**/*.html")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/user/reg")
                // 总的音乐列表，我们需要开放权限，即使用户未登录也可以查看
                .excludePathPatterns("/music/findMusicByName") // 放行总博客列表
                .excludePathPatterns("/music/get")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/player/**")
                .excludePathPatterns("/fonts/**")
                .excludePathPatterns("/images/**"); // 直接img目录下的所有都放行

    }
}
