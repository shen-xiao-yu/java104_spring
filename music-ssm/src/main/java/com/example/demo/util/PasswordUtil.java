package com.example.demo.util;

import cn.hutool.crypto.SecureUtil;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordUtil {
    public static final String salt = "12dijsfd";

    // 第一次加密（前端对用户的输入进行加密）
    public static String inputPasswordToFormPassword(String input) {
        String temp = "" +  salt.charAt(1) + salt.charAt(5) + input + salt.charAt(7);
        return SecureUtil.md5(temp);
    }
    // 加密
    public static String encrypt(String input) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(input);
    }
    // 解密
    public static boolean decrypt(String input, String password) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.matches(input, password);
    }

    public static void main(String[] args) {
        String formPassword = inputPasswordToFormPassword("123456");
        System.out.println(formPassword);
    }

}
