package com.example.demo.util;

import com.example.demo.util.ConstVariable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            // 当前用户未登录，进行拦截--->跳转到登录页面--->后端我们只用给前端返回状态和数据，我们后端不用管页面怎么，跳转工作是前端的事情
            // response.sendRedirect("/login.html");
            // 401表示未登录，没有权限，403不是因为没有登录而没有权限，而是该用户没有权限，管理员和普通用户的区别
            response.setStatus(401);
            return false;
        }
        return true;
    }
}
