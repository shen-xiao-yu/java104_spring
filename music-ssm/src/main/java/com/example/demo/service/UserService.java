package com.example.demo.service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    public UserInfo checkLogin(String username) {
        return userMapper.checkLogin(username);
    }
    public int reg(String username, String password) {
        return userMapper.reg(username, password);
    }
}
