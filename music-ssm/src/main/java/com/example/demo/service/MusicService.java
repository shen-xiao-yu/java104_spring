package com.example.demo.service;

import com.example.demo.mapper.MusicMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.MusicInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MusicService {
    @Autowired
    private MusicMapper musicMapper;
    // 上传音乐
    public int upload(String title, String singer, String time, String url, int userid) {
        return musicMapper.upload(title, singer, time, url, userid);
    }
    // 检查音乐表中是否有对应的音乐(通过音乐名和歌手来检索）
    public MusicInfo musicCheck(String title, String singer) {
        return musicMapper.musicCheck(title, singer);
    }
    // 查找音乐表中是否有指定musicId的音乐记录（通过音乐的id来查找）
    public MusicInfo musicFind(int musicId) {
        return musicMapper.musicFind(musicId);
    }

    // 删除指定音乐名和歌手的音乐
    public int deleteMusic(String title, String singer) {
        return musicMapper.deleteMusic(title, singer);
    }
    // 删除指定musicId的音乐
    public int deleteMusicById(int musicId) {
        return musicMapper.deleteMusicById(musicId);
    }

    // 查找指定姓名的音乐
    public List<MusicInfo> findMusicByName(String title, int limit, int offset) {
        return musicMapper.findMusicByName(title, limit, offset);
    }
    // 音乐数量统计
    public int musicCount(String title) {
        return musicMapper.musicCount(title);
    }

}
