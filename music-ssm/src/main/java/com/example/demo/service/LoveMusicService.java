package com.example.demo.service;

import com.example.demo.mapper.LoveMusicMapper;
import com.example.demo.model.LoveMusicInfo;
import com.example.demo.model.MusicInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoveMusicService {
    @Autowired
    private LoveMusicMapper loveMusicMapper;
    // 检查指定musicId的音乐是否在喜欢列表中
    public LoveMusicInfo loveMusicCheck(int musicId, int userId) {
        return loveMusicMapper.loveMusicCheck(musicId, userId);
    }
    // 添加指定musicId的音乐到喜欢列表（我们在controller层通过
    public int add(int musicId, int userId) {
        return loveMusicMapper.add(musicId, userId);
    }
    // 检索当前用户的所有喜欢的音乐
    public List<MusicInfo> findLoveMusicByUserId(int userId) {
        return loveMusicMapper.findLoveMusicByUserId(userId);
    }
    // 检索查询当前用户喜欢的指定名称的音乐（支持模糊查询）
    public List<MusicInfo> findLoveMusicByName(String title, int userId) {
        return loveMusicMapper.findLoveMusicByName(title, userId);
    }
    // 将指定musicId的音乐从喜欢列表移除
    public int deleteLoveMusic(int musicId) {
        return loveMusicMapper.deleteLoveMusic(musicId);
    }
}
