package com.example.demo.model;

import lombok.Data;

@Data
public class MusicInfo {
    private int id;
    private String title;
    private String singer;
    private String time;
    private String url;
    private int userid;
}

