package com.example.demo.model;

import lombok.Data;

@Data
public class LoveMusicInfo {
    private int id;
    private int user_id;
    private int music_id;
}
