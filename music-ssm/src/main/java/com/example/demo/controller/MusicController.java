package com.example.demo.controller;

import com.example.demo.mapper.MusicMapper;
import com.example.demo.model.LoveMusicInfo;
import com.example.demo.model.MusicInfo;
import com.example.demo.model.UserInfo;
import com.example.demo.service.LoveMusicService;
import com.example.demo.service.MusicService;
import com.example.demo.service.UserService;
import com.example.demo.util.ConstVariable;
import com.example.demo.util.ResponseBodyMessage;
import com.sun.corba.se.impl.ior.OldJIDLObjectKeyTemplate;
import org.apache.ibatis.binding.BindingException;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.BindException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.tika.Tika;
@RestController
@RequestMapping("/music")
public class MusicController {
    @Autowired
    private MusicService musicService;
    @Autowired
    private LoveMusicService loveMusicService;
    @Value("${music.local.path}")
    private String UPLOAD_PATH; /*= "D:/music_ssm_files/";*/

    Set<Map.Entry<Integer, Integer>> set = new HashSet<>();
    /**
     *     上传音乐文件
      */
    @RequestMapping("/upload")
    public Object upload(HttpServletRequest request,
                         String singer, @RequestPart("filename")MultipartFile file,
                        HttpServletResponse response) throws IOException, CannotReadException, TagException, InvalidAudioFrameException, ReadOnlyFileException {

        // 1.先检查是否成功登录
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null ) {
            return new ResponseBodyMessage<>(-10, "请登录后再上传", false);
        }

        // 程序走的这里说明用户已经登录了(继续检测参数传递是否正确
        if (!StringUtils.hasLength(singer)) {
            return new ResponseBodyMessage<>(-1, "参数传递错误", false);
        }
        /**
         * todo
         * 做上传的校验（如果用户上传了一个图片（后缀也改成了mp3格式）
         */


// 创建 Tika 实例
        Tika tika = new Tika();

// 从 InputStream 中提取文件类型
        String mimeType = tika.detect(file.getInputStream());

// 检查文件类型是否是 MP3
        boolean isMp3File = "audio/mpeg".equals(mimeType);
        if (isMp3File == false) {
            return  new ResponseBodyMessage<>(-100, "你上传的文件格式不正确，请重新上传",  false);
        }

        // 2.先查询数据库中是否有当前音乐（题目和歌手是否是同一个）
        // 获取文件名称
        String fileNameAndType = "test.mp3";
        fileNameAndType = file.getOriginalFilename();
        // 获取上传的歌名
        int index =fileNameAndType.lastIndexOf(".");
        if (index == -1) {
            // 文件名不包含后缀名
            return new ResponseBodyMessage<>(-1, "文件名格式不正确", false);
        }
        String title = fileNameAndType.substring(0, index);
        System.out.println(title);
        /// 通过歌曲名和歌手来查询数据库中是否有当前要上传的音乐（如果歌曲名和歌手一样，我们认为是同一首歌曲）
        MusicInfo musicInfo  = musicService.musicCheck(title, singer);
        if (musicInfo != null) {
            return new ResponseBodyMessage<>(-4, "要上传的音乐已经存在，请勿重复上传", false);
        }
        // 3.确认是新音乐后， 将音乐上传到服务器
        String path = UPLOAD_PATH + fileNameAndType;
        System.out.println(path);
        File dest = new File(path);

        if (!dest.exists()) {
            dest.mkdir();
        }
        // 判断文件大小是否超出限制
        if (file.getSize() > 20 * 1024 * 1024) {
            return new ResponseBodyMessage<>(-3, "上传文件大小不能超过20MB", false);
        }
        // 文件上传
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseBodyMessage<>(-1,"服务器上传失败！",false);
        }


        // 4. 上传到数据库中（音乐相关信息——title、singer、time、url、userid）
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String time = simpleDateFormat.format(new Date());
        String url = "/music/get?path=" + title;
        UserInfo userInfo = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
        int userid = userInfo.getId();
        System.out.println("singer: " + singer);


        // 另外的一个问题：  如果重复上传一首歌曲，能否上传成功？？ 可以
        // 在这里我们针对文件上传musicService.upload，加一个异常判断更加合理
        try {
            int res = musicService.upload(title, singer, time, url, userid);
            if (res == 1) {
                // 这里应该跳转到音乐列表页面（但是这里我们在前端进行了对应的跳转）
//                response.sendRedirect("/list.html");
                return  new ResponseBodyMessage<>(200, "上传成功", true);
            }
            else {
                // 上传失败，上传上传到服务器的文件
                return new ResponseBodyMessage<>(-1, "上传失败", false);
            }
        }
        catch (BindingException e) {
            // 非受查异常，程序在运行的时候出现的异常（需要手动
            // 上传失败，抛出异常（把刚刚上传到服务器的音乐给删掉）
            dest.delete();
            return new ResponseBodyMessage<>(-2, "出现异常，上传失败", false);

        }


    }
    /**
     * 音乐播放——音乐上传路径（/music/get?xx.mp3
     * 思考：如果是非mp3格式的文件能否播放——》我们需要上传的时候就杜绝这种情况（通过TAG标签）
     */
    // tips: 这里的音乐播放是在音乐上传的基础上进行的, 我们再前端发送了这样的请求
//    http://127.0.0.1:9090/music/get?path=%E5%90%91%E4%BA%91%E7%AB%AF.mp3，此时title中已经带了.mp3
    @RequestMapping("/get")
    public ResponseEntity<byte[]> getMusic(@RequestParam("path") String title) { // 因为这里前端传递的参数为path
        // 播放音乐不需要登录后才能进行
        if (!StringUtils.hasLength(title)) {
            System.out.println("参数错误");
            return ResponseEntity.badRequest().build();
        }
//        byte[] a = {95, 96,97,98}; // 对应的字节码（ASCII码）
//        return ResponseEntity.internalServerError().build();
        // 这里的path值就是我们音乐文件的存储地址+数据库中音乐的名字+.mp3
        System.out.println(title);
        File file = new File(UPLOAD_PATH + title);
        byte[] a = null;
        try {
            a = Files.readAllBytes(file.toPath());
            if (a == null) {
                // 返回500
                return ResponseEntity.badRequest().build();
            }
            return ResponseEntity.ok(a);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();

    }

    /**
     * 删除音乐（按musicId)
     * （同时喜欢列表中的对应记录也应该同步删除）
     */
    @RequestMapping("/deleteMusicById")
    // 后端参数重命名（后端参数映射：@RequestParam）
    // 思考一点，是否要通过@RequestParm("")注解来指定参数名称，如果不指定，前端传递的参数要和controller函数参数列表中的参数名称一致
    public Object deleteMusicById(HttpServletRequest request, @RequestParam("id") Integer musicId) {
        if (musicId == null || musicId <= 0) {
            return new ResponseBodyMessage<>(-1, "参数传递错误", false);
        }
        // 1. 检查是否登录
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            return new ResponseBodyMessage<>(-2, "用户未登录，请登录后再执行删除操作", false);
        }
        // 2. 判断要删除的音乐是否在数据库中存在（能否先判断该音乐是否在服务器中存储（对应目录中是否有该音乐））
        MusicInfo musicInfo = musicService.musicFind(musicId);
        if (musicInfo == null) {
            return new ResponseBodyMessage<>(-4, "要删除的音乐在数据库中不存在", false);
        }
        // tips:你只能删除你所添加的音乐（等等）——
        // 3.1 从服务器上删除对应的音乐文件（对应的目录中删除该音乐文件）
        String title = musicInfo.getTitle();
        String path = UPLOAD_PATH + title + ".mp3"; // 这里的路径："D:\music_ssm_files\向云端.mp3"
        System.out.println(path);
        File dest = new File(path);
        if (dest.delete()) {
            System.out.println("删除音乐成功");
        }
        else {
            return new ResponseBodyMessage<>(-8, "服务器中该音乐文件删除失败", false);
        }
        // 3.2 把该条音乐记录从数据库中删除(用户只能删除自己上传的音乐，在每个音乐中有编号）
        UserInfo userInfo = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
        int userId = userInfo.getId();
        int res1 = musicService.deleteMusicById(musicId);
        if (res1 == 0) {
            return new ResponseBodyMessage<>(-5, "音乐表中该条音乐记录删除失败", false);
        }
        // 是不是不用同步删除喜欢表中的记录

        // 对应删除喜欢列表的该歌曲的记录的时候，要确保该喜欢表中有该记录（有可能我们已经在喜欢列表中已经移除该记录了）
        // 注意这里删除的可能是多条记录（因为同一首音乐可能被多个不同的用户所收藏）
        LoveMusicInfo loveMusicInfo = loveMusicService.loveMusicCheck(musicId, userId);
        // 只有当喜欢表中有该记录的时候才会删除
        if (loveMusicInfo != null) {
            int res2 = loveMusicService.deleteLoveMusic(musicId);
            if (res2 == 0) {
                return new ResponseBodyMessage<>(-5, "音乐喜欢表中该条音乐记录删除失败", false);
            }
        }
        // 但其他


        return new ResponseBodyMessage<>(200, "删除成功", res1);


    }

    /**
     * 批量删除音乐（按musicId)
     * （同时喜欢列表中的对应记录也应该同步删除）
     */
    @RequestMapping("/deleteMusicArrById")
    public Object deleteMusicArrById(HttpServletRequest request, @RequestParam("id[]") List<Integer> arr) {

        System.out.println("所有的ID: " + arr);

        // 1. 检查是否登录
        int sum = 0;
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            return new ResponseBodyMessage<>(-2, "请登录后再进行批量删除", false);
        }
        for (int i = 0; i < arr.size(); ++i) {
            //2. 检查要删除的音乐文件是否在服务器上存在（是否在数据中存在该条记录）
            MusicInfo musicInfo = musicService.musicFind(arr.get(i));
            if (musicInfo == null) {
                return new ResponseBodyMessage<>(-4, "要删除的音乐在数据库中不存在", false);
            }
            // 3.1 从服务器上删除对应的音乐文件（对应的目录中删除该音乐文件）
            String title = musicInfo.getTitle();
            String path = UPLOAD_PATH + title + ".mp3"; // 这里的路径："D:\music_ssm_files\向云端.mp3"
            System.out.println("当前要删除的音乐路径为：" + path);
            File dest = new File(path);
            if (dest.delete()) {
                System.out.println("服务器上删除该音乐成功");
            }
            else {
                return new ResponseBodyMessage<>(-8, "服务器中该音乐文件删除失败", false);
            }
            // 3.2 把该条音乐记录从数据库中删除
            int res1 = musicService.deleteMusicById(arr.get(i));
            if (res1 == 0) {
                return new ResponseBodyMessage<>(-5, "音乐表中该条音乐记录删除失败", false);
            }

            sum += res1;
        }
        if (sum == arr.size()) {
            System.out.println("批量删除音乐成功");
            return new ResponseBodyMessage<>(200, "批量删除音乐成功", sum);
        }
        else {
            System.out.println("批量删除音乐失败");
            return new ResponseBodyMessage<>(-10, "批量删除音乐失败", false);
        }
    }


    /**
     * 查找音乐（当用户搜索关键字为空的时候，默认查找全部的音乐）
     */
    @RequestMapping(value = "/findMusicByName", produces = "application/json; charset=UTF-8")
    public Object findMusicByName(
                                  @RequestParam(required = false, name = "name") String title,
                                  Integer pindex) {
        try {
            title = URLDecoder.decode(title, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // 处理异常
            System.out.println(e);
            return new ResponseBodyMessage<>(-50, "参数字符编码异常，重新输入", false);
        }
        System.out.println(title);
        int psize = 5;
        HashMap<String, Object> data = new HashMap<>();
        // 登录进去后，第一次初始化页面时候url = list.html，即pindex是空的，username等也是空的
        if (pindex == null || pindex < 1) {
             pindex = 1;
        }
        if (!StringUtils.hasLength(title)) {
            title = "";
        }
        // 上面这些特殊情况的处理，很重要（就是应对url = list.html这种情况，如果没有特判，会出错（查询不到数据）
        // 我们前后端都对这些特殊情况做了处理——不多余（双重保障，你进入公司后前端可不是你写的，所以我们就要把我们负责的后端写的扎实（不管前端怎么传，我后端都能应对）
        int offset = (pindex - 1) * psize;

        int limit = psize;
        // 我们允许用户带未登录的情况下进行搜索
        List<MusicInfo> ret = null;
        try {
            ret = musicService.findMusicByName(title, limit, offset);
            System.out.println("limit: " + limit + "  offset: " + offset);
            System.out.println(ret);
            data.put("list", ret);
            int retCount = musicService.musicCount(title);
            data.put("count", retCount);
            if (ret == null) {
                return new ResponseBodyMessage<>(200, "搜索结果未空", false);
            }
        }
        catch (Exception e) {
            System.out.println(e);
            return new ResponseBodyMessage<>(-1, "从数据中查找失败", false);
        }
        return new ResponseBodyMessage<>(200, "查找指定的全部音乐数据成功", data);
    }
}

