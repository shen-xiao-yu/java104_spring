package com.example.demo.controller;

import com.example.demo.model.UserInfo;
import com.example.demo.service.UserService;
import com.example.demo.util.ConstVariable;
import com.example.demo.util.PasswordUtil;
import com.example.demo.util.ResponseBodyMessage;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/reg")
    public Object reg(String username, String password1, String password2) {
        System.out.println(username);
        System.out.println(password1);
        System.out.println(password2);
        HashMap<String, Object> result = new HashMap<>();

        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password1) || !StringUtils.hasLength(password2)) {
            return new ResponseBodyMessage<>(-1, "参数传递错误", "");
        }
        else {
            if (!password1.equals(password2)) {
                return new ResponseBodyMessage<>(-1, "前后密码不一致", "");
            }
            else {
                // 先要检查该用户名是否已经被注册过
                UserInfo userInfo = userService.checkLogin(username);
                if (userInfo != null) {
                    return new ResponseBodyMessage<>(-100, "该用户名已经被注册，请重新输入用户名", false);
                }
                int res = userService.reg(username, PasswordUtil.encrypt(password1));
                if (res == 1) {
                    return new ResponseBodyMessage<>(200, "注册成功", res);
                }
                else {
                    return new ResponseBodyMessage<>(-2, "注册失败", "");
                }

            }
        }
    }
    @RequestMapping("/login")
    public Object login(HttpServletRequest request,  String username, String password) {
        System.out.println(username);
        System.out.println(password);
        HashMap<String, Object> result = new HashMap<>();
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password)) {
            return  new ResponseBodyMessage<>(-1, "参数输入错误", "");
        }
        // 这里你通过用户名来从数据库中获取对应的记录
        // 验证通过PasswordUtil的解密函数，验证用户输入的明文密码和数据库中存储的密文密码是否一致（是相互对应的）
        UserInfo userInfo = userService.checkLogin(username);// 主要这里我们每个用户名都是唯一的，所有在注册的时候要进行校验
        if (userInfo == null || !PasswordUtil.decrypt(password, userInfo.getPassword()) || userInfo.getId() <= 0) {
            return  new ResponseBodyMessage<>(-2, "登录失败", "");
        }
        else {
            // 如果程序走到这里，说明登陆成功了
            // 每次登录成功都要对数据库中的密码进行重新生成,这样才符合随机加密
            // userService.passwordRefresh()
            HttpSession session = request.getSession(); // 获取session，如果没有就创建一个

            session.setAttribute(ConstVariable.USER_SESSION_KEY, userInfo);
            return  new ResponseBodyMessage<>(200, "登录成功", 1);
        }
    }
    /**
     * 退出登录
     * @param request
     * @param response
     */
    @SneakyThrows
    @RequestMapping("/logout")
    public Object logout(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            // response.sendRedirect("/login.html"); // 当前用户未登录
            return new ResponseBodyMessage<>(-2, "当前用户未登录", false);
        }
        session.setAttribute(ConstVariable.USER_SESSION_KEY, null);
        //response.sendRedirect("/login.html");
        return new ResponseBodyMessage<>(200, "当前状态正常，可以正常退出", true);
    }

}
