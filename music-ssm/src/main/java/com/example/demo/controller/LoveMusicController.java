package com.example.demo.controller;

import com.example.demo.model.LoveMusicInfo;
import com.example.demo.model.MusicInfo;
import com.example.demo.model.UserInfo;
import com.example.demo.service.LoveMusicService;
import com.example.demo.service.MusicService;
import com.example.demo.util.ConstVariable;
import com.example.demo.util.ResponseBodyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.util.List;

@RestController
@RequestMapping("/loveMusic")
public class LoveMusicController {

    @Autowired
    LoveMusicService loveMusicService;
    @Autowired
    MusicService musicService;
    /**
     * 添加指定的音乐到喜欢列表（根据music_id)
     */
    @RequestMapping("/add")
    public Object add(HttpServletRequest request, @RequestParam("id") Integer musicId) {
        if (musicId == null || musicId <= 0) {
            return new ResponseBodyMessage<>(-4, "参数传递错误", false);
        }
        // 检查是否登录
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            return new ResponseBodyMessage<>(-2, "当前未登录，请登录后再试", false);
        }

        // 先要看要添加的音乐在音乐表中是否存在
        MusicInfo musicInfo = musicService.musicFind(musicId);
        if (musicInfo == null) {
            return new ResponseBodyMessage<>(-1, "数据异常，你要添加的音乐不存在", false);
        }
        // 如果存在，接着看该音乐是否已经被添加到了喜欢列表中了
        // 这里我们不应该通过musicId检查该音乐是否已经被添加到lovemusic表中了，因为这个是面向多用户的，
        // 我们只要当这个MusiID和userID同时存在于该lovemusic表中的时候，才能说明该该音乐已经被该用户添加到喜欢列表中了
        UserInfo userInfo = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
        int userId = userInfo.getId();
        System.out.println(userId);
        LoveMusicInfo loveMusicInfo = loveMusicService.loveMusicCheck(musicId, userId);
        if (loveMusicInfo != null) {
            return new ResponseBodyMessage<>(-7, "当前音乐已经添加进喜欢列表，请勿重复添加", false);
        }
        // 添加该音乐到喜欢列表
        int ret = loveMusicService.add(musicId, userId);
        if (ret == 0) {
            return new ResponseBodyMessage<>(-10, "添加失败", false);
        }
        return new ResponseBodyMessage<>(200, "添加成功", true);
    }
    /**
     * 对自己喜欢的音乐列表进行查询（按音乐名称）
     *
     */
    @RequestMapping("/findLoveMusic")
    public Object findLoveMusic(HttpServletRequest request, @RequestParam(required = false, name = "musicName") String title) {
        HttpSession session = request.getSession(false);
        // 因为我们这里查询的是用户个人所喜欢的音乐，错译此时用户必须是已经登录的
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            return new ResponseBodyMessage<>(-2, "当前用户未登录，请登录后再试！", false);
        }
        UserInfo userInfo = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
        int userId = userInfo.getId();
        // 查询不要求必须是在登录状态
        if (userId <= 0) {
            return new ResponseBodyMessage<>(-1, "查询喜欢列表时，参数传递错误", false);
        }
        // 当用户没有输入指定的喜欢的歌曲的姓名，默认检索当前用户的所有喜欢歌曲
        if (title == null) {
            List<MusicInfo> ret = loveMusicService.findLoveMusicByUserId(userId);
            if (ret == null) {
                return new ResponseBodyMessage<>(-2, "从数据库中查询失败", false);
            }
            return new ResponseBodyMessage<>(200, "查询自己喜欢的音乐成功", ret);
        }
        // 在喜欢的音乐列表中按音乐名称和当前用户的用户id来进行查找（支持模糊查询）
        else {
            List<MusicInfo> ret = loveMusicService.findLoveMusicByName(title, userId);
            if (ret == null) {
                return new ResponseBodyMessage<>(-2, "从数据库中查询失败", false);
            }
            return new ResponseBodyMessage<>(200, "查询自己喜欢的指定名称的音乐成功", ret);
        }
    }
    /**
     * 将指定musicId的音乐从喜欢列表中移除
     */
    @RequestMapping("/deleteLoveMusic")
    public Object deleteLoveMusic(HttpServletRequest request, @RequestParam("id") Integer musicId) {
        // 检查用户是否登录
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            return  new ResponseBodyMessage<>(-1, "当前用户未登录，请登录后再进行该操作", false);
        }
        // 检查该记录在数据库中是否存在
        UserInfo userInfo = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
        int userId = userInfo.getId();
        System.out.println(userId);
        System.out.println("musicId: " + musicId);
        LoveMusicInfo loveMusicInfo = loveMusicService.loveMusicCheck(musicId, userId);
        if (loveMusicInfo == null) {
            System.out.println("该音乐在喜欢列表中不存在对应的记录");
            return new ResponseBodyMessage<>(200, "该音乐在喜欢列表中无记录", true);
        }
        else {
            int res = loveMusicService.deleteLoveMusic(musicId);
            if (res == 0) {
                return new ResponseBodyMessage<>(-2, "从喜欢数据中移除失败", false);
            }
            return new ResponseBodyMessage<>(200, "移除成功", res);
        }



    }

}
