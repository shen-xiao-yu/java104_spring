 create database if not exists `music_ssm` character set utf8;
 create table user(
        id int primary key auto_increment,
        username varchar(20) not null unique,
        password varchar(255) not null,
        age int not null,
        gender varchar(64) not null,
        email varchar(64) not null
        ) default charset 'utf8mb4';


      DROP TABLE IF EXISTS `music`;
      CREATE TABLE `music` (
      `id` int PRIMARY KEY AUTO_INCREMENT,
      `title` varchar(50) NOT NULL,
      `singer` varchar(30) NOT NULL,
      `time` varchar(13) NOT NULL,
      `url` varchar(1000) NOT NULL,
      `userid` int(11) NOT NULL
      )default charset 'utf8mb4';

      DROP TABLE IF EXISTS `lovemusic`;
      CREATE TABLE `lovemusic` (
      `id` int PRIMARY KEY AUTO_INCREMENT,
      `user_id` int(11) NOT NULL,
      `music_id` int(11) NOT NULL
      )default charset 'utf8mb4';