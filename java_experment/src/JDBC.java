import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.Scanner;

public class JDBC {
    public static void main(String[] args) throws SQLException{
        /**
         * JDBC 本质上就是 Java 对于各种数据库差异性的 封装
         * throws SQLException是方法签名中的异常声明，表明该方法有可能产生SQLException异常，并将该异常抛给上层调用者来处理
         *  你要通过JDBC操作数据库，你首先需要链接到这个数据库——包括我们需要用到mysql的驱动包也是为了能连上数据库
         */
        // 1. 创建数据源
        DataSource dataSource = new MysqlDataSource(); //MysqlDataSource是MySQL数据库驱动程序提供的一个具体的数据源实现类
        // 其中DataSource是一个接口，不能直接实例化，而MysqlDataSource则是实现了这个DataSource接口
        // 这个类是来自我们之前导入的驱动包的，如果你没有正确导入这个驱动包，你是显示不出来的
        // 你想要查询操作数据库中的数据，还要指定是哪个数据库---包括用户名密码的校验
        // 其中movies_ratings是我们要连接的那个数据库的具体名称
        //(MysqlDataSource)dataSource：类型转换，将dataSource对象强制转换成MysqlDataSource类型
        //setUrl()：用来设置数据源的URL属性，即MySQL数据库的连接地址
        //characterEncoding指定了字符编码为UTF-8，useSSL参数设为false则表示不使用SSL连接(不启用加密)
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/movies_ratings?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root"); // 用户名
        ((MysqlDataSource)dataSource).setPassword("030402"); // 数据库密码

        // 2.和数据服务器建立连接
        //Connection是Java JDBC（Java Database Connectivity）API中的一个接口，表示与数据库的连接
        //通过数据源对象dataSource调用getConnection()方法来获取一个数据库连接。该方法会返回Connection类型
        Connection connection= dataSource.getConnection(); //受查异常

        // 4. 构造 SQL
        //new Scanner(System.in)：使用new关键字创建了一个Scanner对象，并将其初始化为从标准输入流（System.in）中读取输入的数据
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入你要查询的关键词: ");
        String keyword = scanner.nextLine();
        //SUBSTRING_INDEX()函数用于根据指定的分隔符获取子字符串。
        //) AS rating_table：子查询结束，并将其命名为rating_table，作为临时的评分数据表。
        //ON movies.电影id = rating_table.电影id：通过连接条件将主查询的数据表movies和子查询生成的临时数据表rating_table进行连接
        //WHERE movies.电影名 LIKE ?：指定查询的条件，使用占位符?表示待填入的参数，这里是电影名的模糊匹配条件
        String sql = "SELECT movies.电影名, COUNT(rating_table.评分), AVG(rating_table.评分) " +
                "FROM movies " +
                "JOIN (" +
                "  SELECT SUBSTRING_INDEX(ratings.聚合字段, '::', 1) AS 用户id, " +
                "         SUBSTRING_INDEX(SUBSTRING_INDEX(ratings.聚合字段, '::', 2), '::', -1) AS 电影id, " +
                "         SUBSTRING_INDEX(SUBSTRING_INDEX(ratings.聚合字段, '::', 3), '::', -1) AS 评分, " +
                "         SUBSTRING_INDEX(SUBSTRING_INDEX(ratings.聚合字段, '::', 4), '::', -1) AS 评分时间 " +
                "  FROM ratings " +
                ") AS rating_table ON movies.电影id = rating_table.电影id " +
                "WHERE movies.电影名 LIKE ? " +
                "GROUP BY movies.电影id " +
                "ORDER BY AVG(rating_table.评分) DESC";
        //上述代码中使用了占位符 ? 来代表需要绑定的参数，然后通过 setString() 方法将值绑定到参数中，保证了关键字参数的安全性。
        // 在执行查询时，会使用预编译的语句，数据库会对参数进行适当的转义处理，从而防止SQL注入攻击。
        //sql：这是一个占位符，应该替换为具体的SQL查询或更新语句
        //PreparedStatement 是Java JDBC API 中用于执行带有参数的预编译 SQL 语句的接口
        //通过 connection 对象调用 prepareStatement(sql) 方法，创建一个 PreparedStatement 对象。sql 是要执行的SQL语句
        PreparedStatement statement = connection.prepareStatement(sql);
        //setString()：PreparedStatement 接口中的一个方法，用于设置字符串类型参数的值
        //1：表示要设置的参数的位置,表示要填充 SQL 语句中第一个占位符
        //% + 关键词 + % 表示将会匹配包含关键词的所有字符串
        statement.setString(1, "%" + keyword + "%");

        // 5. 执行 SQL.
        //    此处要使用的是 executeQuery查询语句，可以查带占位符的SQL预编译语句
        //    executeUpdate 只能返回一个 int.
        //    executeQuery 返回的是一个 ResultSet 对象. 可以把这个对象视为是一个 "临时表"，结果集
        ResultSet resultSet = statement.executeQuery();

        // 6. 遍历临时表, 拿到里面的数据.
        //    resultSet 简单的当成一个类似于 "迭代器" 这样的东西来看待~
        //    next 如果没有到达末尾, 就是返回 true, 要继续循环.
        //    next 如果到达末尾, 就返回 false, 结束循环.
        // 输出查询结果
        while (resultSet.next()) {
            String movieName = resultSet.getString(1);
            int ratingCount = resultSet.getInt(2);
            double ratingAverage = resultSet.getDouble(3);
            System.out.println("电影名称: " + movieName);
            System.out.println("评分个数: " + ratingCount);
            System.out.println("评分平均值: " + ratingAverage);
            System.out.println("---------------------------------");
        }
        // 7. 关闭连接, 释放资源
        resultSet.close();
        statement.close();
        connection.close();
    }
}
