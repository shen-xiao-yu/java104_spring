package com.example.demo.model;

import lombok.Data;

import java.util.Date;

@Data  // 这是一个普通的实体类
public class UserInfo {
    private Integer id;
    private String username;
    private String password;
    private String photo;
    private Date createTime;
    private Date updateTime;
}
