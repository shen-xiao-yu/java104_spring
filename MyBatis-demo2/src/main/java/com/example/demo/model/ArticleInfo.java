package com.example.demo.model;

import lombok.Data;

import java.util.Date;
// @Data注解包含了getter和setter\toString
@Data // 普通的实体类
public class ArticleInfo {
    private int id;
    private String title;
    private String content;
    private Date createtime;
    private Date updatetime;
    private int uid;
    private int rcount;
    private int state;

}
