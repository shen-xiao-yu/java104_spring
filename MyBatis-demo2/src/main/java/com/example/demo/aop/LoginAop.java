package com.example.demo.aop;

import com.example.demo.controller.UseController;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

// 切面表示我们要统一出来的问题——比如验证用户是否登录
// 切点则是是否进行Aop拦截的规则（哪些页面不需要进行登录验证，哪些需要，这种规则）
// 连接点则是具体到哪些页面需要进行拦截（登录验证）
// 通知则是验证用户是否登录的那个具体方法实现（代码细节）——》前置通知，后置通知
// 验证当前是否登录的aop实现类
//@Component

//@Aspect // 表示当前类是一个切面
//// 1、添加Spring Aop框架支持
//public class LoginAop {
//    // 2、定义切点
//
//    @Pointcut("execution(* com.example.demo.controller.UseController.*(..))")
//    public void pointcut() {
//    }
//    // 3、通知（前置通知）
//    @Before("pointcut()") // 注解中参数声明这个前置方法是针对那个切点的
//    public void before(){
//        System.out.println("前置通知");
//    }
//
//}


