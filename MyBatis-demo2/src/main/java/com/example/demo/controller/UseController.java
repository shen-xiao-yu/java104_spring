package com.example.demo.controller;

import com.example.demo.model.ArticleInfo;
import com.example.demo.service.UseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
// 控制层——》调用服务层（service)
@RestController // @RestController等于@Controller+@ResponseBody
@RequestMapping("/web")
public class UseController {
    @Autowired
    private UseService useService;
    @RequestMapping("/add")
    public String add() {
        ArticleInfo articleInfo = new ArticleInfo();
        articleInfo.setTitle("11通过url请求操控数据库");
        articleInfo.setContent("controller-->service-->mapper—->xml");
        articleInfo.setUid(17);
        useService.add(articleInfo);
        return "添加成功";
    }
    @RequestMapping("/get_by_id")
    public ArticleInfo getById() {
        return useService.getById(5);
    }
    @RequestMapping("/delete_by_id")
    public String deleteById() {
        useService.delete(5);
        return "删除成功！";
    }
    @RequestMapping("/test")
    public String aopTest() {
        System.out.println("这是测试！");
        return "执行测试！";
    }
}
