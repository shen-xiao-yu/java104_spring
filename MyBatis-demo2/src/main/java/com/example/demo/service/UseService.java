package com.example.demo.service;

import com.example.demo.mapper.ArticleMapper;
import com.example.demo.model.ArticleInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// 这是服务层，服务层调用我吗的Mapper（和之前我们测试Mapper所用的测试类有点像）
@Service
public class UseService {
    @Autowired
    private ArticleMapper articleMapper;

    // 插入文章
    public void add(ArticleInfo articleInfo) {
        articleMapper.add(articleInfo);
    }
    // // 查询操作——查询id为1的数据
    public ArticleInfo getById(int id) {
        return articleMapper.select(id);
    }
    // 删除自增主键为5的数据
    public void delete(int id) {
        articleMapper.delete(id);
    }



}
