package com.example.demo.mapper;

import com.example.demo.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
// 这个Mapper的作用就是借助resources/mapper下的.xml文件，实现了该接口
// 加了Mapper注解就表明这个接口是已经被实例化了的，可以调用里面的方法
@Mapper // 添加了这个注解表明该接口是MyBatis接口，不能省略
public interface UserMapper {
    // 在这里写增删改查所对应的抽象方法
    public List<UserInfo> getAll(); // 查询操作
    // 一个增删改查sql语句就对应这里的一个抽象方法
    // 传参查询
    public UserInfo selectById(@Param("id") Integer id); // 查询特定id的值
    // 这里如果@Parm("")括号中的属性名和形参名不一致，在.xml中优先使用@Parm中的名称
}
