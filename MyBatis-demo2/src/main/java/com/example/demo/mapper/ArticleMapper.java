package com.example.demo.mapper;

import com.example.demo.model.ArticleInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
// 这个类对应数据库中的articleinfo表
@Mapper // 表明这个是一个MyBatis接口
// 这个注解作用很大，把这个接口和.xml文件对应起来
public interface ArticleMapper {
    // 查询操作——查询id为1的数据
    public ArticleInfo select(@Param("id") Integer id);

    // 插入文章，这里的返回值表示改数据库操作所影响的行数
    public int add(@Param("articleInfo") ArticleInfo articleInfo);

    // 插入文章并获取自增主键
    public int addGetId(@Param("articleInfo") ArticleInfo articleInfo);

    // 删除文章自增主键为2的文章
    public int delete(@Param("uid") int uid);

    // 更新文章id为1的标题
    public int update(@Param("id") int id, @Param("title") String title);

}
