package com.example.demo.mapper;

import com.example.demo.model.ArticleInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ArticleMapperTest {
    @Autowired
    private ArticleMapper articleMapper;
    @Test
    void select() {
        ArticleInfo articleInfo = articleMapper.select(1);
        System.out.println(articleInfo.toString());
    }

    @Test
    void add() {
        ArticleInfo articleInfo = new ArticleInfo();
        articleInfo.setTitle("海底两万里");
        articleInfo.setContent("dfs都发啥了是搭街坊卡拉时间的");
        articleInfo.setUid(77);


        int result = articleMapper.add(articleInfo);
        System.out.println("执行结果为：" + result);
    }

    @Test
    void addGetId() {
        ArticleInfo articleInfo = new ArticleInfo();
        articleInfo.setTitle("添加文章并返回自增主键");
        articleInfo.setContent("需要在xml中设置useGeneratedKeys=\"true\" keyProperty=\"id\"");
        articleInfo.setUid(100);
        // 注意这里的result不是返回的自增主键，还是该操作所影响的行数
        int result = articleMapper.addGetId(articleInfo);
        System.out.println("影响的行数：" + result + "返回的自增主键：" + articleInfo.getId());
    }

    @Test
    void delete() {
        int result = articleMapper.delete(77);
        System.out.println("删除所收影响的行数是：" + result);

    }

    @Test
    void update() {
        int result = articleMapper.update(1, "这是一个更新操作");
        System.out.println("该更新操作所影响的行数为： " + result);
    }


}