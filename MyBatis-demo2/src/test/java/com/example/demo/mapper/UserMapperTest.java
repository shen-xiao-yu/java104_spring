package com.example.demo.mapper;

import com.example.demo.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest // 当前测试的上下文环境为Spring Boot
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;
    @Test
    void getAll() {

        List<UserInfo> list = userMapper.getAll();
        for (UserInfo user :
                list) {
            System.out.println(user.toString());
        }

    }

    @Test
    void selectById() {
        UserInfo userInfo = userMapper.selectById(1);
        System.out.println(userInfo.toString());
    }
}