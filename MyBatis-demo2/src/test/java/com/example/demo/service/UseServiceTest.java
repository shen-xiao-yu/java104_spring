package com.example.demo.service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class UseServiceTest {
    @Autowired
    private UserMapper userMapper;
    @Test
    void getById() {
        UserInfo userInfo = userMapper.selectById(1);
        System.out.println(userInfo);
    }
}