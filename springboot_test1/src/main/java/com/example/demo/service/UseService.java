package com.example.demo.service;

import com.example.demo.controller.TestController;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class UseService {

    // 1、得到日志对象，注意我们这里的日志对象来自org.slf4j
    private static Logger logger = LoggerFactory.getLogger(TestController.class); // 这个括号里的是当前你打印的类的类名
    @RequestMapping("/log_service")
    public String sayLog() {
//        logger.info("---这是我自定义的一个日志---");
//        logger.debug("a test");
        // 2、使用日志对象
        logger.trace("================= trace ===============");
        logger.debug("================= debug ===============");
        logger.info("================= info ===============");
        logger.warn("================= warn ===============");
        logger.error("================= error ===============");
        return "log service";
    }
}
