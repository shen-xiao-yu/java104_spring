package com.example.demo.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;

import javax.annotation.security.PermitAll;
import java.util.List;

@Data // data这是一个复合注解，包含了getter和setter——这个是lombok库提供的
@Controller // 将该bean对象存储到spring容器中
@ConfigurationProperties(value = "mylist") // 将配置文件中的mylist集合赋值给该类
public class MyList {
    private List data;

}
