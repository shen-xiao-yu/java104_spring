package com.example.demo.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

@Controller // 将该类注册到spring当中
@ConfigurationProperties(prefix = "student") // 将配置文档中的student属性值赋值给这里的类
public class Student {
    private int id;  // 这里的属性值一定要和我们yml配置文件中的属性值一定要相同
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
