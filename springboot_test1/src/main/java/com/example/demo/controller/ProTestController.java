package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j        // 来自lombok框架，更简单的得到日志对象
public class ProTestController {
    // 如果你使用了lombok,这时你就不需要得到日志对象，lombok帮我们做了——log对象，之间使用
    @RequestMapping("/lombok")
    public static String test() {
        log.info("这是你自定义的一个日志"); // 这里因为我们用了开发环境下的配置文件，该文件中对demo这个包下的日志级别做了限制
        log.warn("----warn-----");
        return "还不错哟！";
    }

}
