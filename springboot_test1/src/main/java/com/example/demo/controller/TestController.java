package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// 在springboot的启动类的平级目录下才有用,
// 你记着凡是类(除了实体类），都不能忘了在spring当中加载注册，spring是一个控制反转的IoC容器，先要把bean对象存到spring当中
@RestController  //  // 这个是个复合注解，等于@Controller + @ResponseBody
public class TestController {
    // 读取系统的配置项，注意Value注解的写法
    @Value("${server.port}")
    private Integer port;


    // 读取自定义的配置项
    @Value("${mykey.key}")  // 把{}中的对应的配置内容赋值到下面的mykey
    private String mykey;
    @RequestMapping("/hi")
    private String Test() {
        return mykey + port;
    }


    @RequestMapping("/sayHi") // 路由参数最好用sayHi或say_hi（windows系统不区分大小写，你写sayhi会有问题）
    public String sayHi() {
        return "Hi, spring boot!";
    }


    // 1、得到日志对象，注意我们这里的日志对象来自org.slf4j
    private static Logger logger = LoggerFactory.getLogger(TestController.class); // 这个括号里的是当前你打印的类的类名
    @RequestMapping("/log_controller")
    public String sayLog() {
//        logger.info("---这是我自定义的一个日志---");
//        logger.debug("a test");
        // 2、使用日志对象
        logger.trace("================= trace ===============");
        logger.debug("================= debug ===============");
        logger.info("================= info ===============");
        logger.warn("================= warn ===============");
        logger.error("================= error ===============");
        return "log controller";
    }
}
