package com.example.demo.controller;

import com.example.demo.model.MyList;
import com.example.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@RestController // 这个是个复合注解，等于@Controller + @ResponseBody——我是一个http控制器，返回的是一个非静态的页面
@Controller  // 我是一个控制器，我要在spring加载的时候启动并注册，如果你想实现路由注册，只有用@Controller类注解才能实现，用其他5大类注解不可以
@ResponseBody // 当前类返回的是一个非静态页面
public class UseController {
    @Value("${mykey}")
    private String test;

    @Value("${dir1.dir2.dd1}")
    private String dd1;

    @Value("${dir1.dir2.dd2}")
    private String dd2;

    @Value("${dir1.dir2.dd3}")
    private String dd3;

    @RequestMapping("/test1")
    public String test1() {
        System.out.println(dd1);
        System.out.println(dd2);

        System.out.println(dd3);
        return "你好！yml";
    }

    @Autowired
    private Student student; // 依赖注入，将bean对象从spring中取出来

    @Autowired
    private MyList myList;


    @RequestMapping("/test")
    public String test() {
        System.out.println(dd1);
        System.out.println(dd2);

        System.out.println(dd3);

        System.out.println(student.toString());
        System.out.println(myList.toString());
        System.out.println(myList.getData().size() + " " + myList.getData().get(1));
        return test;

    }
}
