package com.example.demo.model;

import lombok.Data;

@Data // 复合框架——包含了getter和setter\也包含了toString方法
public class Student {
    // 普通的实体类。不用注入，不用把该对象放到spring容器中
    // 咱们的key值是都大小写是敏感的，建议全部小写
    private Integer id; // 前端没传的就是null——所以我们要用包装类（不能用int)空指针异常
    private String name;
    private Integer age;
    // 和前端传递的参数名称一致，不一致的话就获取不到传来的参数


}
