package com.example.demo.controller;

import com.example.demo.model.Student;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@Data
//@RestController // 这个是个复合注解，等于@Controller + @ResponseBody
@Controller  // 我是一个控制器，我要在spring加载的时候启动并注册
@ResponseBody // 当前类返回的是一个非静态页面
@Slf4j
@RequestMapping("/web") //当你在url中使用/web时候，会访问该类————一个http请求
public class WebTest {
    // 支持get和post请求
    @RequestMapping("/mvc") // 当你使用url中使用/web/mvc访问该方法
    public static String test() {
        log.info("--这是你自定义的日志----");
        return "hello springMVC!";
    }
    @RequestMapping(value = "/servlet", method = RequestMethod.POST) // 指定只能接收post请求
    public String useServletApi(HttpServletRequest request, HttpServletResponse response) {
        String name = request.getParameter("name");
        return "传来的参数为：" + name;
    }

    // 当前我们的项目还没有配置热加载，所有我们每改一下代码，就要重新运行一下项目
    // 获取前端的参数尽量用包装类，如果用int，传了一个空就会报错
    @GetMapping("/get1") // 只是接收get请求
    public String getParam1(String name) { // 可以通过url中的queryString或Postman传参
        // 前端参数和后端参数匹配的时候才会获取到，前端穿的key是name,后端接收的也是name
        return "value：" + name;
    }

    // 当前端传递的参数有多个时候，可以用对象接收（注意这个对象是普通的实体类对象，不是json）
    @PostMapping("/post1") // 可以通过postman传参
    public String post1(Student student) {
        return student.toString();
    }

    // 传参——url传参、form表单、ajax传参、
//    我们表单提交时候，就是name作为key的？
    // from表单提交
    @RequestMapping("/index")
    public String index(String username, String password) {
        return "欢迎来到首页！当前登录的用户名是：" + username + "密码是：" + password;
    }
}
