package Thread;
/**
 * 我们需要在主线程中开启多个线程去执行一个任务，然后收集各个线程的返回结果并将最终结果进行汇总，这是就需要用到 Callable 接口。
 * 可以看到，这是一个泛型接口，call()函数返回的类型就是传递进来的V类型。
 *
 * 那么怎么使用Callable呢？一般情况下是配合ExecutorService来使用的，在ExecutorService接口中声明了若干个submit方法的重载版本：
 */

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class CThread implements Callable<String> {
    private String name;

    public CThread(String name ) {
        this.name = name;
    }

    //重写call()方法
    @Override
    public String call() throws Exception {
        return name;
    }
}
public class ExecutorService {
    public static void main(String[] args) {
        //创建线程池
        java.util.concurrent.ExecutorService pool = Executors.newFixedThreadPool(5);

//创建接收结果的列表集合
        List<Future> list = new ArrayList<Future>();

        for(int i = 0;i<5;i++) {
//创建线程对象
            Callable c = new CThread("线程"+i);

//将线程对象提交到线程池中，并将返回结果接收
            Future future = pool.submit(c);
            System.out.println("线程"+i+"已经加入线程池");

//将返回结果加入集合
            list.add(future);
        }

//关闭线程池
        pool.shutdown();

//打印返回结果
        for (Future future : list) {
            try {
                System.out.println(future.get().toString());
            } catch (InterruptedException | ExecutionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
