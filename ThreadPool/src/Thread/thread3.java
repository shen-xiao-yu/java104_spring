package Thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * 线程创建——》实现Runnable接口，重写run方法
 */
class MyRunable implements Runnable {
    @Override
    public void run() {
        System.out.println("实现Runable接口，重写run方法");
    }
}
public class thread3 {
    public static void main(String[] args) {
        ExecutorService es = Executors.newCachedThreadPool();
        MyRunable myRunable = new MyRunable();
        // es.submit(myRunable); // 将我们的Runnable任务提交到我们线程池中
        es.execute(myRunable);
        es.shutdown();
//        FutureTask：是对Runnable和Callable的进一步封装，
//        相比直接把Runnable和Callable扔给线程池，FutureTask的功能更多
    }
    public static void main1(String[] args) {
        MyRunable myRunable = new MyRunable();
        Thread thread = new Thread(myRunable);
        // Thread thread1 = new Thread(new MyRunable());
        thread.start();
    }
}
