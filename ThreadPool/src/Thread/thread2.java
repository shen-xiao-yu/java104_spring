package Thread;
class MyThread2 extends Thread {
    public void run() {
        System.out.println("thread1----这是你通过继承Thread类，重写run方法，创建的一个线程");
    }
}
public class thread2 {

    public static void main(String[] args) {
        // 继承Thread类，重写run方法（借助匿名内部类）
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                System.out.println("thread2----这是你新创建的一个线程");
            }
        };
        thread1.start();

        //  // 继承Thread类，重写run方法
        Thread thread2 = new MyThread2();
        thread2.start();

        // 使用lambda表达式
        Thread thread3 = new Thread(() -> {
            System.out.println("thread3 ---- 这是你通过lambda表达式创建的线程！");
        });
        thread3.start();

        System.out.println("这是主线程！");
    }

}
