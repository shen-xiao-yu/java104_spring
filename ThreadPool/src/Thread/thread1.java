package Thread;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
/**
 * 和Runnable接口不一样，Callable接口提供了一个call（）方法作为线程执行体，call()方法比run()方法功能要强大。
 *  同时创建对象的时候，
 * 》call()方法可以有返回值
 *
 * 》call()方法可以声明抛出异常
 * Java5提供了Future接口来代表Callable接口里call()方法的返回值，并且为Future接口提供了一个实现类FutureTask，
 * 这个实现类既实现了Future接口，还实现了Runnable接口，因此可以作为Thread类的target。在Future接口里定义了几个公共方法来控制它关联的Callable任务。
 */
class MyCallable implements Callable<String> {
    @Override
    public String call() throws Exception {
        System.out.println("这是用Callable创建线程的一个尝试！");
        return "xixi";
    }
}
public class thread1 {
    public static void main1(String[] args) throws ExecutionException, InterruptedException {
        // 1】创建Callable接口的实现类，并实现call()方法，然后创建该实现类的实例（从java8开始可以直接使用Lambda表达式创建Callable对象）。
        MyCallable myThread = new MyCallable(); // myThread是一个Callable对象

        //2】使用FutureTask类来包装Callable对象，该FutureTask对象封装了Callable对象的call()方法的返回值
        FutureTask<String> futureTask = new FutureTask<String>(myThread); // 与Callable关联

        //3】使用FutureTask对象作为Thread对象的target创建并启动线程（因为FutureTask实现了Runnable接口）——实质上还是以Callable对象来创建并启动线程
        // FutureTask实现Future接口，说明可以从FutureTask中通过get取到任务的返回结果，也可以取消任务执行（通过interreput中断）
        Thread thread = new Thread(futureTask, "有返回值的线程");
         thread.start();
        // 4】调用FutureTask对象的get()方法来获得子线程执行结束后的返回值
        System.out.println("子线程的返回值" + futureTask.get()); //get()方法会阻塞，直到子线程执行结束才返回
    }

    public static void main2(String[] args) {
        ExecutorService es = Executors.newCachedThreadPool();
        MyCallable myCallable = new MyCallable();
        es.submit(myCallable); // 你直接把Callable任务丢给线程池，获取不到call返回值
        es.shutdown();
        // FutureTask：是对Runnable和Callable的进一步封装，
        //相比直接把Runnable和Callable扔给线程池，FutureTask的功能更多
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService es = Executors.newCachedThreadPool();
        MyCallable myCallable = new MyCallable();
        //2】使用FutureTask类来包装Callable对象，该FutureTask对象封装了Callable对象的call()方法的返回值
        FutureTask<String> futureTask = new FutureTask<String>(myCallable); // 与Callable关联
        es.submit(futureTask); // 你直接把Callable任务丢给线程池，获取不到call返回值
        System.out.println(futureTask.get()); // 通过futureTask打印返回值
        es.shutdown();
    }


}
