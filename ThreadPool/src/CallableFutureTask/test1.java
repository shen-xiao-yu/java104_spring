package CallableFutureTask;

import java.util.concurrent.*;

class MyCallable implements Callable<String> {
    public String call() {
        System.out.println("111111111111111111111");
        return "xixi";
    }
}

public class test1 {
    public static void main(String[] args) {
        FutureTask<String> futureTask = new FutureTask<>(new MyCallable());
        // 创建线程池
        ExecutorService es = Executors.newCachedThreadPool();
        es.submit(futureTask);
        // es.submit(new MyCallable());
    }
    public static void main1(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<String> futureTask = new FutureTask<>(new MyCallable());
        /*Thread thread = new Thread(futureTask);
        thread.start();
        System.out.println(futureTask.get()); */
        // FutureTask实现了Future接口（可获取任务执行结果\取消任务等），同时也实现了Runnable接口所以可以将 FutureTask 对象作为任务提交给 ThreadPoolExecutor 去执行，也可以直接被 Thread 执行
        new Thread(futureTask).start();
    }
}
