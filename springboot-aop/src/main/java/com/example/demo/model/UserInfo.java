package com.example.demo.model;

import lombok.Data;
// 一个实体类对应了数据库中的一张表，注意字段名要相符（和数据库中）
@Data
public class UserInfo {
    private int id;
    private String username;
    private String password;
    private String photo;
    private String createtime;
    private String updatetim;
    private int state;
}
