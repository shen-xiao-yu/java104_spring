package com.example.demo.controller;

import com.example.demo.service.UserService;
import com.sun.deploy.net.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/add")
    @Transactional // 事务
    public int add(String username, String password) {
        if(!StringUtils.hasLength(username) && !StringUtils.hasLength(password)) {
            return -1;
        }
        try {
            int n = 10 / 0;
        }
        catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return userService.add(username, password);
    }
    @RequestMapping("/login")
    // 通过HttpServletRequest request.getParameter可以获取前端的参数(在servlet能用，在springboot也能用）
//    public void login(HttpServletRequest request, HttpServletRequest response) {
//        String username = request.getParameter("username");
//    }
    // 直接传参,注意形参中的参数名必须和前端传递的参数名相同
    // 1、非空判断
    public Object login(HttpServletRequest request, String username, String password) {
        if (StringUtils.hasLength(username) && StringUtils.hasLength(password)) {
            // 2、验证用户名是否正确
            if (username.equals("123") && password.equals("123")) {
                HttpSession session = request.getSession(); // 获取session,如果当前没有就创建一个
                session.setAttribute("userinfo", "admin");
                return "登录成功";
            }
            else {
                return "用户名或密码输入错误！";
            }
        }
        return "前端参数传递有误！";
    }
    @RequestMapping("/test")
    public Object test() {
        return "this a test";
    }
    @RequestMapping("/reg")
    public Object reg() {
        String str = null;
        // str.hashCode();
        // int n = 1/0;
        return "用户注册！";
    }
}
