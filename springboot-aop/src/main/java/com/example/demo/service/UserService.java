package com.example.demo.service;

import com.example.demo.controller.UserController;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    public int add(String username, String password) {
        return userMapper.add(username, password);
    }
}
