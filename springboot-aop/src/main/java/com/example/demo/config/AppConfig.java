package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截规则配置（针对用户登录这个功能——》切点）
 */
@Configuration // 该配置文件在项目启动时配置,你不添加这个，你下面配置的规则都没有用
public class AppConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor; // 用户登录拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // // 自定义拦截器添加到系统的配置文件中
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**") // 拦截所有的请求
                .excludePathPatterns("/user/login") // 排除不拦截的url
                .excludePathPatterns("/user/reg")
                .excludePathPatterns("/user/add");
    }
}
