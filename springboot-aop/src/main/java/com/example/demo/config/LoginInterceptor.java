package com.example.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 用户登录拦截器（切点）——》整个方法代码细节，顺序（通知）
 * 但这其实用的不是springAop实现用户登录权限的验证（因为他存在以下两个问题）
 * 1.没办法获取到HttpSession对象。
 * 2.我们要对一部分方法进行拦截，而另一部分方法不拦截，如注册方法和登录方法是不拦截的，这样的话排除方法的规则很难定义，甚至没办法定义
 */
//对于以上问题Spring中提供了具体的实现拦截器：HandlerInterceptor,拦截器的实现分为以下两个步骤：
//        1、创建自定义拦截器，实现 HandlerInterceptor 接口的perHandle（执行具体方法之前的预处理）方法。
//        2、将自定义拦截器加入 WebMvcConfiger的 addInterceptors方法中。
    @Component // 讲该类存入spring中
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 判断用户是否登录
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("userinfo") != null) {
            return true;
        }
        System.out.println("当前用户没有权限！");
        response.setStatus(401); // 用户未登录（客户端的问题）
        return false;
    }
}
