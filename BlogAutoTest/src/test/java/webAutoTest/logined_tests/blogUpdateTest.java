package webAutoTest.logined_tests;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import webAutoTest.common.AutotestUtils;

/**
 * 用户登录状态下对博客修改页面进行测试
 */
public class blogUpdateTest extends AutotestUtils {
    @Test
    @BeforeAll
    static void init() {
        driver.get("http://49.235.66.46:9000/blog_content.html?id=17");
    }
    @Test
    void pageTest() {
        // 查看博客标题是否存在
        driver.findElement(By.cssSelector("body > div.container > div.container-right > div > h3"));
        // 查看修改博客和删除博客按钮是否存在
        driver.findElement(By.cssSelector("body > div.nav > a.del"));
        driver.findElement(By.cssSelector("body > div.nav > a.update"));
    }
    @Test
    @AfterAll
    static void exit() {
        driver.quit();
    }
}
