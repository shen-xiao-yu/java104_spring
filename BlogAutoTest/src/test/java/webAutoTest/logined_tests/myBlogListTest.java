package webAutoTest.logined_tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import webAutoTest.common.AutotestUtils;

/**
 * 用户已经登录的情况下，对个人主页进行测试
 */
public class myBlogListTest extends AutotestUtils {
    // 这里我们不重新生成驱动，我们用的还是登录界面的驱动，因为我们要保留登录状态
    @Test
    @BeforeAll
    static void init() {
        driver.get("http://49.235.66.46:9000/myblog_list.html?uid=2");
    }
    @Test
    void pageTest() {
        // 测试是否存在博客列表页的超链接（以及该超链接所显示的文本是否正确，只有在个人主页该超链接的文本才是“首页”这两个字）
        String expect = "首页";
        String actual = driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")).getText();
        Assertions.assertEquals(expect, actual);
        // 是否能找到个人信息栏
        driver.findElement(By.cssSelector("body > div.container > div.container-left > div > h3"));
    }

}
