package webAutoTest.logined_tests;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import webAutoTest.common.AutotestUtils;
import org.openqa.selenium.OutputType;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 说明当前该类下面的测试方法要按一定的顺序执行
public class loginTest extends AutotestUtils {

    public static ChromeDriver driver = createDriver();
    @Test
    @BeforeAll // 被@BeforeAll修饰的方法要是静态的
    static void init() {
        // 跳转到博客登录页面
        driver.get("http://49.235.66.46:9000/login.html");
    }

    /**
     * 检查登录页面是否正常显示
     * @throws InterruptedException
     */
    @Test
    @Order(1)
    void loginPageTest() throws InterruptedException {
        // 隐式等待--// 隐式等待，更加丝滑——》作用于下面的整个作用领域，这个方法中的所有元素，在这3秒内不断轮询
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        // 利用断言判断登录的文本内容显示是否正确
        String expect = "登录";
        String actual = driver.findElement(By.cssSelector("body > div.login-container > div > h3")).getText(); // 检查登录页面的登录文本是否存在
        Assertions.assertEquals(expect, actual);

        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)")); // 检查博客注册页的主页超链接是否存在
        // 检查提交按钮是否存在
        driver.findElement(By.cssSelector("#submit"));

    }
    /**
     * 检查登录失败的情况
     */
    @Order(2)
    @ParameterizedTest // 多个参数
    @CsvSource({"admin, 123"})
    void loginFailTest(String username, String password) throws IOException, InterruptedException {
        // 把之前默认填充内容清空
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(100);
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.accept();
    }

    /**
     * 检查正常登录的情况，每写一个测试用例就测试一下
     */
    @ParameterizedTest // 写了该注解就不用在写@Test注解了(多参数）
    @Order(3)
    @CsvSource({"admin, admin", "小鱼儿, 123"})
    void loginRightTest(String username, String password) throws InterruptedException, IOException {
        // 多个账号登录，在重新输入账号时，需要把之前的输入的内容清空
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(100);
        Alert alert = driver.switchTo().alert();
        alert.accept(); // 选择确认

        // 上述步骤只是说明输入了账号和密码，但还不知道点击提交后是否会跳转到博客列表页
        String expect = "http://49.235.66.46:9000/blog_list.html";
        String actual = driver.getCurrentUrl();
        Assertions.assertEquals(expect, actual); // 查看当前的url是否在博客详情页面
        // 进行截图，看当前是否跳转到了登录界面
        // 程序执行的速度和页面渲染的速度
        File srcFile =  driver.getScreenshotAs(OutputType.FILE);
        String fileName = "loginRightTest.png";
        FileUtils.copyFile(srcFile, new File(fileName));
        //因为我们要测试多个账号，所有在一个账号检测完了后，还需要回退到登录界面
        driver.navigate().back();
        System.out.println(driver.getCurrentUrl());
    }
    // 这里我们不关闭driver驱动，因为我们要保证登录状态

}
