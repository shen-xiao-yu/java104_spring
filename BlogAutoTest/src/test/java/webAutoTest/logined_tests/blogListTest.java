package webAutoTest.logined_tests;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import webAutoTest.common.AutotestUtils;

/**
 * 用户登录状态下的博客列表测试
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 说明当前类中的测试方法要按一定的顺序来执行---和order配合使用
public class blogListTest extends AutotestUtils {
    // 这里我们不重新生成驱动，我们用的还是登录界面的驱动，因为我们要保留登录状态
    @Test
    @BeforeAll
    static void init() {
        driver.get("http://49.235.66.46:9000/blog_list.html");
    }

    /**
     * 测试总的博客列表页面的完整性
     */
    @Test
    @Order(1)
    void pageTest() { // 不能用private修饰该方法——》我们的selenium还要调用该方法
        // 看看是否能够获取到博客列表页面的翻页按钮（只有总的博客列表页有，个人主页也没有）
        driver.findElement(By.cssSelector("body > div.container > div > div.blog-pagnation-wrapper > button:nth-child(1)"));
        // 查看页面的文本内容显示是否正确
        String expect = "Linux删除文件操作";
        String actual = driver.findElement(By.cssSelector("body > div.container > div > div:nth-child(2) > div.title")).getText();
        Assertions.assertEquals(expect, actual); // 断言
        // 查看是否有个人主页的超链接
        driver.findElement(By.cssSelector("#myblog")).click();
        String  expectURL = "http://49.235.66.46:9000/myblog_list.html";
        String actualURL = driver.getCurrentUrl().substring(0, 41);
        // 利用断言看：在登录成功的情况下，界面是否跳转到了个人主页
        Assertions.assertEquals(expectURL, actualURL);
    }


}
