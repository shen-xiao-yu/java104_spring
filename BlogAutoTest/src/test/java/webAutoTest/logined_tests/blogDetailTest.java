package webAutoTest.logined_tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import webAutoTest.common.AutotestUtils;

/**
 * 用户已经登录的状态下，对博客详情页进行测试
 */
public class blogDetailTest extends AutotestUtils {
    // 这里我们不重新生成驱动，我们用的还是登录界面的驱动，因为我们要保留登录状态
    @Test
    @BeforeAll
    static void init() {
        driver.get("http://49.235.66.46:9000/blog_content.html?id=2");
    }
    @Test
    void pageTest() {
        // 测试左侧信息框中的用户名是否能正常显示
        driver.findElement(By.cssSelector("body > div.container > div.container-left > div > h3"));
        // 测试删除博客按钮是否存在(文本显示是否正确）
        String expect1 = "删除博客";
        String actual1 = driver.findElement(By.cssSelector("body > div.nav > a.del")).getText();
        Assertions.assertEquals(expect1, actual1);
        // 测试修改博客按钮是否存在(文本显示是否正确）
        String expect2 = "修改博客";
        String actual2 = driver.findElement(By.cssSelector("body > div.nav > a.update")).getText();
        Assertions.assertEquals(expect2, actual2);
    }
}
