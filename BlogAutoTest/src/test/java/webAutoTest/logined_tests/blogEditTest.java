package webAutoTest.logined_tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import webAutoTest.common.AutotestUtils;

/**
 * 用户已经登录的情况下，对博客编辑页进行测试
 */
public class blogEditTest extends AutotestUtils {
    // 这里我们不重新生成驱动，我们用的还是登录界面的驱动，因为我们要保留登录状态
    @Test
    @BeforeAll
    static void init() {
        driver.get("http://49.235.66.46:9000/blog_edit.html");
    }
    @Test
    void pageTest() throws InterruptedException {
        // 测试是否能够找到博客编辑页的提示按钮
        driver.findElement(By.cssSelector("#submit"));
        // 测试是否能够找到对应页面的超链接
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(4)"));

        // 在博客标题中输入内容
        driver.findElement(By.cssSelector("#title")).sendKeys("测试开发实战");
        // 在博客内容中输入内容，因为我们这里使用了第三方的插件，所以我们不能直接通过sendkeys来输入内容（但我们可以通过click点击事件，往模块内容中插入横线等（本身插件提供的元素）
        driver.findElement(By.cssSelector("#editorDiv > div.editormd-toolbar > div > ul > li:nth-child(21) > a > i")).click();
        Thread.sleep(100);
        driver.findElement(By.cssSelector("#submit")).click(); // 点击提交

        // 如果发布成功，会出现一个弹窗——》提示发布成功
        Thread.sleep(100); // 等待弹窗出现
        Alert alert = driver.switchTo().alert();
        alert.accept();
        // 页面会跳到我们的总的博客列表页面，在博客列表页的最后一个元素看是否能够找到我们刚刚提交的数据
        // 1、到博客列表的末页
        driver.findElement(By.cssSelector("body > div.container > div > div.blog-pagnation-wrapper > button:nth-child(4)")).click();
        Thread.sleep(100);

        String expect = "测试开发实战";

        String actual = driver.findElement(By.xpath("/html/body/div[2]/div/div[last()]/div[1]")).getText(); // 获取该页中最后一个title标签，通过last()
        Assertions.assertEquals(expect, actual);

    }
}
