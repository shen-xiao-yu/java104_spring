package webAutoTest.unlogined_tests;

import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * 用户未登录的状态下，对博客详情页进行测试
 */
public class blogDetailTest {
    static ChromeDriver driver = new ChromeDriver();
    @Test
    @BeforeAll
    static void init() {
        driver.get("http://49.235.66.46:9000/blog_content.html?id=2");
    }
    @Test
    void pageTest() throws InterruptedException {
        // 未登录状态下，可以访问博客详情页，但不可以进行删除博客、修改博客的操作
        driver.findElement(By.cssSelector("body > div.nav > a.update")).click();
        // 点击删除博客，应该会出现-----是否要删除该博客的弹窗，点击确定
        Thread.sleep(300);
        Alert alert1 = driver.switchTo().alert(); // 点击完——确定要删除该博客——》在未登录状态，会出现下一个弹窗
        String expect = "当前用户未登录，你即将跳转到登录页面";
        String actual = alert1.getText();
        alert1.accept();
        Assertions.assertEquals(expect,actual);
    }
    @Test
    @AfterAll
    static void quit() {
        driver.quit();
    }
}
