package webAutoTest.unlogined_tests;

import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import webAutoTest.common.AutotestUtils;

/**
 * 用户未登录状态下的博客列表测试
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 说明当前类中的测试方法要按一定的顺序来执行---和order配合使用
public class blogListTest extends AutotestUtils {

    private static ChromeDriver driver = new ChromeDriver();
    @Test
    @BeforeAll
    static void init() {
        driver.get("http://49.235.66.46:9000/blog_list.html");
    }

    /**
     * 测试总的博客列表页面的完整性
     */
    @Test
    @Order(1)
    void pageTest() throws InterruptedException { // 不能用private修饰该方法——》我们的selenium还要调用该方法
        // 看看是否能够获取到博客列表页面的翻页按钮（只有总的博客列表页有，个人主页也没有）
        driver.findElement(By.cssSelector("body > div.container > div > div.blog-pagnation-wrapper > button:nth-child(1)"));
        // 查看页面的文本内容显示是否正确
        String expect = "Linux删除文件操作";
        String actual = driver.findElement(By.cssSelector("body > div.container > div > div:nth-child(2) > div.title")).getText();
        Assertions.assertEquals(expect, actual); // 断言
        // 查看是否有个人主页的超链接
        driver.findElement(By.cssSelector("#myblog")).click();
        Thread.sleep(100); // 强制等待弹窗的出现（隐式等待无法处理弹窗/显示等待和隐式等待尽量不共存）
        // 在未登录的情况下，页面跳转到个人主页是否会出现弹窗（以及弹窗内容是否和我们预期的一致）
        Alert alert = driver.switchTo().alert();
        String expectAlert = "当前用户未登录，你即将跳转到登录页面";
        String actualAlert = alert.getText();
        Assertions.assertEquals(expectAlert, actualAlert);
        // 不要忘了关闭弹窗
        alert.accept();
    }
    @Test
    @AfterAll
    static void exit() {
        driver.quit();
    }

}
