package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//
//@RequestMapping("/web")
@RestController
public class UserController {
    @RequestMapping("/say_hi") // 连接点1
    public String sayHi() {
        System.out.println("sayHi方法的执行");
        return "say Hi!";
    }
    @RequestMapping("/test") // 连接点2
    public String test() {
        System.out.println("test方法的执行");
        return "this is a test";
    }
}
