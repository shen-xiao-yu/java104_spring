package com.example.demo.mapper;

import com.example.demo.model.NewsInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface NewsMapper {
    // 发布新文章
    public int add(@Param("newsinfo")NewsInfo newsInfo);

    // 查询所有党政新闻（无分页功能）
    public List<NewsInfo> getNewsList();

        // 查询总的博客列表（分页功能的实现）_>注意limit和offset的位置（相对位置（参数的相对位置，不要弄反——》查询数据库会出错
    public List<NewsInfo> getListByPage(@Param("limit") int psize, @Param("offset") int offset);
    // psize对应limit--》表示每页显示的记录条数。pindex对应offset——》表示当前查看的是第几页的数据

    // 删除指定id的新闻
    public int delete(@Param("id") int id);
    // 获取总的新闻个数
    public int getPageCount();

}
