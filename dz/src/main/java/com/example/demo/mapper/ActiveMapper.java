package com.example.demo.mapper;

import com.example.demo.model.ActiveInfo;
import com.example.demo.model.DangyuanInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActiveMapper {

    public int add(@Param("activeinfo")ActiveInfo activeInfo);

    // 查询当前用户列表信息(带分页和查询）
    public List<ActiveInfo> getListByPage(
            @Param("name") String name,
            @Param("limit") Integer limit,
            @Param("offset") Integer offset
    );

    // 得出用户名为username,地址为address,邮箱为email的所有用户个数
    public int getPageCount(
            @Param("name") String name
    );
    // 删除指定id的新闻(党员编号）
    public int delete(@Param("id") int id);
}