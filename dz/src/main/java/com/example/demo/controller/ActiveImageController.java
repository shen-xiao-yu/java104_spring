package com.example.demo.controller;

import com.example.demo.config.AjaxResult;
import com.example.demo.config.ResponseAdvice;

import com.example.demo.util.ConstVariable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.BindException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/images")
public class ActiveImageController {

    @Value("${music.local.path}")
    private String UPLOAD_PATH; /*= "D:/images/upload/";*/

    Set<Map.Entry<Integer, Integer>> set = new HashSet<>();
    /**
     *  修改更新图片(图片一）
     */
    @RequestMapping("/upload_pic1")
    public Object upload1(HttpServletRequest request,
                         @RequestPart("filename")MultipartFile file,
                         HttpServletResponse response) throws IOException {

        // 1.先检查是否成功登录
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null ) {
            return AjaxResult.fail(-1, "用户当前未登录，请稍后再试");
        }





        // 3.确认是新音乐后， 将音乐上传到服务器
        String fileNameAndType = "/pic1.png";
//        fileNameAndType = file.getOriginalFilename();
        // 获取类路径
        ClassLoader classLoader = getClass().getClassLoader();
        String path2 = classLoader.getResource("static/image").getPath() + fileNameAndType;
        System.out.println(path2);
        File dest2 = new File(path2);





        if (!dest2.exists()) {
            dest2.mkdir();
        }
        // 判断文件大小是否超出限制
        if (file.getSize() > 20 * 1024 * 1024) {
            return AjaxResult.fail(-3, "上传大小不能超过20MB");
        }
        // 文件上传
        try {
            file.transferTo(dest2);
        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.fail(-4, "文件上传失败！");
        }
        return AjaxResult.success(200, "文件上传成功");
    }

    /**
     *  修改更新图片(图片二）
     */
    @RequestMapping("/upload_pic2")
    public Object upload2(HttpServletRequest request,
                          @RequestPart("filename")MultipartFile file,
                         HttpServletResponse response) throws IOException {

        // 1.先检查是否成功登录
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null ) {
            return AjaxResult.fail(-1, "用户当前未登录，请稍后再试");
        }

        // 3.确认是新音乐后， 将音乐上传到服务器
        String fileNameAndType = "/pic2.png";
//        fileNameAndType = file.getOriginalFilename();
        // 获取类路径
        ClassLoader classLoader = getClass().getClassLoader();
        String path2 = classLoader.getResource("static/image").getPath() + fileNameAndType;
        System.out.println(path2);
        File dest2 = new File(path2);

        if (!dest2.exists()) {
            dest2.mkdir();
        }
        // 判断文件大小是否超出限制
        if (file.getSize() > 20 * 1024 * 1024) {
            return AjaxResult.fail(-3, "上传大小不能超过20MB");
        }
        // 文件上传
        try {
            file.transferTo(dest2);
        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.fail(-4, "文件上传失败！");
        }
        return AjaxResult.success(200, "文件上传成功");
    }

    /**
     *  修改更新图片(图片3）
     */
    @RequestMapping("/upload_pic3")
    public Object upload3(HttpServletRequest request,
                         @RequestPart("filename")MultipartFile file,
                         HttpServletResponse response) throws IOException {

        // 1.先检查是否成功登录
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null ) {
            return AjaxResult.fail(-1, "用户当前未登录，请稍后再试");
        }



        // 3.确认是新音乐后， 将音乐上传到服务器
        String fileNameAndType = "/pic3.png";
//        fileNameAndType = file.getOriginalFilename();
        // 获取类路径
        ClassLoader classLoader = getClass().getClassLoader();
        String path2 = classLoader.getResource("static/image").getPath() + fileNameAndType;
        System.out.println(path2);
        File dest2 = new File(path2);

        if (!dest2.exists()) {
            dest2.mkdir();
        }

        // 判断文件大小是否超出限制
        if (file.getSize() > 20 * 1024 * 1024) {
            return AjaxResult.fail(-3, "上传大小不能超过20MB");
        }
        // 文件上传
        try {
            file.transferTo(dest2);
        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.fail(-4, "文件上传失败！");
        }
        return AjaxResult.success(200, "文件上传成功");
    }

    /**
     *  上传音乐文件
     */
//    @RequestMapping("/upload")
//    public Object upload(HttpServletRequest request,
//                         String image_name, @RequestPart("filename")MultipartFile file,
//                         HttpServletResponse response) throws IOException {
//
//        // 1.先检查是否成功登录
//        HttpSession session = request.getSession(false);
//        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null ) {
//            return AjaxResult.fail(-1, "用户当前未登录，请稍后再试");
//        }
//
//        // 程序走的这里说明用户已经登录了(继续检测参数传递是否正确
//        if (!StringUtils.hasLength(image_name)) {
//            return AjaxResult.fail(-2, "参数传递错误");
//        }
//        String fileNameAndType = "test.png";
//        fileNameAndType = file.getOriginalFilename();
//        // 获取上传的歌名
//        int index =fileNameAndType.lastIndexOf(".");
//        if (index == -1) {
//            // 文件名不包含后缀名
//            return AjaxResult.fail(-1, "文件名格式不正确", false);
//        }
//
//        String title = fileNameAndType.substring(0, index);
//        System.out.println(title);
//        /// 通过歌曲名和歌手来查询数据库中是否有当前要上传的音乐（如果歌曲名和歌手一样，我们认为是同一首歌曲）
//        MusicInfo musicInfo  = musicService.imageCheck(title);
//        if (musicInfo != null) {
//            return AjaxResult.fail(-4, "要上传的音乐已经存在，请勿重复上传", false);
//        }
//        // 3.确认是新音乐后， 将音乐上传到服务器
////        String fileNameAndType = "test.mp3";
////        fileNameAndType = file.getOriginalFilename();
//        String path = UPLOAD_PATH + fileNameAndType;
//        System.out.println(path);
//        File dest = new File(path);
//
//        if (!dest.exists()) {
//            dest.mkdir();
//        }
//        // 判断文件大小是否超出限制
//        if (file.getSize() > 20 * 1024 * 1024) {
//            return AjaxResult.fail(-3, "上传大小不能超过20MB");
//        }
//        // 文件上传
//        try {
//            file.transferTo(dest);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return AjaxResult.fail(-4, "文件上传失败！");
//        }
//        // 将上传的图片等信息保存在数据库中
//
//        return AjaxResult.success(200, "文件上传成功");
//    }

//    /**
//     * 删除图片（)
//     * （同时喜欢列表中的对应记录也应该同步删除）
//     */
//    @RequestMapping("/deleteMusicById")
//    // 后端参数重命名（后端参数映射：@RequestParam）
//    // 思考一点，是否要通过@RequestParm("")注解来指定参数名称，如果不指定，前端传递的参数要和controller函数参数列表中的参数名称一致
//    public Object deleteMusicById(HttpServletRequest request) {
//
//        // 1. 检查是否登录
//        HttpSession session = request.getSession(false);
//        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
//            return AjaxResult.fail(-2, "用户未登录，请登录后再执行删除操作", false);
//        }
//
//        // tips:你只能删除你所添加的音乐（等等）——
//        // 3.1 从服务器上删除对应的音乐文件（对应的目录中删除该音乐文件）
//        String title = musicInfo.getTitle();
//        String path = UPLOAD_PATH + title + ".mp3"; // 这里的路径："D:\music_ssm_files\向云端.mp3"
//        System.out.println(path);
//        File dest = new File(path);
//        if (dest.delete()) {
//            System.out.println("删除音乐成功");
//        }
//        else {
//            return new ResponseBodyMessage<>(-8, "服务器中该音乐文件删除失败", false);
//        }
//        // 3.2 把该条音乐记录从数据库中删除(用户只能删除自己上传的音乐，在每个音乐中有编号）
//        UserInfo userInfo = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
//        int userId = userInfo.getId();
//        int res1 = musicService.deleteMusicById(musicId);
//        if (res1 == 0) {
//            return new ResponseBodyMessage<>(-5, "音乐表中该条音乐记录删除失败", false);
//        }
//        // 是不是不用同步删除喜欢表中的记录
//
//        // 对应删除喜欢列表的该歌曲的记录的时候，要确保该喜欢表中有该记录（有可能我们已经在喜欢列表中已经移除该记录了）
//        // 注意这里删除的可能是多条记录（因为同一首音乐可能被多个不同的用户所收藏）
//        LoveMusicInfo loveMusicInfo = loveMusicService.loveMusicCheck(musicId, userId);
//        // 只有当喜欢表中有该记录的时候才会删除
//        if (loveMusicInfo != null) {
//            int res2 = loveMusicService.deleteLoveMusic(musicId);
//            if (res2 == 0) {
//                return new ResponseBodyMessage<>(-5, "音乐喜欢表中该条音乐记录删除失败", false);
//            }
//        }
//        // 但其他
//        return new ResponseBodyMessage<>(200, "删除成功", res1);
//
//
//    }


}

