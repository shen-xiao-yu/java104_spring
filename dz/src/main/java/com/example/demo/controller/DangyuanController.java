package com.example.demo.controller;

import com.example.demo.config.AjaxResult;
import com.example.demo.model.DangyuanInfo;
import com.example.demo.model.NewsInfo;
import com.example.demo.model.UserInfo;
import com.example.demo.service.DangyuanService;
import com.example.demo.util.ConstVariable;
import com.example.demo.util.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/dangyuan")
public class DangyuanController {
    @Autowired
    private DangyuanService dangyuanService;

    @RequestMapping("/add")
    public Object add(HttpServletRequest request, DangyuanInfo dangyuanInfo) {
        int result = 0;

//        // 1、参数校验
        if (!StringUtils.hasLength(dangyuanInfo.getName())
        ) {
            // 如果进入if语句中，说明前端所吃传参数有问题
            return AjaxResult.fail(-1, "前端参数传递有问题");
        }
        // 2、判断当前登录的用户是否的管理员（只有管理员有权限进行新增和删除）
        HttpSession session = request.getSession(false); // 获取当前的session，没有的话不自动创建
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) return AjaxResult.fail(-2 , "当前用户未登录，请登录后再试！");

        // 4、添加用户到数据库
        result = dangyuanService.add(dangyuanInfo);
        System.out.println(result);
        return AjaxResult.success("添加成功", result);
    }
    // 获取所有党政新闻（有分页）
    @RequestMapping("get_list_by_page")
    public Object getListByPage(Integer pindex, Integer psize, String name) {
        HashMap<String, Object> data = new HashMap<>();
        // 登录进去后，第一次初始化页面时候url = list.html，即pindex是空的，username等也是空的
        if (!StringUtils.hasLength(name)) {
            name = null;
        }
        if (pindex == null || pindex < 1) {
            pindex = 1;
        }
        if (psize == null || psize <= 0) {
            psize = 2;
        }
        // 上面这些特殊情况的处理，很重要（就是应对url = list.html这种情况，如果没有特判，会出错（查询不到数据）
        // 我们前后端都对这些特殊情况做了处理——不多余（双重保障，你进入公司后前端可不是你写的，所以我们就要把我们负责的后端写的扎实（不管前端怎么传，我后端都能应对）
        int offset = (pindex - 1) * psize;
        List<DangyuanInfo> list = dangyuanService.getListByPage(name, psize, offset);

        // getPageCount函数————》得到总的新闻条数
        int totalCount = dangyuanService.getPageCount(name);
        data.put("list", list);
        data.put("count", totalCount);
        return AjaxResult.success("分页功能的实现", data);

    }
    /**
     * 从用户信息列表中删除指定id的用户信息
     */
    @RequestMapping("/delete")
    public Object delete(int id) {
        int ret = dangyuanService.delete(id);
        if (ret == 0) {
            return AjaxResult.fail(-1, "删除指定id的党员信息失败");
        }
        return AjaxResult.success(ret, "删除指定id的党员信息成功");
    }
}
