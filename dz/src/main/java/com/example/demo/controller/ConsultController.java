package com.example.demo.controller;

import com.example.demo.config.AjaxResult;
import com.example.demo.model.ConsultInfo;
import com.example.demo.model.NewsInfo;
import com.example.demo.service.ConsultService;
import com.example.demo.service.NewsService;
import com.example.demo.util.ConstVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * 而Spring Boot框架项目接口返回 JSON格式的数据比较简单：
 * 在 Controller 类中使用@RestController注解即可返回 JSON格式的数据。
 */
@RestController
@RequestMapping("/consult")
public class ConsultController {
    @Autowired
    private ConsultService consultService;// 属性注入


    // 新增操作（发表博客）
    @RequestMapping("/add")
    public Object add(HttpServletRequest request, String title, String url) {
        // todo 非空校验
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            return AjaxResult.fail(-1, "当前用户未登录，新闻新增失败！");
        }
        // 新增新闻
        ConsultInfo consultInfo = new ConsultInfo();
        consultInfo.setTitle(title);
        consultInfo.setUrl(url);
        int result = consultService.add(consultInfo);
        if (result == 1) {
            return AjaxResult.success("新闻新增成功！", 1);
        }
        else {
            return AjaxResult.fail(-2, "数据库插入失败，新闻失败，请稍后再试！");
        }
    }
    // 获取所有党政新闻（无分页）
    @RequestMapping("/get_list")
    public Object getNewsList() {
        HashMap<String, Object> data = new HashMap<>();
        List<NewsInfo> list = consultService.getNewsList();
        data.put("list", list);
        return AjaxResult.success("查找新闻成功", data);
    }
    // 获取所有党政新闻（有分页）
    @RequestMapping("get_list_by_page")
    public Object getListByPage(Integer pindex, Integer psize) {
        HashMap<String, Object> data = new HashMap<>();
        // 登录进去后，第一次初始化页面时候url = list.html，即pindex是空的，username等也是空的
        if (pindex == null || pindex < 1) {
            pindex = 1;
        }
        if (psize == null || psize <= 0) {
            psize = 2;
        }
        // 上面这些特殊情况的处理，很重要（就是应对url = list.html这种情况，如果没有特判，会出错（查询不到数据）
        // 我们前后端都对这些特殊情况做了处理——不多余（双重保障，你进入公司后前端可不是你写的，所以我们就要把我们负责的后端写的扎实（不管前端怎么传，我后端都能应对）
        int offset = (pindex - 1) * psize;
        List<NewsInfo> list = consultService.getListByPage(psize, offset);

        // getPageCount函数————》得到总的新闻条数
        int totalCount = consultService.getPageCount();
        data.put("list", list);
        data.put("count", totalCount);
        return AjaxResult.success("分页功能的实现", data);

    }
    /**
     * 从用户信息列表中删除指定id的用户信息
     */
    @RequestMapping("/delete")
    public Object delete(int id) {
        int ret = consultService.delete(id);
        if (ret == 0) {
            return AjaxResult.fail(-1, "删除指定id的新闻失败");
        }
        return AjaxResult.success(ret, "删除指定id的新闻成功");
    }





}
