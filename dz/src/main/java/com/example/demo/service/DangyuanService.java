package com.example.demo.service;

import com.example.demo.mapper.DangyuanMapper;
import com.example.demo.model.DangyuanInfo;
import com.example.demo.model.UserInfo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DangyuanService {
    @Autowired
    private DangyuanMapper dangyuanMapper;
    public int add(DangyuanInfo dangyuanInfo) {
        return dangyuanMapper.add(dangyuanInfo);
    }
    // 查询当前用户列表信息(带分页和查询）
    public List<DangyuanInfo> getListByPage(
            String name, Integer limit, Integer offset
    ){
        return  dangyuanMapper.getListByPage(name, limit, offset);
    }

    // 得出用户名为username,地址为address,邮箱为email的所有用户个数
    public int getPageCount(String name) {
        return dangyuanMapper.getPageCount(name);
    }
    /// 删除指定id的新闻
    public int delete(int id) {
        return dangyuanMapper.delete(id);
    }
}
