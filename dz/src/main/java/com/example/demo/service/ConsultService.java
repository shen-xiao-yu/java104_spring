package com.example.demo.service;

import com.example.demo.mapper.ConsultMapper;
import com.example.demo.mapper.NewsMapper;
import com.example.demo.model.ConsultInfo;
import com.example.demo.model.NewsInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsultService {
    @Autowired
    private ConsultMapper consultMapper;
    // 发布新的新闻
    public int add(ConsultInfo consultInfo) {
        return consultMapper.add(consultInfo);
    }
    //  // 获取所有党政新闻（无分页）
    public List<NewsInfo> getNewsList() {
        return consultMapper.getNewsList();
    }
    /// 删除指定id的新闻
    public int delete(int id) {
        return consultMapper.delete(id);
    }



    //  // 查询所有用户的博客列表(分页功能的实现）
    public List<NewsInfo> getListByPage(int pindex, int psize) {
        return consultMapper.getListByPage(pindex, psize);
    }
    // 总的新闻条数
    public int getPageCount() {
        return consultMapper.getPageCount();
    }
}
