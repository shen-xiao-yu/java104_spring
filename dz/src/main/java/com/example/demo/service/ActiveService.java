package com.example.demo.service;

import com.example.demo.mapper.ActiveMapper;
import com.example.demo.mapper.DangyuanMapper;
import com.example.demo.model.ActiveInfo;
import com.example.demo.model.DangyuanInfo;
import com.example.demo.model.UserInfo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActiveService {
    @Autowired
    private ActiveMapper activeMapper;
    public int add(ActiveInfo activeInfo) {
        return activeMapper.add(activeInfo);
    }
    // 查询当前用户列表信息(带分页和查询）
    public List<ActiveInfo> getListByPage(
            String name, Integer limit, Integer offset
    ){
        return  activeMapper.getListByPage(name, limit, offset);
    }

    // 得出用户名为username,地址为address,邮箱为email的所有用户个数
    public int getPageCount(String name) {
        return activeMapper.getPageCount(name);
    }
    /// 删除指定id的新闻
    public int delete(int id) {
        return activeMapper.delete(id);
    }
}
