package com.example.demo.service;

import com.example.demo.mapper.NewsMapper;
import com.example.demo.model.NewsInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsService {
    @Autowired
    private NewsMapper newsMapper;
    // 发布新的新闻
    public int add(NewsInfo newsInfo) {
        return newsMapper.add(newsInfo);
    }
    //  // 获取所有党政新闻（无分页）
    public List<NewsInfo> getNewsList() {
        return newsMapper.getNewsList();
    }
    /// 删除指定id的新闻
    public int delete(int id) {
        return newsMapper.delete(id);
    }



    //  // 查询所有用户的博客列表(分页功能的实现）
    public List<NewsInfo> getListByPage(int pindex, int psize) {
        return newsMapper.getListByPage(pindex, psize);
    }
    // 总的新闻条数
    public int getPageCount() {
        return newsMapper.getPageCount();
    }

}
