package com.example.demo.util;

// 公共变量，全局变量----当前项目中的所有常量
public class ConstVariable {
    // session中的key，当前用户登录信息的所在会话session中的key
    public static final String USER_SESSION_KEY = "user_session_key";
}
