package com.example.demo.model;

import lombok.Data;

@Data
public class DangyuanInfo {
    private int id;
    private String name;
    private String gender;
    private String phone;
    private String depart;
}
