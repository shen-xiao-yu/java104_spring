package com.example.demo.model;

import lombok.Data;

@Data
public class ConsultInfo {
    private int id;
    private String title;
    private String url;
}
