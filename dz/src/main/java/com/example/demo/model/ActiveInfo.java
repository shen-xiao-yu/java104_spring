package com.example.demo.model;

import lombok.Data;

@Data
public class ActiveInfo {
    private int id;
    private String name; // 预约人
    private String order_time; // 预约时间
    private String phone;  // 预约人电话
    private String address; // // 预约地点
}
