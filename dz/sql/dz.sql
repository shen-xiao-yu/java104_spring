-- 创建数据库
drop database if exists dz;
create database dz DEFAULT CHARACTER SET utf8mb4;

-- 使用数据数据
use dz;

-- 创建表[用户表]
drop table if exists  userinfo;
create table userinfo(
    id int primary key auto_increment,
    username varchar(100) not null unique,
    password varchar(65) not null,
    photo varchar(500) default '',
    createtime timestamp default now(),
    `state` int default 1
) default charset 'utf8mb4';

create table newsinfo(
    id int primary key auto_increment,
    title varchar(64) not null unique,
    url varchar(255) not null unique
) default charset 'utf8mb4';



create table dangyuaninfo(
    id int primary key auto_increment,
    name varchar(64) not null unique,
    gender varchar(10) not null,
    phone varchar(255) not null,
    depart varchar(255) not null
) default charset 'utf8mb4';


create table consultinfo(
    id int primary key auto_increment,
    title varchar(64) not null unique,
    url varchar(255) not null unique
) default charset 'utf8mb4';

create table activeinfo(
    id int primary key auto_increment,
    name varchar(64) not null unique,
    order_time varchar(10) not null,
    phone varchar(255) not null,
    address varchar(255) not null
) default charset 'utf8mb4';


create table images_active(
    id int primary key auto_increment,
    title varchar(64) not null unique
) default charset 'utf8mb4';
