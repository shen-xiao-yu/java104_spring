package unloginTests;

import common.AutoTest;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class myMusicListTest extends AutoTest {
    public static ChromeDriver driver = createDriver();
    @Test
    @BeforeAll // 被@BeforeAll修饰的方法要是静态的
    static void init() {
        driver.get("http://49.235.66.46:9090/loveMusic.html");
//        driver.manage().window().maximize();
    }


    @Test
    @Order(1)
    void pageTest() {

        Duration timeout = Duration.ofSeconds(3); // 设立弹窗轮询查看时间

        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            // 等待弹窗出现---如果出现弹窗，说明当前收藏（喜欢）列表为空，就不执行接下来的测试步骤，直接返回
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            System.out.println("弹窗内容：" + alert.getText());
            Assertions.assertEquals("当前用户未登录，你即将跳转到登录页面", alert.getText());
            alert.accept();
            // 点击弹窗的确定，查看是否跳转到了登录页面
            String expectUrl = "http://49.235.66.46:9090/login.html";
            Assertions.assertEquals(expectUrl, driver.getCurrentUrl());
        } catch (TimeoutException e) {
            // 异常处理：超时未出现弹窗--收藏（喜欢列表）不为空
            System.err.println("等待弹窗超时");
        }
    }
    @Test
    @AfterAll
    static void exit() {
        driver.quit();
    }
}
