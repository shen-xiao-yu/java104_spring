package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class AutoTest {
    // public static ChromeDriver driver;
    public static ChromeDriver createDriver() {
        System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        ChromeDriver driver = new ChromeDriver(options);
        // 隐式等待一下，让页面渲染完成，防止找不到页面中的元素
        // / 隐式等待--// 隐式等待，更加丝滑——》作用于下面的整个作用领域，这个方法中的所有元素，在这3秒内不断轮询
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        return driver;
        // 隐式等待会作用于driver的整个生命周期

    }


}