package loginedTests;

import common.AutoTest;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class regTest extends AutoTest {
    // 这里我们调用了父类AutoTest中的静态方法，获取驱动对象
    public static ChromeDriver driver = createDriver();
    @Test
    @BeforeAll // 被@BeforeAll修饰的方法要是静态的
    static void init() {
        // 跳转到博客登录页面
        driver.get("http://49.235.66.46:9090/reg.html");
        driver.manage().window().maximize();
    }
    // 检查页面显示，进行注册用户的测试
    @Test
    @Order(1)
    void regPageTest() throws InterruptedException {
        Assertions.assertEquals("登录", driver.findElement(By.cssSelector("body > div.nav > a")).getText());
        Assertions.assertEquals("注册", driver.findElement(By.cssSelector("body > div.login-container > div > h3")).getText());
        Assertions.assertEquals("确认密码", driver.findElement(By.cssSelector("body > div.login-container > div > div:nth-child(4) > span")).getText());
        // 未输入用户名和密码，就点击提交
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(150); // 等待弹窗的出现
        Alert alert = driver.switchTo().alert();
        Assertions.assertEquals("用户名或者密码不能为空！", alert.getText());
        alert.accept();
        // 正常注册的情况
        driver.findElement(By.cssSelector("#username")).sendKeys("是11");
        driver.findElement(By.cssSelector("#password1")).sendKeys("123456");
        driver.findElement(By.cssSelector("#password2")).sendKeys("123456");
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(150);
        Alert alert1 = driver.switchTo().alert();
        Assertions.assertEquals("注册成功！", alert1.getText());
        alert1.accept();
        String expectUrl = "http://49.235.66.46:9090/login.html";
        Assertions.assertEquals(expectUrl, driver.getCurrentUrl());
        driver.quit();
    }
}
