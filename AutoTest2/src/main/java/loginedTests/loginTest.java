package loginedTests;

import common.AutoTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.commons.io.FileUtils;


import java.io.File;
import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 指定类的测试顺序(根据每个方法的order序号）
public class loginTest extends AutoTest {
    // 这里我们调用了父类AutoTest中的静态方法，获取驱动对象

    public static ChromeDriver driver = createDriver();
    @Test
    @BeforeAll // 被@BeforeAll修饰的方法要是静态的
    static void init() {
        // 跳转到博客登录页面
        driver.get("http://49.235.66.46:9090/login.html");
        driver.manage().window().maximize();
    }
    // 检查登录页面显示是否正常
    @Test
    @Order(1)
    void loginPageTest() {
        String expect = "登录";
        String actual = driver.findElement(By.xpath("/html/body/div[2]/div/h3")).getText();
        // 通过断言查看页面显示
        Assertions.assertEquals(expect, actual);
        // 查看页面相关元素是否存在
        driver.findElement(By.cssSelector("#submit"));
        driver.findElement(By.cssSelector("body > div.nav > a"));
        Assertions.assertEquals("音乐播放器", driver.findElement(By.cssSelector("body > div.nav > span.title")).getText());
    }
    // 检查用户名密码出错的情况（登录失败的情况）
    @Order(2)
    @ParameterizedTest // 多个参数
    @CsvSource({"admin, 123"})
    void loginFailTest(String username, String password) throws InterruptedException {
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);

        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(100);
        Alert alert = driver.switchTo().alert();
        Assertions.assertEquals("登录失败，密码或者用户名错误！", alert.getText());
        alert.accept();

    }
    // 检查用户名或者密码正确的情况（登录成功的情况)
    @Order(3)
    @ParameterizedTest // 多个参数
    @CsvSource({"小鱼儿, 123"})
    void loginSuccessfulTest(String username, String password) throws InterruptedException, IOException {
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);

        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(200);
        Alert alert = driver.switchTo().alert();
        Assertions.assertEquals("登录成功！", alert.getText());
        alert.accept();
        // 上述步骤只是说明输入了账号和密码，但还不知道点击提交后是否会跳转到音乐列表页
        String expect = "http://49.235.66.46:9090/list.html";
        String actual = driver.getCurrentUrl();
        Assertions.assertEquals(expect, actual); // 查看当前的url是否音乐列表页面
        // 进行截图，看当前是否跳转到了音乐列表界面
        // 程序执行的速度和页面渲染的速度
        File srcFile =  driver.getScreenshotAs(OutputType.FILE);
        String fileName = "loginRightTest.png";
        FileUtils.copyFile(srcFile, new File(fileName));


    }


}
