package loginedTests;



import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
// 测试套件，同时运行多个测试类
// 第一种方法： @Suite && @SelectClasses
// 第二种方法： @Suite && @SelectPackages -- 指定包名类运行包下所有的测试用例
@Suite  // 我们要按照一定的顺序执行各个界面的测试类（有些界面是在登录后，才执行的）
@SelectClasses({ regTest.class, loginTest.class, musicListTest.class, myMusicListTest.class, musicUpload.class
})
//@SelectPackages("webAutoTest.logined_tests")
public class runSuite {

}