package loginedTests;

import common.AutoTest;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;

import java.io.File;
import java.io.IOException;

import static loginedTests.loginTest.driver;

/**
 * 登录状态，对音乐上传页面的自动化测试
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 指定类的测试顺序(根据每个方法的order序号）
public class musicUpload extends AutoTest {
    // 这里我们调用了父类AutoTest中的静态方法，获取驱动对象
//    public static ChromeDriver driver = createDriver();
    // 我们这里不用重新生成驱动，用的还是登录页面的驱动
    @Test
    @BeforeAll // 被@BeforeAll修饰的方法要是静态的
    static void init() {
        // 跳转到博客登录页面
        driver.get("http://49.235.66.46:9090/upload.html");
//        driver.manage().window().maximize();
    }
    /**
     * 检查页面显示、按钮
     */
    @Test
    @Order(1)
    void uploadPageTest() throws IOException, InterruptedException {
        File srcFile =  driver.getScreenshotAs(OutputType.FILE);
        String fileName = "musicUploadTest.png";
        FileUtils.copyFile(srcFile, new File(fileName));

        Assertions.assertEquals("音乐上传", driver.findElement(By.cssSelector("#uploadForm > h2")).getText());
        Assertions.assertEquals("选择文件：", driver.findElement(By.cssSelector("#uploadForm > div:nth-child(2) > span")).getText());
        Assertions.assertEquals("歌手名字：", driver.findElement(By.cssSelector("#uploadForm > div:nth-child(4) > span.text-form")).getText());

        // 未选择文件和输入歌手名，点击上传
        driver.findElement(By.cssSelector("#uploadForm > input[type=button]")).click();
        Thread.sleep(150);
        // 查看是否有弹窗
        Alert alert = driver.switchTo().alert();
        Assertions.assertEquals("请输入歌曲的作者", alert.getText());
        alert.accept();

    }
    @Test
    @AfterAll
    static void exit() {
        driver.quit();
    }

}
