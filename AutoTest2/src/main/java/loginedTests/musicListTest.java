package loginedTests;

import common.AutoTest;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;

import java.io.File;
import java.io.IOException;

import static loginedTests.loginTest.driver;

/**
 * 登录情况下
 * 音乐总列表页面的测试
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 指定类的测试顺序(根据每个方法的order序号）
public class musicListTest extends AutoTest {
//    public static ChromeDriver driver = createDriver();

    @Test
    @BeforeAll
    static void init() {
        driver.get("http://49.235.66.46:9090/list.html?pindex=1&name=");
        driver.manage().window().maximize();
    }
    @Test
    @Order(1)
    void pageTest() { // 检查页面显示和音乐播放功能
        // 跳转后的新页面的文字显示
        Assertions.assertEquals("音乐列表", driver.findElement(By.cssSelector("#body > div.container > h3")).getText());
        // 查看播放悬浮框是否存在
        driver.findElement(By.cssSelector("#body > div:nth-child(3) > div > div > div.big-play-btn > a"));
        Assertions.assertEquals("喜欢列表", driver.findElement(By.cssSelector("#body > div.container > div:nth-child(3) > a:nth-child(1)")).getText());
        Assertions.assertEquals("添加歌曲", driver.findElement(By.cssSelector("#body > div.container > div:nth-child(3) > a:nth-child(2)")).getText());
        Assertions.assertEquals("删除选中", driver.findElement(By.cssSelector("#delete")).getText());
        Assertions.assertEquals("退出登录", driver.findElement(By.cssSelector("#logout")).getText());
        // 各个链接、按钮，能否正常跳转到对应页面
//        driver.findElement(By.cssSelector("#body > div.container > div:nth-child(3) > a:nth-child(1)")).click();
//        String expectUrl1 = "http://49.235.66.46:9090/loveMusic.html";
//        Assertions.assertEquals(expectUrl1, driver.getCurrentUrl());
//        driver.navigate().back(); // 回退到音乐列表页面
        driver.findElement(By.cssSelector("#body > div.container > div:nth-child(3) > a:nth-child(2)")).click();
        String expectUrl = "http://49.235.66.46:9090/upload.html";
        Assertions.assertEquals(expectUrl, driver.getCurrentUrl());
        driver.navigate().back();
        // 检查音乐播放功能
        // 获取第一首歌曲的播放按钮元素，点击播放
        driver.findElement(By.cssSelector("#info > tr:nth-child(1) > td:nth-child(4) > button")).click();
        String expectSrc = "http://jackzhang1204.github.io/materials/where_did_time_go.mp3";
        String actualSrc = driver.findElement(By.cssSelector("#body > div:nth-child(3) > div > audio")).getAttribute("src");
        // 默认未点击播放的src值为：http://jackzhang1204.github.io/materials/where_did_time_go.mp3
        // 如果正常播放：比如当前页面的第一个个歌曲——此时src的值 ：http://49.235.66.46:9090/music/get?path=%E5%BC%80%E5%A7%8B%E6%87%82%E4%BA%86-%E5%AD%99%E7%87%95%E5%A7%BF.128.mp3
        Assertions.assertNotEquals(expectSrc,actualSrc); // 如果不匹配，说明音乐可以正常播放

    }
    @Test
    @Order(2)  // 分页功能测试
    void pagingTest() throws InterruptedException {
        // 先点击首页功能，因为我们本来就是在首页 ，页面地址没有发生变化
        String expectUrl1 = driver.getCurrentUrl();
        driver.findElement(By.cssSelector("#all > li:nth-child(1) > a")).click();
        String actualUrl1 = driver.getCurrentUrl();
        Assertions.assertEquals(expectUrl1, actualUrl1);
        // 接着点击上一页，查看页面的变化（因为此时我们已经是第一页了，所有会出现提示弹窗）
        driver.findElement(By.cssSelector("#all > li:nth-child(2) > a")).click();
        // 因为我们musicList继承了AutoTest父类，父类中隐式等待作用范围仍然包括这部分
        // 但是我们弹窗不属于HTML元素，隐式等待无法生效，我们这样需要强制等待
        Thread.sleep(100);
        Alert alert = driver.switchTo().alert();
        Assertions.assertEquals("已经是首页了", alert.getText());
        alert.accept();
        // 点击下一页(查看点击后，跳转的地址是否正确）
        driver.findElement(By.cssSelector("#all > li:nth-child(3) > a")).click();
        String expect = "http://49.235.66.46:9090/list.html?pindex=2&name=";
        Assertions.assertEquals(expect, driver.getCurrentUrl());
        // 末页元素的查看
        driver.findElement(By.cssSelector("#all > li:nth-child(4) > a"));
    }
    @Test
    @Order(3)// 搜索功能测试
    void searchTest() throws IOException, InterruptedException {
        // 往搜索框中输入文本，点击搜索，查看页面是否出现你要找的搜索结果（按歌曲名称查找）
        driver.findElement(By.cssSelector("#exampleInputName2")).sendKeys("开始懂了");
        driver.findElement(By.cssSelector("#submit1")).click();
        // 进行截图，看当前是否跳转到了登录界面
        // 程序执行的速度和页面渲染的速度
        File srcFile =  driver.getScreenshotAs(OutputType.FILE);
        String fileName = "searchTest.png";
        FileUtils.copyFile(srcFile, new File(fileName));

        // 如果搜索到了，应该能找到对应的歌手
        String expect = "孙燕姿";
        Assertions.assertEquals(expect, driver.findElement(By.cssSelector("#info > tr > td:nth-child(3)")).getText());
        // 如果搜索结果为空，是否有弹窗提醒
        driver.findElement(By.cssSelector("#exampleInputName2")).clear();
        driver.findElement(By.cssSelector("#exampleInputName2")).sendKeys("ddf懂了");
        driver.findElement(By.cssSelector("#submit1")).click();
        Thread.sleep(200);
        Alert alert = driver.switchTo().alert();
        Assertions.assertEquals("当前搜索结果为空，请重新搜索", alert.getText());
        alert.accept();
    }

    @AfterAll
    @Test
    static void exit() {
//        driver.quit();
    }

}
