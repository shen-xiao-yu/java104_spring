package loginedTests;

import common.AutoTest;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

import java.io.File;
import java.io.IOException;
import java.util.List;
import static loginedTests.loginTest.driver;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 指定类的测试顺序(根据每个方法的order序号）
public class myMusicListTest extends AutoTest {
    // 这里我们调用了父类AutoTest中的静态方法，获取驱动对象
//    public static ChromeDriver driver = createDriver();
    // 我们这里不用重新生成驱动，用的还是登录页面的驱动
    @Test
    @BeforeAll // 被@BeforeAll修饰的方法要是静态的
    static void init() {
        // 跳转到博客登录页面
        driver.get("http://49.235.66.46:9090/loveMusic.html");
//        driver.manage().window().maximize();
    }
    // 返回当前页面是否存在弹窗
    private boolean isAlertPresent(WebDriver driver) {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }
    /**
     *   检查页面显示和各个按钮所对应的功能（音乐播放，从收藏列表中移除该元素）
     */
    @Test
    @Order(1)
    void lovePageTest() throws InterruptedException, IOException {
        /**
         * 判断是否有弹窗，如果当前收藏列表为空，会出现弹窗提示
         */
        // 初始化 WebDriverWait 对象
        //  WebDriverWait 类来设置等待时间为3秒，并使用 until() 方法结合 ExpectedConditions.alertIsPresent() 来等待弹窗的出现。
        //  如果在等待时间内成功找到弹窗，until() 方法返回一个 Alert 对象；否则，返回 null。然后，我们将 Alert 对象与 null 进行比较，以判断弹窗是否存在。
        File srcFile1 =  driver.getScreenshotAs(OutputType.FILE);
        String fileName1 = "myMusicListTest.png";
        FileUtils.copyFile(srcFile1, new File(fileName1));
        Duration timeout = Duration.ofSeconds(3); // 设立弹窗轮询查看时间

        boolean isAlert = isAlertPresent(driver);

        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            // 等待弹窗出现---如果出现弹窗，说明当前收藏（喜欢）列表为空，就不执行接下来的测试步骤，直接返回
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            System.out.println("弹窗内容：" + alert.getText());
            alert.accept();
            return;
        } catch (TimeoutException e) {
            // 异常处理：超时未出现弹窗--收藏（喜欢列表）不为空
            System.err.println("等待弹窗超时");
        }
        String expect = "我喜欢的音乐列表";
        String actual = driver.findElement(By.cssSelector("body > div.container > h3")).getText();
        Assertions.assertEquals(expect, actual);
        // 查看页面相关元素是否存在，文本是否一致
        Assertions.assertEquals("回到首页", driver.findElement(By.cssSelector("body > div.container > div:nth-child(3) > a.btn.btn-primary")).getText());
        Assertions.assertEquals("退出登录", driver.findElement(By.cssSelector("#logout")).getText());
        // 搜索框和搜索按钮
        driver.findElement(By.cssSelector("#exampleInputName2"));
        Assertions.assertEquals("查询", driver.findElement(By.cssSelector("#submit1")).getText());
        // 查看播放悬浮框是否存在
        driver.findElement(By.cssSelector("body > div:nth-child(3) > div"));
        // 检查音乐播放功能
        //
        // 获取第一首歌曲的播放按钮元素，点击播放
        driver.findElement(By.cssSelector("#info > tr:nth-child(1) > td:nth-child(3) > button")).click();
        String expectSrc = "http://jackzhang1204.github.io/materials/where_did_time_go.mp3";
        String actualSrc = driver.findElement(By.cssSelector("body > div:nth-child(3) > div > audio")).getAttribute("src");
        // 默认未点击播放的src值为：http://jackzhang1204.github.io/materials/where_did_time_go.mp3
        // 如果正常播放：比如当前页面的第一个个歌曲——此时src的值 ：http://49.235.66.46:9090/music/get?path=%E5%BC%80%E5%A7%8B%E6%87%82%E4%BA%86-%E5%AD%99%E7%87%95%E5%A7%BF.128.mp3
        Assertions.assertNotEquals(expectSrc,actualSrc); // 如果不匹配，说明音乐可以正常播放

        /**
         * 从音乐列表中移除该元素(移除  列表中的第三首歌）
         */
        // 移除前，喜欢列表的歌曲数目 = 移除后，喜欢列表的歌曲数目 + 1；
        //   // 定位歌曲表格
        WebElement table = driver.findElement(By.cssSelector("body > div.container > table"));
        // 定位歌曲行元素
        List<WebElement> songRows = table.findElements(By.tagName("tr"));
        // 获取歌曲数量
        int songCount = songRows.size() - 1; //减去表格的头部
        System.out.println("移除前，当前页面歌曲的数目：" + songCount);
        // 移除元素--移除元素
        WebElement elementPlay;

        try {
            elementPlay = driver.findElement(By.cssSelector("#info > tr:nth-child(3) > td:nth-child(4) > button"));
        } catch (Exception e) {
            System.err.println("你要移除的元素不存在！！！");
            return;  // // 如果要移除的元素（歌曲），没有显示在列表中(或者已经移除），出现异常，直接返回
        }

        System.out.println("从而达到是");
        elementPlay.click();
        /**
         * 我们获取弹窗的时候，最好都try catch一下，这样更保险，处理因为找不到弹窗而出现的异常
         */
        // 会出现一个弹窗，提示是否要移除该元素
        try {
            Alert alert2 = driver.switchTo().alert();
            alert2.accept();
        }
        catch (TimeoutException e) {
            // 异常处理：超时未出现弹窗
            System.err.println("等待弹窗超时");
        }

        // 点击，确定，系统会处理这个操作，从数据库的喜欢列表中移除该元素，这是需要时间的，所有需要等待弹窗的出现
        // 移除后，有会出现一个弹窗，提示移除成功
        try {
            Alert alert3 = wait.until(ExpectedConditions.alertIsPresent());
            alert3.accept();
            File srcFile4 =  driver.getScreenshotAs(OutputType.FILE);
            String fileName4 = "moveResultTest.png";
            FileUtils.copyFile(srcFile4, new File(fileName4));
        }
        catch (TimeoutException e) {
            // 异常处理：超时未出现弹窗
            System.err.println("等待弹窗超时");
        }

        // 删除后，重新计算歌曲数量
        WebElement tableNew = driver.findElement(By.cssSelector("body > div.container > table"));
        // 定位歌曲行元素
        songRows = tableNew.findElements(By.tagName("tr"));
        // 获取歌曲数量
        int songCountNew = songRows.size() - 1; //减去表格的头部
        System.out.println("移除后，当前页面歌曲的数目：" + songCountNew);
        Assertions.assertEquals(songCount, songCountNew + 1);

    }

    /**
     * 搜索功能测试
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    @Order(2)
    void searchTest() throws IOException, InterruptedException {
        // 往搜索框中输入文本，点击搜索，查看页面是否出现你要找的搜索结果（按歌曲名称查找）
        driver.findElement(By.cssSelector("#exampleInputName2")).sendKeys("开始懂了");
        driver.findElement(By.cssSelector("#submit1")).click();

        // 等待弹窗出现
        Duration timeout = Duration.ofSeconds(3);
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        // 尝试获取弹窗
        // 我们在输入框中输入文本内容，点击查询，看是否出现弹窗
        // 如果出现弹窗，说明搜索结果为空————直接返回
        try {
            Alert alert4 = wait.until(ExpectedConditions.alertIsPresent());
            alert4.accept();
            return;
        }
        catch (TimeoutException e) {
            // 异常处理：超时未出现弹窗
            System.err.println("等待弹窗超时");
        }
        // 程序走到这里，说明上面点击搜索后----没有弹窗————搜索结果不为空
        System.out.println("No Alert Found.");
        // 搜索结果截图
        File srcFile =  driver.getScreenshotAs(OutputType.FILE);
        String fileName = "searchResultTest.png";
        FileUtils.copyFile(srcFile, new File(fileName));
        // 如果按歌曲名搜索到了，应该能找到该歌曲名所对应的歌手
        String expect = "孙燕姿";
        Assertions.assertEquals(expect, driver.findElement(By.cssSelector("#info > tr > td:nth-child(2)")).getText());


    }
}
