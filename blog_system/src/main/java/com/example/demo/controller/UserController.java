package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.ConstantVariable;
import com.example.demo.model.UserInfo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
/**
 * 而Spring Boot框架项目接口返回 JSON格式的数据比较简单：
 * 在 Controller 类中使用@RestController注解即可返回 JSON格式的数据。
 *  @RestController是@Controller和@ResponseBody两者的结合，使用这个注解后该controller的所有方法都会返回json格式的数据，
 *  因为@ResponseBody的作用就是把返回的对象转换为json格式，并把json数据写入response的body中，前台收到response时就可以获取其body中的json数据了。
 * 如果在整个controller类上方添加@RestController，其作用就相当于把该controller下的所有方法都加上@ResponseBody，使每个方法直接返回response对象。
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    public UserService userService;
    @RequestMapping("/reg")
    public Object reg(String username, String password1, String password2) {
        HashMap<String, Object> result = new HashMap<>();
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password1) || !StringUtils.hasLength(password2)) {

            return AjaxResult.fail("你输入的参数有误，请重新输入！");
        }
        else {
            if (!password1.equals(password2)) {
                return AjaxResult.fail("前后密码不一致，请重新输入！");
            }
            else {
                UserInfo userInfo = new UserInfo();
                userInfo.setUsername(username);
                userInfo.setPassword(password1);
                int ret = userService.reg(userInfo);
                if (ret != 1) {
                   return AjaxResult.fail("数据库添加用户失败，请稍后再试！");
                }
                else {
                    return AjaxResult.success("恭喜，注册成功！", ret);
                }
            }
        }
    }
    @RequestMapping("/login")
    public Object login(HttpServletRequest request, String username, String password) {
        HashMap<String, Object> result = new HashMap<>();
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password)) {
            return AjaxResult.fail("你输入的参数有误，请重新输入！");
        }
        else {
            // 需要在数据库中查询当前登录的用户是否存在
            UserInfo userInfo = userService.selectByUsername(username);
            if (userInfo == null || !password.equals(userInfo.getPassword())) {
               return AjaxResult.fail("你当前的用户名或密码错误，请重新输入！");
            }
            else  {
                HttpSession session = request.getSession(); // 创建session，持久化到session内存中
                // 把用户信息储存到我们的session中
                session.setAttribute(ConstantVariable.SESSION_USERINFO_KEY, userInfo);

                return AjaxResult.success("恭喜，登录成功！", "");
            }
        }
    }
}
