package com.example.demo.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 系统配置文件类（把拦截规则配置到该类中）
 */
@Configuration // 该配置文件在项目启动时配置，添加了该注解，下面的规则才有用
public class ApplicationConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterception loginInterception; // 用户登录拦截器

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 自定义拦截器添加到系统的配置规则当中
        registry.addInterceptor(loginInterception)
                .addPathPatterns("/**") // 拦截所有的请求
                .excludePathPatterns("/user/login") // 排除不拦截的url
                .excludePathPatterns("/user/reg")
                .excludePathPatterns("/login.html")
                .excludePathPatterns("/reg.html")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/img/**")
                .addPathPatterns("/editor.md/**");
    }
}
