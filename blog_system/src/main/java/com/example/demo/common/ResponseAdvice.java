package com.example.demo.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.HashMap;

/**
 * 统一数据格式返回（灵活性有待提高）
 *  通过统一数据格式的返回，不管我们控制层的方法返回了什么类型的数据
 *  通过重写末尾都能把他转成hashmap格式的数据，然后又通过@ResponseBody注解，将java对象转成了json对象。
 */
@ControllerAdvice
@ResponseBody
public class ResponseAdvice implements ResponseBodyAdvice {
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true; // 这个值为true的时候，才会对返回的数据进行重写
    }
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        // 在有了我们自定义数据返回后，我们的这个统一数据格式返回类就像是一个托地的。
        // 因为controller层可以直接调用AjaxResult，来返回hashmap(通过@ResponseBody转成json)
        // 但如果controller没有调用AjaxResult，直接返回了
        if (body instanceof HashMap) {
            return body;  // 此时已经是hashmap格式了
        }
        if (body instanceof Integer){ // 当controller层中的方法直接返回int类型时候
            int num = (int) body;
            if (num <= 0) {
                // 应对int类型错误返回(查询文章列表，新增和删除博客可能会用到）——》也可能用不到，如果新增或查询失败，我直接就在controller就返回了（通过调用AjaxResult)
                // 新增、删除或查询失败（非得在controller返回int值，再通过这里返回json对象的话，不灵活（出错信息显示的不具体
                // 所以说这里我们只是以防万一，我们还是选择再controller层直接返回json对象，这样更信息具体，更有怎针对性）
                return AjaxResult.fail("抱歉，本次操作失败，请稍后再试！"); // 这里无法区分是新增失败还是删除失败
                // 这里我们本来返回的是一个hashmap格式的对象，但加了@ResponseBody，把我们的java对象转成的了json格式
            }
        }

        if (body == null) { // （比如查询操作，直接返回查询到的UserInfo对象，然后直接返回该对象）
            // 这里我们本来返回的是一个hashmap格式的对象，但加了@ResponseBody，把我们的java对象转成的了json格式
            return AjaxResult.fail("抱歉，查询失败！"); // 这时对查询当前用户的特判
        }
        // 这里我们本来返回的是一个hashmap格式的对象，但加了@ResponseBody，把我们的java对象转成的了json格式
        return AjaxResult.success("操作成功", body);
        // 前端是通过result中的status值来判断操作是否成功的，这个类用来处理操作成功的情况（为操作成功的情况兜底）
        // 但这可能存在问题，如果操作失败，并且在controller层没有调用AjaxResult中的fail方法（而是直接返回，通过这个类来返回统一的数据格式，就会出现问题——》在这个类我们都是按成功的处理的）
        // 解决方案，在该类中提前判断body(判断操作失败的情况）--->我们约定如果操作失败就返回负数（在controller层调用AjaxResult的情况）
    }
}
