package com.example.demo.common;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * 用户登录拦截器（查看当前用户是否登录）
 */
@Component // 你需要把这个拦截器类存到spring当中
public class LoginInterception implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 判断用户是否登录
        HttpSession session = request.getSession(false); // false表示如果获取不到，不创建新的session
        if (session != null && session.getAttribute("userinfo") != null) {
            return true;
        }
        // 在后端我们一般不对页面进行处理，我们只需要把结果返回给前端
        response.setStatus(401);
        return false;

    }
}
