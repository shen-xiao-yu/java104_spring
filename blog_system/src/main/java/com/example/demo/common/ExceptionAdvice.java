package com.example.demo.common;
// 统一异常处理
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

// 统一功能处理：包括统一异常处理，统一数据格式的返回需要加这个注解
@ControllerAdvice
@ResponseBody      //@ResponseBody的作用其实是将java对象转为json格式的数据。
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    public HashMap<String, Object> exAdvice(Exception e) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("status", -1);
        result.put("msg", "程序出现异常：" + e.getMessage());
        result.put("data", "");
        return result;
    }

}
