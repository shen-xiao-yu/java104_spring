package com.example.demo.common;

import java.util.HashMap;

/**
 * 自定义返回数据（我们通过AjaxResult类返回的都是hashmap格式的对象
 * 之后还有通过@ResponseBody将java对象转成json格式的数据
 */
public class AjaxResult {
    /**
     * 操作成功时(带msg)
     */
    public static Object success(Object msg, Object data) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("status", 200);
        result.put("msg", msg);
        result.put("data", data);
        return result;
    }
    /**
     * 操作失败时候
     */
    public static Object fail(Object msg) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("status", -1);
        result.put("msg", msg);
        result.put("data", "");
        return result;
    }
}
