package com.example.demo.mapper;

import com.example.demo.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    // 用户注册
    public int reg(@Param("userinfo")UserInfo userInfo);
    // 从数据库中查询指定username的用户（用于登录，验证当前登录的用户在数据库中是否存在
    public UserInfo selectByUsername(@Param("username") String username);

}
