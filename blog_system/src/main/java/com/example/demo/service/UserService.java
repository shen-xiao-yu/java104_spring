package com.example.demo.service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service // 不要忘了类注解
public class UserService {
    // 属性注入mapper持久层的userMapper类
    @Autowired
    public UserMapper userMapper;
    // 用户登录
    public int reg(UserInfo userInfo) {
        return userMapper.reg(userInfo);
    }
    // 从数据库中查询指定username的用户（用于登录，验证当前登录的用户在数据库中是否存在
    public UserInfo selectByUsername(String username) {
        return userMapper.selectByUsername(username);
    }
}
