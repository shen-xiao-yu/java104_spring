package com.example.demo.model;

import lombok.Data;
// 该实体类对应数据库中的用户表
// @Data注解包含了getter和setter\toString
@Data // 普通的实体类
public class UserInfo {
    private int id;
    private String username;
    private String password;
    private String photo;
    private String createtime;
    private int state;
}