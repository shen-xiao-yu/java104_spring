drop database if exists usermanager;
create database usermanager character set 'utf8mb4';

use usermanager;

create table userinfo(
    uid int primary key auto_increment,
    username varchar(250) not null,
    login_name varchar(250) unique not null,
    password varchar(65) not null,
    sex varchar(2) default '男',
    age varchar(10) default '0',
    address varchar(250) default '',
    qq varchar(250) default '',
    email varchar(250) default '',
    for_admin bit default 0,
    state int default 1,
    create_time datetime default now(),
    update_time datetime default now()
) default charset='utf8mb4';
insert into userinfo(username, login_name, password, for_admin)
    values("超级管理员", "admin", "123", 1);