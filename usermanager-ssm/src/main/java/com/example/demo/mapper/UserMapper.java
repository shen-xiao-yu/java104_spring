package com.example.demo.mapper;

import com.example.demo.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    // 查看当前用户是否登录
    public UserInfo login(@Param("login_name") String login_name,
                          @Param("password") String password);

    // 新增用户
    public int add(@Param("userinfo") UserInfo userInfo);

    // 查询所有的用户
    public List<UserInfo> getList();

    // 查询指定login_name的用户
    public UserInfo getByLoginName(@Param("login_name") String login_name);

    // 查询指定uid的用户
    public UserInfo getByUid(@Param("uid") int uid);

    // 更新指定的用户信息
    public int update(@Param("userinfo") UserInfo userInfo);

    // 从用户信息列表中删除指定uid的用户信息
    public int delete(@Param("uid") int uid);

    // 删除多条数据
    public int delByIds(@Param("ids") List<Integer> ids);

    // 查询当前用户列表信息(带分页和查询）
    public List<UserInfo> getListByPage(
            @Param("username") String username,
            @Param("address") String address,
            @Param("email") String email,
            @Param("limit") Integer limit,
            @Param("offset") Integer offset
    );

    // 得出用户名为username,地址为address,邮箱为email的所有用户个数
    public int getPageCount(
            @Param("username") String username,
            @Param("address") String address,
            @Param("email") String email
    );

}
