package com.example.demo.service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.UserInfo;
import com.sun.corba.se.impl.ior.OldJIDLObjectKeyTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    // 管理员登录
    public UserInfo login(String login_name, String password) {
        return userMapper.login(login_name, password);
    }
    // 新增用户
    public int add(UserInfo userInfo) {
        return userMapper.add(userInfo);
    }
    // 查询所有的用户信息
    public List<UserInfo> getList() {
        return userMapper.getList();
    }
    // 查询指定login_name的用户
    public UserInfo getByLoginName(String login_name) {
        return userMapper.getByLoginName(login_name);
    }
    // 查询指定uid的用户
    public UserInfo getByUid(int uid) {
        return userMapper.getByUid(uid);
    }
    // 更新指定用户的uid
    public int update(UserInfo userInfo) {
        return userMapper.update(userInfo);
    }
    // 从用户信息列表中删除指定uid的用户信息
    public int delete(int uid) {
        return userMapper.delete(uid);
    }
    // 删除选中的多条用户信息
    public int delByIds(List<Integer> ids) {
        return userMapper.delByIds(ids);
    }
    // 查询当前用户列表信息(带分页和查询）
    public List<UserInfo> getListByPage(
        String username, String address, String email, Integer limit, Integer offset
    ){
        return  userMapper.getListByPage(username, address, email, limit, offset);
    }

    // 得出用户名为username,地址为address,邮箱为email的所有用户个数
    public int getPageCount(String username, String address, String email) {
        return userMapper.getPageCount(username, address, email);
    }

}
