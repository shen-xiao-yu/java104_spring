package com.example.demo.controller;

import com.example.demo.model.UserInfo;
import com.example.demo.service.UserService;
import com.example.demo.util.ConstVariable;
import com.example.demo.util.PasswordUtil;
import com.sun.corba.se.impl.ior.OldJIDLObjectKeyTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;
    /**
     * 管理员登录登录
     */
    @RequestMapping("/login")
    public Boolean login(HttpServletRequest request, String login_name, String password) {
        // 通过我们统一数据格式的配置，简化了我们在控制层的操作——在返回false数据前对数据进行了处理
        // 返回的不是false,而是json格式的数据
        if (StringUtils.hasLength(login_name) && StringUtils.hasLength(password)) {
            // 能够进入到if中，说明参数有效
            // UserInfo userInfo = userService.login(login_name, password); 未用随机加盐的登录方法）
            UserInfo userInfo = userService.getByLoginName(login_name);
            // 得到数据库中的密码
            String dbPassword = userInfo.getPassword();
            // 如果通过前端传来的登录名和密码能够在数据库中查到该条数据，说明登录成功
            if (userInfo != null && userInfo.getUid() > 0) {
                // 当通过用户输入的密码和数据库中的密码一致的时候，说明用户登录成功
                if (PasswordUtil.decrypt(password, dbPassword)) { // 随机加盐加密算法（解密操作）
                    // 存储session
                    HttpSession session = request.getSession(true); // 如果当前没有session,就创建一个出来
                    session.setAttribute(ConstVariable.USER_SESSION_KEY, userInfo);
                    return true;

                }
            }
        }
        return false;
    }

    /**
     * 新增用户（注意登录名是唯一的，要在数据库里设置唯一缺少，只是在程序新增用户前进行判断是不行的，
     *         因为该操作不是原子操作（可能两个登录名相同的用户同时进行新增操作）
     * @param request
     * @param userInfo
     * @return
     */
    @RequestMapping("/add")
    public int add(HttpServletRequest request, UserInfo userInfo) {
        int result = 0;
//        // 1、参数校验
        if (userInfo == null || !StringUtils.hasLength(userInfo.getLogin_name()) || !StringUtils.hasLength(userInfo.getUsername())
        || !StringUtils.hasLength(userInfo.getPassword())
        ) {
            // 如果进入if语句中，说明前端所吃传参数有问题
            return -1;
        }
        // 2、判断当前登录的用户是否的管理员（只有管理员有权限进行新增和删除）
        HttpSession session = request.getSession(false); // 获取当前的session，没有的话不自动创建
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) return -2;
        else {
            UserInfo loginUser = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
            if (!loginUser.isFor_admin()) return -3; // 非管理员直接返回

            // 3、校验要添加的用户登录名是否存在（虽然在之后的MaBatis已经做了限制——，但如果登录名已经存在，继续执行已经没有意义了）
            // 虽然这里做了限制，但由于原子性（两个登录名相同的用户同时进行添加）等问题，还要小概率会出错，我们的对数据库的唯一性做限制就是应对这种情况的。
            String login_name = userInfo.getLogin_name(); // 获取当前添加用户的登录名
            UserInfo loginNameUser = userService.getByLoginName(login_name);
            // 注意这里的userService.getByLoginName(login_name); ——该值可能为空，容易触发统一异常异常处理（空指针）
            // 不能这样写userService.getByLoginName(login_name) != null , 要这样写 loginNameUser != null
            if (loginNameUser != null && loginNameUser.getUid() > 0) {
                // 说明当前新添加的用户名在数据中存在
                return -4;
            }

        }
        // 4、添加用户到数据库
        userInfo.setPassword(PasswordUtil.encrypt(userInfo.getPassword())); // 对密码进行加密
        result = userService.add(userInfo);
        return result;
    }

    /**
     * 查询当前所有的用户信息(无分页和查询）
     * @return
     */
    @RequestMapping("/get_list")
    public List<UserInfo> getList() {
        return userService.getList();
    }

    /**
     * 查询当前用户列表信息(带分页和查询）
     * @param username
     * @param address
     * @param email
     * @param pindex
     * @param psize
     * @return
     */
    @RequestMapping("get_list_by_page")
    public HashMap<String, Object> getListByPage(
            String username, String address, String email, Integer pindex, Integer psize
    ){
        HashMap<String, Object> data = new HashMap<>();
        // 登录进去后，第一次初始化页面时候url = list.html，即pindex是空的，username等也是空的
        if (pindex == null || pindex < 1) {
            // pindex = 1;
        }
        if (psize == null || psize <= 0) {
            psize = 2;
        }
        if (!StringUtils.hasLength(username)) {
            username = null;
        }
        if (!StringUtils.hasLength(address)) {
            address = null;
        }
        if (!StringUtils.hasLength(email)) {
            email = null;
        }
        // 上面这些特殊情况的处理，很重要（就是应对url = list.html这种情况，如果没有特判，会出错（查询不到数据）
        // 我们前后端都对这些特殊情况做了处理——不多余（双重保障，你进入公司后前端可不是你写的，所以我们就要把我们负责的后端写的扎实（不管前端怎么传，我后端都能应对）
        int offset = (pindex - 1) * psize;
        int limit = psize;
        List<UserInfo> list = userService.getListByPage(username, address, email, limit, offset);
        // 得出用户名为username,地址为address,邮箱为email的所有用户个数
        int totalCount = userService.getPageCount(username, address, email);
        data.put("list", list);
        data.put("count", totalCount);
        return data;
    }

    /**
     * 查询指定login_name的用户信息
     * @param login_name
     * @return
     */
    @RequestMapping("/get_by_login_name")
    public UserInfo getByLoginName(String login_name) {

        return userService.getByLoginName(login_name);
    }
    /**
     * 查询指定uid的用户信息
     */
    @RequestMapping("/get_by_uid")
    public UserInfo getByUid(int uid) {
        UserInfo userInfo = null;
        // 1、非空校验
        if (uid < 0) {
            return userInfo;
        }
        // 2、查询数据库
        userInfo = userService.getByUid(uid);
        // 3、将密码隐藏起来
        userInfo.setPassword("");
        return userInfo;
    }
    /**
     * 更新指定uid的用户信息
     */
    @RequestMapping("/update")
    public int update(UserInfo userInfo, HttpServletRequest request) {
        // 1、非空校验
        int result = 0;
        if (userInfo == null
        || userInfo.getUid() <= 0
        || !StringUtils.hasLength(userInfo.getUsername())
        ) {
            // 如果进入if语句中，说明前端所吃传参数有问题
            return -1;
        }
        // 2、判断当前登录的用户是否的管理员（只有管理员有权限进行新增、修改删除）
        HttpSession session = request.getSession(false); // 获取当前的session，没有的话不自动创建
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) return -2;
        else {
            UserInfo loginUser = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
            if (!loginUser.isFor_admin()) return -3; // 非管理员直接返回
        }
        // 用户可能没有修改密码，只有当密码修改了，我们才用对密码进行随机加密
        if (StringUtils.hasLength(userInfo.getPassword())) {
            userInfo.setPassword(PasswordUtil.encrypt(userInfo.getPassword()));
        }
        result = userService.update(userInfo);
        return result;
    }
    /**
     * 从用户信息列表中删除指定uid的用户信息
     */
    @RequestMapping("/delete")
    public int delete(int uid) {
        if (uid <= 0) return -1;
        return userService.delete(uid);
    }
    /**
     * 删除选中的多条用户信息
     */
    @RequestMapping("/del_by_ids")
    public int delByIds(String ids, HttpServletRequest request) {
        if (!StringUtils.hasLength(ids)) return -1;
        String[] idsArr = ids.split(",");
        List<Integer> idsList = new ArrayList<>();
        // 得到当前登录用户的uid，在删除时候不能自己删自己
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) return -2;
        UserInfo userInfo = (UserInfo) session.getAttribute(ConstVariable.USER_SESSION_KEY);
        int thisUid = userInfo.getUid();
        // 进行删除操作
        for (String temp :
                idsArr) {
            int uid = Integer.parseInt(temp);
            if (uid == thisUid) {
                return -3; // 用户自己删自己，直接返回
            }
            idsList.add(uid);
        }
        return userService.delByIds(idsList);
    }
}
