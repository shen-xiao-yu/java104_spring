package com.example.demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
/**
 * 统一返回的数据格式
 */
import java.util.HashMap;
// 我们的controller和这个类配合这返回统一json格式的数据
// 统一返回的数据格式——统一的格式需要用到@ControllerAdvice注解
@ControllerAdvice
//@ResponseBody 返回的可以页面可以是数据，但这里因为我们是与前端交付，我们把返回的数据交给前端，所有我们
public class ResponseAdvice implements ResponseBodyAdvice {
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true; // 这个值为true的时候，才会对返回的数据进行重写，下面的方法才有用
    }


    @SneakyThrows
    @Override
    // 在返回数据前对数据进行重写，封装成一个json类型的数据。这样简化了控制层（不用在新生成一个json对象了——这个工作由我们当前类来做了）但丧失了一定的灵活性
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("state", 1);
        result.put("msg", "");
        result.put("data", body); // 这个body是从我们controller层获取的，
        // 为了避免类型转换出错（String类型的特殊性，我们进行特判
        if (body instanceof String) {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(result);
        }
        // spring帮助我们之间把map对象自动转换成json
        return result;
        // 我们前端是通过result中的data来判断操作是否执行成功的
    }
}
