package com.example.demo.config;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * 统一异常拦截处理类
 */
@ControllerAdvice  // 统一格式或处理类——要用到@ControllerAdvice
@ResponseBody // 返回的页面可以是数据
// 这两个注解的功能等于@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    public Object exceptionAdvice(Exception e) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("state", -1);
        result.put("msg", "程序出现异常: " + e.getMessage());
        result.put("data", "");
        return result;
    }

}
