package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// 系统配置文件类
@Configuration // 该配置文件在项目启动时配置
public class ApplicationConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor; // 用户登录拦截器
    // 自定义拦截器添加到系统的配置文件中
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")       // 设置拦截规则
                .excludePathPatterns("/login") // 下面这些指定的url，不会检查是否登录——进行拦截
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/fonts/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/images/**")
                .excludePathPatterns("/login.html");
    }
}
