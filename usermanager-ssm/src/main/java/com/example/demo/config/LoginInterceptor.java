package com.example.demo.config;

import com.example.demo.util.ConstVariable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

// 用户自定义拦截器，查看当前用户是否登录
@Component // 把这个类交给spring托管
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);
        // 注意这个session.getAttribute()中的userinfo最好放到公共的变量(USER_SESSION_KEY)里面
        if (session == null || session.getAttribute(ConstVariable.USER_SESSION_KEY) == null) {
            // 当前用户未登录，进行拦截
            response.sendRedirect("/login.html");
            return false;
        }
        // 程序走到这里，说明用户已经登录
        return true;
    }
}
