package com.example.demo.model;

import lombok.Data;
// 用户实体类
@Data // 该注解就包含了getter和setter
public class UserInfo {
    private int uid;
    private String username;
    private String login_name;
    private String password;
    private String sex;
    private String  age;
    private String address;
    private String qq;
    private String email;
    private boolean for_admin; // 布尔类型的getter方法是is开头的，叫isFor_admin()
    private int state;     // 阿里巴巴开发手册不建议使用is开头的变量名（属性名）
    private String create_time;
    private String update_time;
}
