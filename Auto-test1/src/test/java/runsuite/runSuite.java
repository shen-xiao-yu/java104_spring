package runsuite;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

// 测试套件，同时运行多个测试类
// 第一种方法： @Suite && @SelectClasses
@Suite
@SelectClasses({oneTest.class, twoTest.class, threeTest.class})
public class runSuite {



//    第二种方法：@Suite && @SelectPackages
//    既然选择类文件的方式太低效了，那不如我们直接选择包。
    // 如果用使用你保密来值得运行该包下的所有测试类，这些测试类的命名必须要以Test或Tests结尾

}
