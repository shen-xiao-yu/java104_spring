package autotest;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.swing.tree.TreeNode;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Set;

public class SecondAutoTest {
    ChromeDriver driver = new ChromeDriver();
    public void imgTest() throws InterruptedException, IOException {
        driver.get("https://baidu.com");
        // Thread.sleep(3000);// 隐式等待和显示等待不能共存
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        // 隐式等待，更加丝滑——》作用于下面的整个作用领域，这个方法中的所有元素
        // 显示等待针对某一个元素来进行等待，极大降低了自动化整体的等待时间。
        driver.findElement(By.cssSelector("#s-top-left > a:nth-child(6)")).click();
        String curHandle = driver.getWindowHandle();
        System.out.println("当前句柄为: " + curHandle);

        // 先获取所有标签的句柄
        Set<String> handles = driver.getWindowHandles();
        for (String handle : handles) {
            System.out.println(handle);
            if (handle != curHandle) {
                driver.switchTo().window(handle);
            }
        }
        System.out.println("跳转后的当前句柄为：" + driver.getWindowHandle());
        driver.findElement(By.cssSelector("#homeSearchForm > span.s_ipt_wr > input")).sendKeys("风景");
        driver.findElement(By.cssSelector("#homeSearchForm > span.s_btn_wr > input")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));

        // 保存当前屏幕，屏幕截图
        // 程序执行的速度和页面渲染的速度
        File srcFile =  driver.getScreenshotAs(OutputType.FILE);
        String fileName = "test.png";
        FileUtils.copyFile(srcFile, new File(fileName));
        Thread.sleep(3000);
        driver.quit();
    }
    public void searchTest() throws InterruptedException {
        // 窗口最大话
        driver.manage().window().maximize();
        // 1、进入百度搜索网址
        driver.get("https://baidu.com");
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        // 隐式等待
        // driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        //Thread.sleep(3000);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        // 2、定位到搜索框位置，并输入内容
        WebElement ele = driver.findElement(By.xpath("//*[@id=\"kw\"]"));
        ele.sendKeys("是小鱼儿哈");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        driver.findElement(By.cssSelector("#su")).click();
        // 3、
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        // 4、获取文本内容
        String str = driver.findElement(By.cssSelector("#\\31  > div > div:nth-child(1) > h3 > a")).getText();
        System.out.println("文本内容：" + str);
//        Thread.sleep(3000);
        driver.quit();
    }
    public void blogTest() throws InterruptedException {
        driver.get("http://49.235.66.46:8080/BlogSystem/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("admin");
        driver.findElement(By.cssSelector("#password")).sendKeys("123456");
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(3000);
        driver.quit();
    }
}
