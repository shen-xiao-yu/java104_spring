package autotest;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class Main {
    public static void main(String[] args) {

    }
    @Test
    public void test() {
        // 指定ChromeDriver的驱动版本
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");
        // 解决selenium频繁更新导致的问题）
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("https://www.baidu.com");
        driver.findElement(By.cssSelector("#kw")).sendKeys("白盒测试");
        driver.findElement(By.xpath("//*[@id=\"su\"]")).click();
        // 隐式等待——作用于下面整个方法语句（在指定等待的3秒中，不断的对每个语句进行轮回查询，看要找的元素是否存在，不存在就等待几毫秒，进行下一次轮询
        // 如何不加等待，可能搜索结果还没有出来，会显示找不到该元素-=报错
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        // 查找页面指定元素
        String str = driver.findElement(By.cssSelector("#\\31  > div > h3 > a > div > div > p > span > span")).getText();
        System.out.println("文本内容为：" + str);
        // 断言，可以查看文本是否一致（检查页面元素是否符合用户
        Assertions.assertEquals("白盒测试(测试用例设计方法) - 百度百科", str);
        driver.quit();
    }
}
