package autotest;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class FirstAutoTest {
    // 搜狗网站搜索“Spring全家桶”
    public void souGouTest() throws InterruptedException {
        // 1、（创建驱动实例）打开谷歌浏览器，
        ChromeDriver driver = new ChromeDriver();
        Thread.sleep(3000); // 休眠3秒
        // 2、在浏览器中输入搜狗网址，访问搜狗首页
        driver.get("https://sogou.com/");
        Thread.sleep(3000);
        // 3、找到搜狗搜索的输入框元素，并输入搜索内容
        driver.findElement(By.cssSelector("#query")).sendKeys("Spring全家桶");
        Thread.sleep(3000);
        // 4、找到搜索按钮，并点击
        driver.findElement(By.cssSelector("#stb")).click();
        Thread.sleep(5000);
        // 5、结束会话（退出浏览器）
        driver.quit();
    }
    public void bliTest() throws InterruptedException {
        ChromeDriver driver = new ChromeDriver();
        Thread.sleep(3000);
//        driver.get("https://www.bilibili.com/");
//        Thread.sleep(3000);
//        // 定位到b 站的搜索框，并输入“社会心理学“这个搜索内容
//        driver.findElement(By.xpath("//*[@id=\"nav-searchform\"]/div[1]/input")).sendKeys("社会心理学");
//        // 需要注意，我们通过findElement定位元素，所定位查找的元素必须是唯一的
//        Thread.sleep(3000);
//        // 找到搜索按钮，并点击
//        driver.findElement(By.cssSelector("#nav-searchform > div.nav-search-btn")).click();
//        Thread.sleep(5000);
//        driver.quit();
        driver.get("https://www.bilibili.com/video/BV1LP4y1k7Ny/?spm_id_from=333.1007.tianma.1-1-1.click&vd_source=8a7e1f9c7d1ff6d13cda84d3c3c449f9");
        // 获取文本内容
        String test = driver.findElement(By.cssSelector("#viewbox_report > h1")).getText();
        System.out.println("打印视频标题结果为：" + test);

        String str = driver.findElement(By.cssSelector("#v_desc > div.desc-info.desc-v2 > span")).getText();
        System.out.println("文本内容：" + str);
        Thread.sleep(5000);
        driver.quit();
        // findElement()和findElements()——》查找页面元素
        // xpath、selector ——》元素定位的方式——》在浏览器的检查页面直接选择页面元素，复制粘贴
        // 但如果在页面中
    }

}
