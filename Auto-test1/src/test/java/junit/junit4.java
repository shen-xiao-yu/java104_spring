package junit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.stream.Stream;

// 参数化——》尽可能多的通过一个用例，多组参数来模拟用户的行为
public class junit4 {

    // 单参数
    @ParameterizedTest
    @ValueSource(strings = {"tom", "jack", "marry"})
    public void test(String name) {
        System.out.println(name);
    }
    // 多参数
    @ParameterizedTest
    @CsvSource({"tim, 34", "luck, 23", "marry, 27"})
    public void test2(String name, int age) {
        System.out.println(name + " " + age);
    }
    // 参数数据放到文件中（大量数据）
    @ParameterizedTest
    @CsvFileSource(files = "E:\\c盘文档\\mytest.csv")
    public void test3(String name, int age) {
        System.out.println(name + " " + age);
    }
    // 动态参数（动态方法来提供数据源
    @ParameterizedTest
    @MethodSource("dynamicParamTest")
    public void dynamicParamTest(String name, int age) {
        System.out.println("name: " + name + "  age: " + age);
    }
    static Stream<Arguments> dynamicParamTest() throws InterruptedException {
        // 构造动态参数
        String[] arr =  new String[5];
        for (int i = 0; i < arr.length; ++i) {
            Thread.sleep(100);
            arr[i] = System.currentTimeMillis() + " ";
        }
        return Stream.of(Arguments.arguments(arr[0], 20),
                Arguments.arguments(arr[1], 23),
                Arguments.arguments(arr[2], 23),
                Arguments.arguments(arr[3], 23),
                Arguments.arguments(arr[4], 23)

        );
    }
}
