package junit;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;


//用例的执行顺序
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)  // 现有使用注解说明当前类中的每个测试用例要按一定的顺序执行
public class junit3 {
    public static void main(String[] args) {

    }

    @Test
    @Order(1)
    public void test1() throws InterruptedException {
        // 指定ChromeDriver的驱动版本
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");
        // 解决selenium频繁更新导致的问题）
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        ChromeDriver driver = new ChromeDriver(options);
        driver.findElement(By.cssSelector("#kw")).sendKeys("黑盒测试");
        driver.findElement(By.cssSelector("#su")).click();
        // 隐式等待——作用于下面整个方法语句（在指定等待的3秒中，不断的对每个语句进行轮回查询，看要找的元素是否存在，不存在就等待几毫秒，进行下一次轮询
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        // 查找页面指定元素
        String str = driver.findElement(By.cssSelector("#\\31  > div > h3 > a")).getText();
        System.out.println("文本内容为：" + str);
        driver.quit();
    }
    //    assertNull：如果条件为null，通过测试；反之，则不通过
    @Test
    @Order(2)
    public void test2() {
        System.out.println("222222222222");

    }
    @Test
    @Order(3)
    void test3() {
        System.out.println("33333333333");
    }
}
