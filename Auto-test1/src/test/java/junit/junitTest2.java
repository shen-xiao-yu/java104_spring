package junit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class junitTest2 {
    // 断言匹配/不匹配
    // 断言结果为真，为假的
    // 断言结果为空，不为空
    @Test
    void test4() {
        Assertions.assertEquals("sdfk", "sdfk");
    }
    @Test
    void test1() {
        Assertions.assertNotEquals("dfskl", "d9sjf9wwe");
    }
//    assertNull：如果条件为null，通过测试；反之，则不通过
    @Test
    void test2() {
        Assertions.assertNull(null);
    }
     @Test
     void test3() {
        Assertions.assertNotNull("", "");
    }
}
