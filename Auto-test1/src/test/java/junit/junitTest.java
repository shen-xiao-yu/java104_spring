package junit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;
// 我们为什么要用junit，想想我们之前的测试方法是不是还要放到runAutoTest中才能执行，而在junit 中，我们可以直接通过@Test注解来实现这一点
public class junitTest {
    // junit 中提供和了非常强大的注解功能
    // @Test说明方法 是测试方法，执行当前这个类时，会自动的执行该类下的所有带@Test注解的用例
//    ChromeDriver driver = new ChromeDriver();
//    @Test
//    public void test1() throws InterruptedException {
//        driver.get("https://www.baidu.com");
//        Thread.sleep(2000);
//        driver.quit();
//
//    }
    // 有@BeforeEach注解的方法，表示当前的方法需要在每个测试用例的执行前都执行一次
    @BeforeEach
    public void beforeEachTest() {
        System.out.println("beforeEach当前方法，需要在每个用例执行前都执行一次");
    }
    @BeforeAll // 注意被该注解修饰的方法，必须为静态方法
    public static void beforeAll() {
        System.out.println("带有BeforeAll注解的方法会在当前类下的所有测试用例之前（方法）执行一次，注意只是执行一次");
    }
    @Test
    public void test2() {
        System.out.println("111111111111");
    }
    @Test
    public void test3() {
        System.out.println("2222222222222");
    }
    // 其余的还有@AfterEach\@AfterAll
}
