package autotest2;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class autoTest {
//    ChromeDriver driver = new ChromeDriver();
//    public void searchTest() {
//        driver.get("https://www.baidu.com");
//        driver.findElement(By.cssSelector("#kw")).sendKeys("java从入门到入门");
//        driver.findElement(By.cssSelector("#su")).click();
//
//        driver.quit();
//    }
//    // 操作浏览器的前进和后退 navigate
//    public void navigateControl() throws InterruptedException, IOException {
//
//        driver.manage().t // 隐式等待——作用于下面整个方法语句（在指定等待的3秒中，不断的对每个语句进行轮回查询，看要找的元素是否存在，不存在就等待几毫秒，进行下一次轮询imeouts().implicitlyWait(Duration.ofSeconds(3)); // 隐式等待，更加丝滑
//        driver.get("https://www.baidu.com"); // 到百度网址
//        driver.navigate().to("https://blog.csdn.net/DarkAndGrey/article/details/127837859"); // 到csdn
//        // 操作浏览器的前进和后退 navigate
//        // 回退操作——————》显示跳转到csdn网址前的百度网址
//        driver.navigate().back();
//        // 保存当前屏幕，屏幕截图
//        // 程序执行的速度和页面渲染的速度
//        File srcFile =  driver.getScreenshotAs(OutputType.FILE);
//        String fileName = "newTest.png";
//        FileUtils.copyFile(srcFile, new File(fileName));
//        // 前进，又进入到了csdn网址对应的页面
//        driver.navigate().forward();
//        // 刷新csdn首页
//        driver.navigate().refresh();
//        driver.quit();
//    }
//    // 弹窗测试、经过填充、确认弹窗、提示弹窗
//    public void popoverTest() throws InterruptedException {
//        // 弹窗不能直接通过前端元素来定位到弹窗
////        1、将driver对象作用的弹窗上（切换到弹窗）
////        2、选择确认/取消（提示填充还要输入文本）
//        // get的参数是url,而不是文件路径
////                driver.switchTo().alert().accept(); // 弹窗确认
////        driver.switchTo().alert().dismiss(); // 弹窗取消
//
//        driver.get("file:///E:/table(zhuo-mian)/%E6%9C%9F%E6%9C%AB%E5%A4%87%E8%80%83/d%20.html");
//        Thread.sleep(1000);
//        Alert alert = driver.switchTo().alert();
//        Thread.sleep(1000);
//        alert.accept();
//        Thread.sleep(1000);
//        driver.quit();
//
//    }
//    // 虽然警告填充只有确认按钮，但accept和dismiss都能处理
//
//    // 下拉框测试
//    public void selectTest() {
//
//    }
//    // js脚本测试
//    public void scriptTest() {
//        driver.get("");
//        driver.executeScript("do");
//    }
//    // 上传文件测试
//    public void uploadTest() throws InterruptedException {
//        driver.get("file:///D:/java%E4%BB%A3%E7%A0%81/java104/BlogSystem/src/main/webapp/selectTest.html");
//        // 这里只是把要上传的文件以名称的方式放到这里
//        driver.findElement(By.cssSelector("body > form > div:nth-child(1) > input[type=file]")).sendKeys("E:\\table(zhuo-mian)\\壁纸\\wlop-50se.jpg");
//        Thread.sleep(3000);
//        driver.quit();
//    }
    // 浏览器的参数设置_>无头模式
    @Test
    public void headlessTest() {
        // 无头模式
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        ChromeDriver driver = new ChromeDriver(options);
        options.addArguments("-headless");

        driver.get("https://www.baidu.com");
        driver.findElement(By.cssSelector("#kw")).sendKeys("是小鱼儿哈");
        driver.findElement(By.cssSelector("#su")).click();
        driver.quit();

    }
    @Test
    public void test() {
        // 指定ChromeDriver的驱动版本
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");
        // 解决selenium频繁更新导致的问题）
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        ChromeDriver driver = new ChromeDriver(options);
        driver.get("https://www.baidu.com");
        driver.findElement(By.cssSelector("#kw")).sendKeys("白盒测试");
        driver.findElement(By.xpath("//*[@id=\"su\"]")).click();
    }
    @Test
    public void bugTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.baidu.com");
//        driver.findElement(By.cssSelector("#kw")).sendKeys("黑盒测试");
//        driver.findElement(By.cssSelector("#su")).click();
//        Thread.sleep(1000);
        // 浏览器的页面加载是需要时间的，页面还没有把要下面要查找的页面元素给加载出来，你就要查找该页面元素，那么这样自然是找不到该元素的
//        String str = driver.findElement(By.cssSelector("#\\31  > div > h3 > a")).getText();
//        System.out.println("文本内容为：" + str);
    }

}
