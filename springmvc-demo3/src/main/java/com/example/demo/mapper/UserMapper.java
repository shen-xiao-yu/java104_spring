package com.example.demo.mapper;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper  // 添加了这个注解表明该接口是MyBatis接口，不能省略
public interface UserMapper {
    // 在这里写增删改查所对应的抽象方法
    public List<User> getAll(); // 查询操作
}
