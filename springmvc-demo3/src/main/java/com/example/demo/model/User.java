package com.example.demo.model;

import lombok.Data;
import java.util.Date;
@Data
public class User {
    private Integer id;
    private String username;
    private String password;
    private String photo;
    private Date createTime;
    private Date updateTime;
}