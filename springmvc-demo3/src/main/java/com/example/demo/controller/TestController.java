package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/web")
public class TestController {
    // 请求转发，做的活多，服务器内部帮我们跳转到一个新的页面，但此时访问的url却还是原来的
    @RequestMapping("/get1")
    public Object get1() {
        return "forward:/index.html";
    }

    // 请求重定向，做的活少，告诉当前访问的url说，你当前访问的地址不对，换个地址访问吧。
    // 跳转到了一个新的的页面，对应的url变了——和直接通过新的url访问该跳转后的页面是一样的
    @RequestMapping("/get2")
    public Object get2() {
        return "redirect:/blog_reg.html";

    }
}
