package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
// 等同与@Controller+@ResponseBody
public class UseController {
    @RequestMapping("/test1")
    public String test1(String name) {
        System.out.println("加油！！");
        return "前端传入的参数:" + name + "fdlksd";
    }

}
