package com.example.demo.mapper;

import com.example.demo.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest // 当前测试的上下文环境为Spring Boot
class UserMapperTest {
    // 通过属性注入，注入要测试的类
    @Autowired
    private UserMapper userMapper;
    @Test
    void getAll() {
        // 进行验证，看是否查询成功
        List<User> list = userMapper.getAll();
        for (User user :
                list) {
            System.out.println(user.toString());
            // 要注意我们的User实体类用了@Data注解，有toString方法
        }
    }
}