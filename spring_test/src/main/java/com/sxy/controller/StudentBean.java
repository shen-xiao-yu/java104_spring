package com.sxy.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
// Bean注解必须配合类注解来使用
public class StudentBean {
    @Bean
    // Bean重命名后，类名就不管用
    public Student test1() {  // Bean注解只能用在无参的方法中，因为Spring在这里无法传参
        Student student = new Student();
        student.setAge(20);
        student.setName("张三");
        return student;
    }
    @Bean
    public Student test2() {
        Student student = new Student();
        student.setAge(10);
        student.setName("StudentBean: 珊珊");
        return student;
    }
    // Spring储存Bean对象
    // HashMap<String, Object>
    @Bean(name = "StudentBean3")
    public Student test3() {
        Student student = new Student();
        student.setAge(18);
        student.setName("StudentBean: 小麦");
        return student;
    }
}
