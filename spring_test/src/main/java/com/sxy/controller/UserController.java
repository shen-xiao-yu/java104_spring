package com.sxy.controller;

import org.springframework.stereotype.Controller;

// 得到 UserService 三部曲：
// 1.最早：new UserService()
// 2.Spring：1.得到 spring 对象；2.使用 getBean 读取到 UserService
// 3.更加简单的读取方式：对象注入（属性注入、Setter注入、构造方法注入）
@Controller
public class UserController {
    public void sayController() {
        System.out.println("hello controller");
    }
}
