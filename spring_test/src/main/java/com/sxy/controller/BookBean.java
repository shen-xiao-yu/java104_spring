package com.sxy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
// 通过依赖注入，简化了我们从spring中读取Bean对象的过程
// 通过注解，简化了我们往Spring中存Bean对象的过程
@Controller
public class BookBean {
//    @Autowired
//    private Book book1; // 属性注入

    private Book book2; // setter注入
    @Autowired
    public void setBook2(Book book2) {
        this.book2 = book2;
    }

    public void sayAutowired() {
        System.out.println("nice! Autowired!!");
//        book1.setBookName("海底两万里");
//        book1.setBookPrice(23.5);
//        System.out.println("bookName: " + book1.getBookName() + " bookPrice: " + book1.getBookPrice());

        System.out.println("bookName: " + book2.getBookName() + " bookPrice: " + book2.getBookPrice());
    }
}
