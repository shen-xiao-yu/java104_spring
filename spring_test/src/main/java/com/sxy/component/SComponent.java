package com.sxy.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SComponent {
    @Autowired
    private UserComponent userComponent;
    public void printTest() {
        System.out.println("在用类注解来往Spring中存入Bean对象的时候，如果该类的类名首字母还有第二个字母");
        System.out.println("是大小的话，在用getBean方法来从Spring中读取存入的对象时候，一个参数就是类名，而不是首字母小写");
        userComponent.sayComponent();
    }
}
