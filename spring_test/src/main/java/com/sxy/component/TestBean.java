package com.sxy.component;

import com.sxy.controller.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class TestBean {
    @Autowired
    private UserComponent userComponent;
    @Bean(name = "test")
    public Student test1() {
        Student student = new Student();
        student.setAge(10);
        student.setName("珊珊");
        userComponent.sayComponent();
        return student;
    }

}
