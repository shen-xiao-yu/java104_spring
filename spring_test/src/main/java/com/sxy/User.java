package com.sxy;


import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

// 创建Bean对象，当然了他只是在spring中叫bean对象，在java中就是普通的对象
@Controller
public class User {
    @Bean
    public User SayHi() {
        System.out.println("Hi~~");
        System.out.println("这是一次更改！");
        User user = new User();
        return user;
    }
}
