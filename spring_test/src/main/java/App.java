

import com.sxy.User;
import com.sxy.component.UserComponent;
import com.sxy.controller.Student;
import com.sxy.controller.StudentBean;
import com.sxy.controller.UserController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

// 这是我们的启动类
// 这里我们创建的是Spring core项目，不是Spring web项目，项目不自带启动类，需要我们手动创建启动类

public class App {



    public static void main(String[] args) {
        // 1、创建Spring上下文对象
//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");
//        // 2、从Spring上下文对象中获取Bean对象
//        // 2.1 通过id,要强转
////        User user = (User) applicationContext.getBean("user");
//        // 2.2 有风险，如果在spring-config.xml里面存放了我们同一个类的两个不同的对象
////        User user = applicationContext.getBean(User.class);
//        // 2.3 好用
//        User user = applicationContext.getBean("SayHi", User.class);
//
//        // 3、使用Bean对象
//        user.SayHi();



////        UserController userController = applicationContext.getBean("userController", UserController.class);
        // 使用五大类注解
////        userController.sayController();


        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        // getBean第一个参数是被bean注解修饰的方法名
        // 使用方法注解
         Student student2 = context.getBean("test2", Student.class);
        System.out.println(student2.toString());


//
//        System.out.println(student2.getAge() + " | " + student2.getName());
//
//        Student student3 = context.getBean("StudentBean3", Student.class);
//        System.out.println(student3.getAge() + " | " + student3.getName())


//        Student student4 = context.getBean("student", Student.class);
//        student4.setName("是西安呀");  // 这是对我们得到的student对象的属性值进行了重新的赋值
//        student4.setAge(11);
//        // 对于5大类注解来说，getBean的第一个参数为类名的首字母小写形式
//        System.out.println(student4.getAge() + " | " + student4.getName());





    }
}