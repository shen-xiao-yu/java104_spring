import com.sxy.component.SComponent;
import com.sxy.component.UserComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;


public class App2 {
//    @Autowired
//    private UserComponent userComponent;
    public static void main(String[] args) {
        // 在Spring中，存和读Bean对象的时机是比较晚的
        // 不能直接通过属性注入来，你 _——-main方法是静态的
        // userComponent.sayComponent();


        // 通过五大类注解来从Spring获取Bean对象
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        UserComponent userComponent = context.getBean("userComponent", UserComponent.class);
        userComponent.print();

        SComponent sComponent = context.getBean("SComponent", SComponent.class);
        sComponent.printTest();
    }
}
