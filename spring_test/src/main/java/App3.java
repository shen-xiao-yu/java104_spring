import com.sxy.controller.Book;
import com.sxy.controller.BookBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App3 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        BookBean bookBean = context.getBean("bookBean", BookBean.class);
        bookBean.sayAutowired();
    }
}
