package com.example.demo.controller;

import com.example.demo.model.Student;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

// 注意这里只能用@Controller，不能用其他5大类注解（原因稍后在再说）
@Controller // 我是一个控制器，我要在spring加载的时候启动并注册
@ResponseBody // 当前类返回的是一个非静态页面
public class UseController {

    // 上传文件
    @RequestMapping("/upload")
    public String upload(@RequestPart("myfile")MultipartFile file) throws IOException {
        // 保存上传的文件
        file.transferTo(new File("D:\\img.png"));
        // 你要吧上传的文件保存到哪里，保存后的文件名是什么
        return "上传成功！";

    }


    /**
     *
     * 接收json对象
     * @param student
     * @return
     */
    @RequestMapping("login2")


    // 用HashMap来储存Json对象，其中String是存储key,Object存储value
    public HashMap<String, Object> login2(@RequestBody Student student) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", student.getId());
        result.put("name", student.getName());
        result.put("age", student.getAge());

        return result;
    }


    // from表单提交接收
    @RequestMapping("/login")
    public String login(String name, String password) {
        // 要注意的是：我们的形参名和from表单提交的属性的name是对应的
        // 中间是后端对前端传来数据的处理——这些业务代码我们就不写了
        return "<h1>name: " + name + "password: " + password + "</h1>";
    }


    // 用对象接收多个参数
    @RequestMapping("/test2")
    public String test2(Student student) {
        return student.toString();
    }




    @RequestMapping("/test1")
    public String test1(String name, Integer age) {
        return "传来的参数是：" + name + " | " + age;
    }


//    @RequestMapping(value = "/servlet", method = RequestMethod.GET) // 指定只能接收Get请求
//    public String useServletApi(HttpServletRequest request, HttpServletResponse response) {
//        String name = request.getParameter("name");
//        // 通过getParam默认获取的是String类型的数据，有时候需要强转（而当传来是参数为空的时候就会出问题）
//        return "传来的参数为：" + name;
//    }

//    // 之前get和post请求（浏览器url访问是get）
//    @RequestMapping("/mvc")
//    public String test() {
//        return "Hello SpringMVC!";
//    }
//    @GetMapping("/mvc1") // 只支持get请求
//    public String test1() {
//        return "Hello SpringMVC!";
//    }
//    @PostMapping("/mvc2") // 只支持post请求
//    public String test2() {
//        return "Hello SpringMVC!";
//    }

}
