package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@ResponseBody
public class WebTest {
    /**
     * 简洁的获取cookie
     * @param sxy
     * @return
     */
    @RequestMapping("/get1")
    public String get1(@CookieValue("sxy") String sxy) {
        return "获取到的cookie为: " + sxy;
    }
    // 传统的获取cookies   Header
    @RequestMapping("/get2")
    public String get2(HttpServletRequest req, HttpServletResponse resp) {
        // 获取所有 cookie 信息
        Cookie[] cookies = req.getCookies();
        String getCookie = cookies[0].getValue();
        String userAgent = req.getHeader("User-Agent");
        return getCookie + " " + userAgent;

    }
    // 简洁获取 Header—@RequestHeader
    @RequestMapping("/get3")
    public String get3(@RequestHeader("User-Agent") String UserAgent) {
        return UserAgent;
    }

    // 获取 HttpSession 对象，参数设置为 true 表示如果没有 session 对象就创建⼀个 session
    // 传统方法（servlet)
    @RequestMapping("get4")
    public String get4(HttpServletRequest request, HttpServletResponse response) {
        // 获取 HttpSession 对象，参数设置为 true 表示如果没有 session 对象就创建⼀个session
        HttpSession session = request.getSession(true);
        if (session != null) {
            session.setAttribute("username","java");
        }
        String getSession = (String) session.getAttribute("username");
        return "session为：" + getSession;
    }
    /**
     * 更简便的读取session
     * @param testSession
     * @return
     */
    @RequestMapping("/get5")
    public String get5(@SessionAttribute(value = "username", required =false) String testSession) {
        return "session: " + testSession;
    }

    // 在后端修改参数名（前端参数重命名）
    @RequestMapping("/get6")
    public String get6(@RequestParam("t") String time) {
        return time;
    }

    // 通过url传递参数，注意不是在问号中传递
    @RequestMapping("/get7/{name}/{password}")
    public String get7(@PathVariable String name, @PathVariable("password") String pass) {
        System.out.println("yuanl d如此！！！");
        return "打发打发name: " + name + " | pass: " + pass;  // 也可以在这里给前端参数重新命名
    }

}
