package com.example.demo.model;

import lombok.Data;

// 这就是一个纯粹的实体类——用来接收大量参数
// 不用注册到spring中
@Data // 复合注解；包含了getter\setter和toString
public class Student {
    private Integer id;
    private String name;
    private Integer age;
}
