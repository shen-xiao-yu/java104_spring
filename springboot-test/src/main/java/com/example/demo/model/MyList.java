package com.example.demo.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@Data // 包含了getter\setter和toString
@ConfigurationProperties(prefix = "mylist")
// 将配置文档中的集合赋值给这里的类
public class MyList {
    private List<String> data;
}
