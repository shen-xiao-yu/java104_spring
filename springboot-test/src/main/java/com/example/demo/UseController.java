package com.example.demo;

import com.example.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
// @RestController ————这个是个复合注解，等于@Controller + @ResponseBody
@Controller
@ResponseBody
public class UseController {
    @Autowired // 把我们model包中的Student对象通过属性注入
    private Student student1;

    @RequestMapping("/test")
    public String test() {
        return student1.toString();
    }
    // 读取用户自定义的配置文件
    @Value("${mykey.key}") // 把{}中的对应的配置内容赋值到下面的key1
    private String key1;  // 属性类型必须和配置文件中信息类型相同，但名称不做要求（不用非得是key）

    // 读取系统配置文件
    @Value("${server.port}")
    private String port; // 必须用String或Object类型来接收

    @RequestMapping("/sayHi") // 路由参数最好用sayHi或say_hi（windows系统不区分大小写，你写sayhi会有问题）
    public String sayHi() {
        return "Hi, spring boot!  " + key1  + "————项目的端口号为：" + port;
    }}
