package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class TestYml {
    @Value("${server.port}")
    private int port;


    @Value("${spring.datasource.password}")
    private Integer password;

    @Value("${string.value}")
    private String string;

    @Value("${boolean.value}")
    private Boolean aBoolean;

    @Value("${float.value}")
    private Float aFloat;

    @RequestMapping("/yml")
    public String testYml() {
        return "这个方法用来检测yml和properties配置不同的数据类型"
                + string + " " + aBoolean + " " + aFloat + port + password;
    }
}
