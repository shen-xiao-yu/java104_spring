package com.example.demo.controller;

import com.example.demo.model.MyList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class TestList {
    @Autowired // 把我们model包中的MyList对象通过属性注入
    private MyList list;
    @RequestMapping("/list")
    public String get() {
        return list.toString();
    }
}
